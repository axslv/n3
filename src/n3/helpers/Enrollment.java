/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.helpers;

import java.util.ArrayList;
import java.util.List;
import lv.nmc.entities.GroupEntry;
import lv.nmc.entities.JournalEntry;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.rest.JournalService;
import lv.nmc.rest.RegistrationService;
import lv.nmc.shared.MessageSender;
import n3.dialogs.shared.DialogState;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author JM
 */
public class Enrollment {
    
    private static List<RegistrationEntry> selectedRegs = new ArrayList<>();
    private static final DateTimeFormatter sqlDate = DateTimeFormat.forPattern("yyyy-MM-dd");

    public static void entrollRecord(RegistrationEntry currentRegistration, GroupEntry tmp) {
        
        currentRegistration.setGroupId(tmp.getGroupId());
        currentRegistration.setDateEnd(tmp.getDateEnd());
        currentRegistration.setDateStart(tmp.getDateStart());

        currentRegistration.setEvent("checkFutureRegistrations");

        try {
            selectedRegs = RegistrationService.fetchList(currentRegistration, currentRegistration.getEvent());
        } catch (Exception ex) {
            // Logger.getLogger(RegistrationCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (!selectedRegs.isEmpty()) {
            RegistrationEntry rer = new RegistrationEntry();
            rer = selectedRegs.get(0);
            StringBuilder tt = new StringBuilder().append(currentRegistration.getNameSurname()).append(" were already registered")
                    .append(" on date: ").append(rer.getDateStart()).append(" and course: ")
                    .append(currentRegistration.getCourseName());

            if (null != currentRegistration.getCertName()) {
                tt.append(" Cert NR: ").append(currentRegistration.getCertName());

            }
            tt.append("\nRegister client again?");
          //  dialogs.initConfirmDialog(tt.toString());

            if (1 == DialogState.getInstance().getState()) {
                return;
            }

        }

        JournalEntry je = new JournalEntry();
        je.setUsername(System.getProperty("n3.username"));
        je.setUserId(currentRegistration.getUserId());
        je.setGroupId(currentRegistration.getGroupId());
        je.setCourseId(currentRegistration.getCourseId());
        je.setOptype("insert");
        je.setRegistrationId(currentRegistration.getRegistrationId());
        je.setEvent("_updateInsertGroupRecord");
        je.setSysmsg("Username " + je.getUsername() + " has confirmed registration for userId=" + je.getUserId() + "");

        currentRegistration.setEvent("_updateRegisterUser");
        try {
            RegistrationService.updateRecord(currentRegistration, currentRegistration.getEvent());
            JournalService.insertRecord(je, je.getEvent());

            DateTime de = sqlDate.parseDateTime(currentRegistration.getDateEnd());
            DateTime today = sqlDate.parseDateTime(new DateTime().toString(sqlDate));

            if (today.isAfter(de)) {
                String messageText = "User " + System.getProperty("n3.username") + " has registered client <span style='color:red; font-weight: bold'>" + currentRegistration.getNameSurname() + " "
                        + currentRegistration.getPersonCode() + "</span> to some course with dateEnd=" + currentRegistration.getDateEnd();
                String subject = "Rogue registration!";
                String sendTo = "n3@novikontas.lv";
                String sendFrom = "training@novikontas.lv";
                MessageSender.sendMail(messageText, subject, sendTo, sendFrom);
            }

        } catch (Exception ex) {
            //  Logger.getLogger(RegistrationCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<RegistrationEntry> ls = new ArrayList<>();
        currentRegistration.setEvent("findPayment");

        try {
            ls = RegistrationService.fetchList(currentRegistration, currentRegistration.getEvent());

            if (ls.isEmpty()) {
                currentRegistration.setEvent("_updateInsertPaymentReg");
                RegistrationService.updateRecord(currentRegistration, currentRegistration.getEvent());
                je.setSysmsg("Payment inserted automatically for registrationId=" + currentRegistration.getRegistrationId());
                JournalService.insertRecord(je, je.getEvent());
            } else {
                je.setSysmsg("Payment was NOT inserted for registrationId=" + currentRegistration.getRegistrationId() + ". Payment record already exists (paymentId=" + ls.get(0).getPaymentId());
                JournalService.insertRecord(je, je.getEvent());
            }

        } catch (Exception ex) {
            //  Logger.getLogger(RegistrationCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
