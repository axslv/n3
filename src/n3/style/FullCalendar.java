/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.style;

import javafx.scene.control.Hyperlink;

/**
 *
 * @author Novikontas
 */
public class FullCalendar extends Hyperlink {
    
    public FullCalendar() {
        getStyleClass().add("greenLink");
    }
    
       public FullCalendar(String title) {
        getStyleClass().add("greenLink");
        setText(title);
    } 
    
}
