/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.style;

import javafx.scene.control.Hyperlink;

/**
 *
 * @author Novikontas
 */
public class EmptyCalendar extends Hyperlink {
    
    public EmptyCalendar() {
        getStyleClass().add("defaultLink");
        setMinWidth(24d);
     
    }
    
       public EmptyCalendar(String title) {
        getStyleClass().add("defaultLink");
        setText(title);
         setMinWidth(24d);
    } 
       
       public void checkHoliday(String day) {
           
           if ("sat".equalsIgnoreCase(day) || "sun".equalsIgnoreCase(day)) {
               getStyleClass().remove("greenLink");
               getStyleClass().add("holidayLink");
           }
       }
    
}
