/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package n3;

import java.util.logging.Level;
import java.util.logging.Logger;
import lv.nmc.entities.JournalEntry;
import lv.nmc.rest.JournalService;

/**
 *
 * @author jm
 */
public class Journal {
    
    public static void InsertRecord(JournalEntry je) {
        try {
            JournalService.insertRecord(je, je.getEvent());
        } catch (Exception ex) {
            Logger.getLogger(Journal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
