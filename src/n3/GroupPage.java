package n3;

import com.sai.javafx.calendar.FXCalendar;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Slider;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import lv.nmc.entities.CourseEntry;
import lv.nmc.entities.GroupEntry;
import lv.nmc.entities.JournalEntry;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.entities.StaffEntry;
import lv.nmc.rest.CourseService;
import lv.nmc.rest.GroupService;
import lv.nmc.rest.JournalService;
import lv.nmc.rest.RegistrationService;
import lv.nmc.rest.StaffService;
import lv.nmc.shared.CourseData;
import lv.nmc.shared.InstructorData;
import lv.nmc.shared.MessageSender;
import lv.nmc.shared.SharedData;
import n3.dialogs.shared.DialogState;
import n3.table.cells.Assistant1Cell;
import n3.table.cells.Assistant2Cell;
import n3.table.cells.Assistant3Cell;
import n3.table.cells.Assistant4Cell;
import n3.table.groupstable.CertPrintedCell;
import n3.table.groupstable.FinProtCell;
import n3.table.groupstable.RegReqCell;
import n3.table.cells.BanDateCell;
import n3.table.cells.CertGivenCell;
import n3.table.cells.CertNrCell;
import n3.table.cells.CompanyCell;
import n3.table.cells.CrewCommentCell;
import n3.table.cells.InstructorCell;
import n3.table.cells.NameSurnameCell;
import n3.table.cells.PriceCell;
import n3.table.cells.ToRegCell;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class GroupPage
        implements Initializable {

    private TextField dateEnd = new TextField();
    private TextField dateStart = new TextField();
    private WinLoader dialogs = new WinLoader();
    @FXML
    private CheckBox removeFromPayments; // Value injected by FXMLLoader 
    @FXML
    private ComboBox<String> documentType; // Value injected by FXMLLoader    
    @FXML
    private ListView<String> coursesList; // Value injected by FXMLLoader
    @FXML
    private RadioButton domainPe; // Value injected by FXMLLoader
    @FXML
    private RadioButton domainPn; // Value injected by FXMLLoader
    @FXML
    private RadioButton domainTc, ceng, cnav, cpvn, cgwo; // Value injected by FXMLLoader
    @FXML
    private TextField groupDateEnd; // Value injected by FXMLLoader
    @FXML
    private TextField groupDateStart; // Value injected by FXMLLoader
    @FXML
    private TextField groupTimeStart; // Value injected by FXMLLoader
    @FXML
    private TextField groupRoom; // Value injected by FXMLLoader
    @FXML
    private TextField groupName; // Value injected by FXMLLoader    
    @FXML
    private ComboBox<String> instructorsList, movableGroups; // Value injected by FXMLLoader
    @FXML
    private HBox pDateEnd; // Value injected by FXMLLoader
    @FXML
    private HBox pDateStart; // Value injected by FXMLLoader
    @FXML
    private TableView<GroupEntry> theory; // Value injected by FXMLLoader    
    @FXML
    private TableView<GroupEntry> practice; // Value injected by FXMLLoader   
    @FXML
    private TableColumn<GroupEntry, String> colThIns, colPractIns; // Value injected by FXMLLoader
    @FXML
    private TableColumn<GroupEntry, String> colThAss1, colThAss2, colThAss3, colThAss4;
    @FXML
    private TableColumn<GroupEntry, String> colPractAss1, colPractAss2, colPractAss3, colPractAss4;
    @FXML
    private TableView<GroupEntry> groupsTable; // Value injected by FXMLLoader
    @FXML
    private TableView<RegistrationEntry> preregTable; // Value injected by FXMLLoader  
    @FXML
    private TableView<RegistrationEntry> regTable; // Value injected by FXMLLoader 
    @FXML
    private TableView<RegistrationEntry> banTable; // Value injected by FXMLLoader 
    @FXML
    private TableColumn<RegistrationEntry, Integer> banRegId; // Value injected by FXMLLoader
    @FXML
    private TableColumn<RegistrationEntry, String> banNameSurname; // Value injected by FXMLLoader
    @FXML
    private TableColumn<RegistrationEntry, String> banPersonCode; // Value injected by FXMLLoader
    @FXML
    private TableColumn<RegistrationEntry, String> banDate; // Value injected by FXMLLoader
    @FXML
    private TableColumn<RegistrationEntry, String> banReason; // Value injected by FXMLLoader
    @FXML
    private TableColumn<GroupEntry, Boolean> tCert; // Value injected by FXMLLoader
    @FXML
    private TableColumn<GroupEntry, String> tCourseName; // Value injected by FXMLLoader
    @FXML
    private TableColumn<GroupEntry, String> tDateEnd; // Value injected by FXMLLoader
    @FXML
    private TableColumn<GroupEntry, String> tDateStart; // Value injected by FXMLLoader
    @FXML
    private TableColumn<GroupEntry, Boolean> tFinProtocol; // Value injected by FXMLLoader
    @FXML
    private TableColumn<GroupEntry, Integer> tGroupId; // Value injected by FXMLLoader
    @FXML
    private TableColumn<GroupEntry, String> tGroupName; // Value injected by FXMLLoader
    @FXML
    private TableColumn<GroupEntry, Boolean> tRegRequest; // Value injected by FXMLLoader
    @FXML
    private TableColumn<RegistrationEntry, Integer> preId; // Value injected by FXMLLoader   
    @FXML
    private TableColumn<RegistrationEntry, String> preNameSurname;
    @FXML
    private TableColumn<RegistrationEntry, String> regPrice; // Value injected by FXMLLoader          
    @FXML
    private TableColumn<RegistrationEntry, String> crewComment; // Value injected by FXMLLoader 
    @FXML
    private TableColumn<RegistrationEntry, Integer> regId; // Value injected by FXMLLoader    
    @FXML
    private TableColumn<RegistrationEntry, String> regNameSurname; // Value injected by FXMLLoader  
    @FXML
    private TableColumn<RegistrationEntry, String> regCompany; // Value injected by FXMLLoader  
    //   @FXML
//    private TableColumn<RegistrationEntry, Boolean> regCertGiven; // Value injected by FXMLLoader 
    @FXML
    private TableColumn<RegistrationEntry, String> regBanDate, regCertNr; // Value injected by FXMLLoader    
    @FXML
    private TableColumn<RegistrationEntry, String> payerCompany; // Value injected by FXMLLoader   
    @FXML
    private TableColumn<RegistrationEntry, String> payerComment; // Value injected by FXMLLoader         
    @FXML
    private ProgressBar progressBar; // Value injected by FXMLLoader
    @FXML
    private Label statusLabel; // Value injected by FXMLLoader
    @FXML
    private ToggleGroup domain; // Value injected by FXMLLoader   
    @FXML
    private Slider monthesPlus; // Value injected by FXMLLoader 
    @FXML
    private TitledPane x8, x7;
    @FXML
    private CheckBox checkCertificate;
    @FXML
    private TextField supervisor;

    private final DateTimeFormatter lvDate = DateTimeFormat.forPattern("dd/MM/yyyy");
    private final DateTimeFormatter sqlDate = DateTimeFormat.forPattern("yyyy-MM-dd");
    private final DateTimeFormatter yearDate = DateTimeFormat.forPattern("yy");
    private DateTime dateTime = new DateTime();

    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {

        preregTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        regTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
//        documentsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        movableGroups.getItems().clear();

        FXCalendar ds = new FXCalendar();
        FXCalendar de = new FXCalendar();

        ds.getTextField().textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov,
                    String oldDate, String newDate) {

                DateTime selected = lvDate.parseDateTime(newDate);
                // String sqlInput;                
                dateStart.setText(selected.toString(sqlDate));
            }
        });

        de.getTextField().textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov,
                    String oldDate, String newDate) {

                DateTime selected = lvDate.parseDateTime(newDate);
                dateEnd.setText(selected.toString(sqlDate));

            }
        });

        pDateStart.getChildren().add(ds);
        pDateEnd.getChildren().add(de);

        domainTc.setUserData("tc");
        domainPe.setUserData("ppp_eng");
        domainPn.setUserData("ppp_nav");
        cnav.setUserData("col_nav");
        ceng.setUserData("col_eng");
        cpvn.setUserData("pvn");
        cgwo.setUserData("gwo");

        initGroupsTable();
        initPreregTable();
        initRegTable();
        initInstructorTables();

        fetchData();

    }

    private void fetchData() {
        Task<Integer> t = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                fetchCoursesList("tc");
                fetchInstructorsList();
                fetchGroups(new ActionEvent());

                return 0;
            }
        };

        Task<Integer> progress = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working");
                return 0;
            }
        };

        Task<Integer> stop = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");

                return 0;
            }
        };

        Platform.runLater(progress);
        Platform.runLater(t);
        Platform.runLater(stop);
    }

    private void initInstructorTables() {
        theory.setEditable(true);
        practice.setEditable(true);
        theory.getSelectionModel().setCellSelectionEnabled(true);
        practice.getSelectionModel().setCellSelectionEnabled(true);
        colThIns.setEditable(true);
        colThAss1.setEditable(true);
        colThAss2.setEditable(true);
        colThAss3.setEditable(true);
        colThAss4.setEditable(true);

        colPractIns.setEditable(true);
        colPractAss1.setEditable(true);
        colPractAss2.setEditable(true);
        colPractAss3.setEditable(true);
        colPractAss4.setEditable(true);

        colThIns.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("instructorName"));
        colThAss1.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("assistant1Name"));
        colThAss2.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("assistant2Name"));
        colThAss3.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("assistant3Name"));
        colThAss4.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("assistant4Name"));

        colPractIns.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("instructorName"));
        colPractAss1.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("assistant1Name"));
        colPractAss2.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("assistant2Name"));
        colPractAss3.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("assistant3Name"));
        colPractAss4.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("assistant4Name"));

        Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>> instructorFactory
                = new Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>>() {
            @Override
            public TableCell<GroupEntry, String> call(TableColumn<GroupEntry, String> p) {
                return new InstructorCell();
            }
        };

        Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>> instructorPractFactory
                = new Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>>() {
            @Override
            public TableCell<GroupEntry, String> call(TableColumn<GroupEntry, String> p) {
                return new InstructorCell();
            }
        };

        Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>> ass1F
                = new Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>>() {
            @Override
            public TableCell<GroupEntry, String> call(TableColumn<GroupEntry, String> p) {
                return new Assistant1Cell();
            }
        };

        Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>> ass2F
                = new Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>>() {
            @Override
            public TableCell<GroupEntry, String> call(TableColumn<GroupEntry, String> p) {
                return new Assistant2Cell();
            }
        };

        Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>> ass3F
                = new Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>>() {
            @Override
            public TableCell<GroupEntry, String> call(TableColumn<GroupEntry, String> p) {
                return new Assistant3Cell();
            }
        };

        Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>> ass4F
                = new Callback<TableColumn<GroupEntry, String>, TableCell<GroupEntry, String>>() {
            @Override
            public TableCell<GroupEntry, String> call(TableColumn<GroupEntry, String> p) {
                return new Assistant4Cell();
            }
        };

        colThIns.setCellFactory(instructorFactory);
        colThAss1.setCellFactory(ass1F);
        colThAss2.setCellFactory(ass2F);
        colThAss3.setCellFactory(ass3F);
        colThAss4.setCellFactory(ass4F);

        colPractIns.setCellFactory(instructorPractFactory);
        colPractAss1.setCellFactory(ass1F);
        colPractAss2.setCellFactory(ass2F);
        colPractAss3.setCellFactory(ass3F);
        colPractAss4.setCellFactory(ass4F);

    }

    private void initGroupsTable() {
        groupsTable.setEditable(true);
        groupsTable.getSelectionModel().setCellSelectionEnabled(true);

        tCert.setEditable(true);
        tFinProtocol.setEditable(true);
        tRegRequest.setEditable(true);

        tGroupId.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, Integer>("groupId"));
        tGroupName.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("groupName"));
        tDateStart.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("dateStart"));
        tDateEnd.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("dateEnd"));
        tCourseName.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, String>("courseName"));
        tRegRequest.setCellValueFactory(
                new PropertyValueFactory<GroupEntry, Boolean>("regPrinted"));

        Callback<TableColumn<GroupEntry, Boolean>, TableCell<GroupEntry, Boolean>> regRegFactory
                = new Callback<TableColumn<GroupEntry, Boolean>, TableCell<GroupEntry, Boolean>>() {
            @Override
            public TableCell<GroupEntry, Boolean> call(TableColumn<GroupEntry, Boolean> p) {
                return new RegReqCell();
            }
        };

        Callback<TableColumn<GroupEntry, Boolean>, TableCell<GroupEntry, Boolean>> finProtFactory
                = new Callback<TableColumn<GroupEntry, Boolean>, TableCell<GroupEntry, Boolean>>() {
            @Override
            public TableCell<GroupEntry, Boolean> call(TableColumn<GroupEntry, Boolean> p) {
                return new FinProtCell();
            }
        };

        Callback<TableColumn<GroupEntry, Boolean>, TableCell<GroupEntry, Boolean>> certPrintFactory
                = new Callback<TableColumn<GroupEntry, Boolean>, TableCell<GroupEntry, Boolean>>() {
            @Override
            public TableCell<GroupEntry, Boolean> call(TableColumn<GroupEntry, Boolean> p) {
                return new CertPrintedCell();
            }
        };

        tRegRequest.setCellValueFactory(new PropertyValueFactory<GroupEntry, Boolean>("regPrinted"));
        tRegRequest.setCellFactory(regRegFactory);
        tFinProtocol.setCellValueFactory(new PropertyValueFactory<GroupEntry, Boolean>("finPrinted"));
        tFinProtocol.setCellFactory(finProtFactory);
        tCert.setCellValueFactory(new PropertyValueFactory<GroupEntry, Boolean>("certPrinted"));
        tCert.setCellFactory(certPrintFactory);

        groupsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observale, Object oldValue, Object newValue) {
                GroupEntry ge = (GroupEntry) newValue;
                selectedGroup = ge;
                fetchGroupData(ge);
            }
        });
    }
    private GroupEntry selectedGroup = new GroupEntry();

    private void fetchGroupData(GroupEntry ge) {

        if (null == ge) {
            return;
        }

        final GroupEntry gg = ge;

        preregTable.getItems().clear();
        regTable.getItems().clear();

        groupDateStart.setText(ge.getDateStart());
        groupDateEnd.setText(ge.getDateEnd());
        groupName.setText(ge.getGroupName());
        groupTimeStart.setText(ge.getTimeStart());
        groupRoom.setText(ge.getRoom());

        Task<Integer> progress = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working");

                return 0;
            }
        };

        Task<Integer> stop = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");

                return 0;
            }
        };

        Task<Integer> task = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                RegistrationEntry re = new RegistrationEntry();
                re.setCourseId(CourseData.getInstance().getLastCourses().get(gg.getCourseName()).getCourseId());
                re.setDateStart(gg.getDateStart());
                re.setGroupId(gg.getGroupId());
                re.setEvent("fetchRegisteredClients");
                List<RegistrationEntry> rlist = new ArrayList<>();
                List<RegistrationEntry> prereg = new ArrayList<>();
                List<RegistrationEntry> reg = new ArrayList<>();
                try {

                    rlist = RegistrationService.fetchList(re, re.getEvent());
                    int ireg = 0;
                    int banned = 0;
                    int fin = 0;
                    int pre = 0;

                    for (RegistrationEntry rre : rlist) {

                        if (null == rre.getGroupId()) {
                            prereg.add(rre);
                            pre++;
                        } else if (null != rre.getGroupId() && rre.getGroupId().equals(gg.getGroupId())) {
                            reg.add(rre);
                            ireg++;
                            fin++;

                            if (rre.getBanned().equals(1)) {
                                banned++;
                                fin--;
                            }

                        }
                    }

                    preregTable.getItems().addAll(prereg);
                    regTable.getItems().addAll(reg);

                    String title = "CONFIRMED REGISTRATIONS | GOOD: " + fin + " TOTAL: " + ireg;
                    String title2 = "PENDING REGISTRATIONS | TOTAL: " + pre;

                    x8.setText(title);
                    x7.setText(title2);

                } catch (Exception ex) {
                    Logger.getLogger(GroupPage.class.getName()).log(Level.SEVERE, null, ex);
                }
                fetchInstructorsAssigned();
                fetchMovableGroups();
                return 0;

            }
        };
        Platform.runLater(progress);
        Platform.runLater(task);
        Platform.runLater(stop);
    }

    private void initPreregTable() {
        preregTable.setEditable(true);
        preregTable.getSelectionModel().setCellSelectionEnabled(true);
        crewComment.setEditable(true);
        regPrice.setEditable(true);
//        toRegister.setEditable(true);

        preId.setCellValueFactory(
                new PropertyValueFactory<RegistrationEntry, Integer>("registrationId"));
        preNameSurname.setCellValueFactory(new Callback<CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<RegistrationEntry, String> p) {

                return new SimpleStringProperty(p.getValue().getNameSurname());
            }
        });

        regPrice.setCellValueFactory(new Callback<CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<RegistrationEntry, String> p) {

                return new SimpleStringProperty(p.getValue().getPrice());
            }
        });

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> priceCell
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new PriceCell();
            }
        };

        Callback<TableColumn<RegistrationEntry, Boolean>, TableCell<RegistrationEntry, Boolean>> regCell
                = new Callback<TableColumn<RegistrationEntry, Boolean>, TableCell<RegistrationEntry, Boolean>>() {
            @Override
            public TableCell<RegistrationEntry, Boolean> call(TableColumn p) {
                return new ToRegCell();
            }
        };

        regPrice.setCellFactory(priceCell);

        crewComment.setCellValueFactory(new Callback<CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<RegistrationEntry, String> p) {

                return new SimpleStringProperty(p.getValue().getCrewComment());
            }
        });

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> crewCellFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new CrewCommentCell();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> nameFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new NameSurnameCell();
            }
        };

        crewComment.setCellFactory(crewCellFactory);
        preNameSurname.setCellFactory(nameFactory);
    }

    private void initRegTable() {
        //  regTable.setSelectionModel(null);
        regTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        regTable.setEditable(true);
        regCompany.setEditable(true);
        regId.setCellValueFactory(
                new PropertyValueFactory<RegistrationEntry, Integer>("registrationId"));
        regNameSurname.setCellValueFactory(
                new PropertyValueFactory<RegistrationEntry, String>("nameSurname"));

        regTable.getSelectionModel().setCellSelectionEnabled(true);

        regCertNr.setCellValueFactory(new Callback<CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<RegistrationEntry, String> p) {

                return new SimpleStringProperty(p.getValue().getCertName());
            }
        });

        //   regCertGiven.setEditable(true);
        payerComment.setCellValueFactory(new Callback<CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<RegistrationEntry, String> p) {

                return new SimpleStringProperty(p.getValue().getCrewComment());
            }
        });

        payerCompany.setCellValueFactory(new Callback<CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<RegistrationEntry, String> p) {

                return new SimpleStringProperty(p.getValue().getCompany());
            }
        });

        regNameSurname.setCellValueFactory(new Callback<CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<RegistrationEntry, String> p) {

                return new SimpleStringProperty(p.getValue().getNameSurname());
            }
        });

        regBanDate.setCellValueFactory(new Callback<CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<RegistrationEntry, String> p) {
                return new SimpleStringProperty(p.getValue().getDelayDate());
            }
        });

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> certCellFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new CertNrCell();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> crewCellFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new CrewCommentCell();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> nameFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new NameSurnameCell();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> certFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new BanDateCell();
            }
        };

        regCompany.setCellValueFactory(new Callback<CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(CellDataFeatures<RegistrationEntry, String> p) {
                return new SimpleStringProperty(p.getValue().getCompany());
            }
        });

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> companyFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new CompanyCell();
            }
        };

        Callback<TableColumn<RegistrationEntry, Boolean>, TableCell<RegistrationEntry, Boolean>> certGivenFactory
                = new Callback<TableColumn<RegistrationEntry, Boolean>, TableCell<RegistrationEntry, Boolean>>() {
            @Override
            public TableCell<RegistrationEntry, Boolean> call(TableColumn p) {
                return new CertGivenCell();
            }
        };

        regCertNr.setCellFactory(certCellFactory);
        regNameSurname.setCellFactory(nameFactory);
        regCompany.setCellFactory(companyFactory);

        regBanDate.setCellFactory(certFactory);
        payerCompany.setCellFactory(companyFactory);
        payerComment.setCellFactory(crewCellFactory);

    }

    private HashMap<String, StaffEntry> selectedIns = new HashMap<>();
    private HashMap<Integer, StaffEntry> selectedInsId = new HashMap<>();
    private HashMap<String, StaffEntry> selectedInsName = new HashMap<>();

    private void fetchInstructorsList() {

        instructorsList.getItems().clear();
        //   instructorsList.getItems().add(zero.getNameSurname());
        InstructorData.getInstance().getLastInstructors().clear();

        try {
            List<StaffEntry> ilist = new ArrayList<>();
            StaffEntry se = new StaffEntry();
            se.setStaffId(0);
            se.setEvent("activeInstructorsList");
            ilist = StaffService.fetchList(se, se.getEvent());

            for (StaffEntry sse : ilist) {

                StringBuilder strb = new StringBuilder();
                strb.append(sse.getNameSurname());
                strb.append(" ");
                strb.append(sse.getPersonCode());

                InstructorData.getInstance().getLastInstructors().put(strb.toString(), sse);
                instructorsList.getItems().add(strb.toString());
                selectedIns.put(strb.toString(), sse);
                selectedInsId.put(sse.getStaffId(), sse);
                selectedInsName.put(sse.getNameSurname().trim(), sse);

            }

            SharedData.getInstance().setSelectedIns(selectedIns);
            SharedData.getInstance().setSelectedInsId(selectedInsId);
            SharedData.getInstance().setSelectedInsName(selectedInsName);

        } catch (Exception e) {
            e.printStackTrace();
            setError("Error fetching instructors list");
        }

    }

    @FXML
    protected void updateSupervisor(ActionEvent ev) {

        Task<Integer> t = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                if (supervisor.getText().length() < 3) {
                    dialogs.initUserError("Please, enter supervisor name");
                    return 0;
                }

                RegistrationEntry re = new RegistrationEntry();
                re.setEvent("_updateSaveSupervisor");
                re.setInstructor(supervisor.getText());
                RegistrationService.updateRecord(re, re.getEvent());

                return 0;
            }
        };

        Task<Integer> progress = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working");

                return 0;
            }
        };

        Task<Integer> stop = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Supervisor changed");
                return 0;
            }
        };

        Platform.runLater(progress);
        Platform.runLater(t);
        Platform.runLater(stop);

    }

    @FXML
    protected void fetchCoursesClicked(ActionEvent evt) {

        Task<Integer> task = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                try {
                    fetchCoursesList(domain.getSelectedToggle().getUserData().toString());
                    //System.out.println(domain.getSelectedToggle().getUserData().toString());
                } catch (Exception e) {
                    e.printStackTrace();

                    return 1;
                }

                return 0;

            }
        };
        task.run();
    }

    @FXML
    protected void fetchInstructorsClicked(ActionEvent evt) {
        fetchInstructorsList();
    }

    public void fetchCoursesList(String domain) {

        coursesList.getItems().clear();
        coursesList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        CourseEntry ce = new CourseEntry();
        ce.setDomain(domain);
        List<CourseEntry> fetchList = new ArrayList();

        try {
            fetchList = CourseService.fetchList(ce, "coursesByDomain");
        } catch (Exception ex) {
            dialogs.initErrorDialog(ex.getMessage());
            Logger.getLogger(GroupPage.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (CourseEntry ice : fetchList) {
            coursesList.getItems().add(ice.getCourseName());
            CourseData.getInstance().getLastCourses().put(ice.getCourseName(), ice);
            //System.out.println(ice.getCourseName() + "---" + ice.getCourseId());
        }
    }

    @FXML
    protected void fetchGroups(ActionEvent event) {
        groupsTable.getItems().clear();

        GroupEntry ge = new GroupEntry();
        ge.setEvent("fetchGroupsByCourses");

        ge.setDateStart(dateStart.getText());
        ge.setDateEnd(dateEnd.getText());

        DateTime dt = new DateTime();
        BigDecimal val = BigDecimal.valueOf(monthesPlus.getValue());

        if (dateStart.getText().isEmpty()) {
            ge.setDateStart(dt.toString(sqlDate));
        }

        if (dateEnd.getText().isEmpty() && dateStart.getText().isEmpty()) {
            val.setScale(0, RoundingMode.HALF_DOWN);

            DateTime pm = dt.plusMonths(val.intValue());
            ge.setDateEnd(pm.toString(sqlDate));

        } else if (dateEnd.getText().isEmpty() && !dateStart.getText().isEmpty()) {

            try {
                val.setScale(0, RoundingMode.HALF_DOWN);
                dt = sqlDate.parseDateTime(dateStart.getText());
                DateTime pm = dt.plusMonths(val.intValue());
                ge.setDateEnd(pm.toString(sqlDate));
            } catch (Exception e) {
                dialogs.initUserError("Invalid date format");
                //  JOptionPane.showMessageDialog(null, "Invalid date format", "Error", JOptionPane.ERROR_MESSAGE);

            }

        } else if (!dateStart.getText().isEmpty() && !dateEnd.getText().isEmpty()) {

            try {
                sqlDate.parseDateTime(dateStart.getText());
                sqlDate.parseDateTime(dateEnd.getText());
            } catch (Exception e) {
                dialogs.initUserError("Invalid date format");
                // JOptionPane.showMessageDialog(null, "Invalid date format", "Error", JOptionPane.ERROR_MESSAGE);

            }
            ge.setDateStart(dateStart.getText());
            ge.setDateEnd(dateEnd.getText());
        }

        ObservableList<String> courses = coursesList.getSelectionModel().getSelectedItems();
        List<Integer> cids = new ArrayList<>();

        ObservableList<String> allCourses = coursesList.getItems();
        ObservableList<String> apply = FXCollections.observableArrayList();
        List<GroupEntry> groupList = new ArrayList<>();

        if (courses.isEmpty()) {
            apply = allCourses;
        } else {
            apply = courses;
        }

        for (String cname : apply) {
            cids.add(CourseData.getInstance().getLastCourses().get(cname).getCourseId());
        }
        ge.setCourseIdsList(cids);

        try {
            groupList = GroupService.fetchList(ge, ge.getEvent());

            if (!groupList.isEmpty()) {
                groupsTable.getItems().addAll(groupList);
            }

        } catch (Exception e) {
            e.printStackTrace();
            setError("Error fetching groups list");

        }

    }

    @FXML
    protected void addClient(ActionEvent event) {
        // handle the event here
    }

    private List<GroupEntry> selectedGroups = new ArrayList<GroupEntry>();

    @FXML
    protected void generateGroupName(ActionEvent event) {

        Task<Integer> not = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working...");
                return 1;
            }
        };

        Task<Integer> done = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");
                return 1;
            }
        };

        Task<Integer> update = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                selectedGroup.setEvent("lastGroup");

                selectedGroups = GroupService.fetchList(selectedGroup, selectedGroup.getEvent());
                String abbr = selectedGroups.get(0).getGroupName().split("\\-")[0];
                if (selectedGroups.isEmpty()) {
                    groupName.setText(abbr.toString() + "-" + "001/" + dateTime.toString(yearDate));
                    return 1;
                }
                num = Integer.parseInt(selectedGroups.get(0).getGroupName().split("\\-")[1].split("/")[0]);
                num = num + 1;
                groupName.setText(abbr + "-" + String.format("%03d", num) + "/" + dateTime.toString(yearDate));

                return 1;
            }
        };

        Platform.runLater(not);
        Platform.runLater(update);
        Platform.runLater(done);

    }
    int num = 0;

    @FXML
    protected void renameGroup(ActionEvent event) {
        selectedGroup.setEvent("_updateChangeGroupName");
        selectedGroup.setGroupName(groupName.getText());

        Task<Integer> not = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working...");
                return 1;
            }
        };

        Task<Integer> done = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");
                return 1;
            }
        };

        Task<Integer> update = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                GroupService.updateRecord(selectedGroup, selectedGroup.getEvent());
                return 1;
            }
        };

        Platform.runLater(not);
        Platform.runLater(update);
        Platform.runLater(done);

        try {
            JournalEntry je = new JournalEntry();
            je.setUsername(System.getProperty("n3.username"));
            je.setGroupId(selectedGroup.getGroupId());
            je.setOptype("update");
            je.setSysmsg("Username " + je.getUsername() + " has changed group name to \"" + selectedGroup.getGroupName() + "\"");
            je.setEvent("_updateInsertGroupRecord");
            JournalService.insertRecord(je, je.getEvent());
        } catch (Exception ee) {
            ee.printStackTrace();
        }

    }

    private GroupEntry gia = new GroupEntry();

    @FXML
    protected void updateGroupDates(ActionEvent event) {

        gia.setDateStart(groupDateStart.getText());
        gia.setDateEnd(groupDateEnd.getText());

        Task<Integer> not = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working...");
                return 1;
            }
        };

        Task<Integer> done = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");
                return 1;
            }
        };

        Task<Integer> update = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                if (preregTable.getItems().size() > 0) {

                    for (RegistrationEntry re : preregTable.getItems()) {
                        if (!re.getDateStart().equals(groupDateStart.getText())) {
                            dialogs.initUserError("NO. Change reg.dates for preregistered users or remove/register them first.");
                            return 0;
                        }
                    }
                }

                JournalEntry je = new JournalEntry();
                je.setUsername(System.getProperty("n3.username"));
                je.setGroupId(selectedGroup.getGroupId());
                je.setOptype("update");
                je.setSysmsg("Username " + je.getUsername() + " has changed group dates to \" from " + selectedGroup.getDateStart() + " to " + selectedGroup.getDateEnd() + "\""
                        + ". Old group dates: from " + gia.getDateStart() + " to " + gia.getDateEnd());
                je.setEvent("_updateInsertGroupRecord");
                JournalService.insertRecord(je, je.getEvent());
                selectedGroup.setEvent("_updateChangeGroupDates");
                selectedGroup.setDateStart(groupDateStart.getText());
                selectedGroup.setDateEnd(groupDateEnd.getText());
                selectedGroup.setTimeStart(groupTimeStart.getText());
                selectedGroup.setRoom(groupRoom.getText());
                GroupService.updateRecord(selectedGroup, selectedGroup.getEvent());
                selectedGroup.setEvent("_updateChangeRegDates");
                GroupService.updateRecord(selectedGroup, selectedGroup.getEvent());

                return 1;
            }
        };
        Platform.runLater(not);
        Platform.runLater(update);
        Platform.runLater(done);

    }

    @FXML
    protected void addTheory(ActionEvent event) {
        StaffEntry se = selectedIns.get(instructorsList.getValue());
        selectedGroup.setInstructorName(instructorsList.getValue().replace(se.getPersonCode(), "").trim());
        selectedGroup.setInstructorId(se.getStaffId());
        selectedGroup.setInstructorType("theory");
        selectedGroup.setEvent("_updateInsertInstructors");
        theory.getItems().add(selectedGroup);

        try {
            GroupService.updateRecord(selectedGroup, selectedGroup.getEvent());

        } catch (Exception ex) {
            // Logger.getLogger(GroupPage.class.getName()).log(Level.SEVERE, null, ex);
        }
        fetchInstructorsAssigned();
    }

    @FXML
    protected void addPractice(ActionEvent event) {
        StaffEntry se = selectedIns.get(instructorsList.getValue());
        selectedGroup.setInstructorName(instructorsList.getValue().replace(se.getPersonCode(), "").trim());
        selectedGroup.setInstructorId(se.getStaffId());
        selectedGroup.setInstructorType("practice");
        selectedGroup.setEvent("_updateInsertInstructors");
        practice.getItems().add(selectedGroup);

        try {
            GroupService.updateRecord(selectedGroup, selectedGroup.getEvent());

        } catch (Exception ex) {
            // Logger.getLogger(GroupPage.class.getName()).log(Level.SEVERE, null, ex);
        }
        fetchInstructorsAssigned();
    }

    @FXML
    protected void delTheory(ActionEvent event) {
        GroupEntry se = theory.getSelectionModel().getSelectedItem();
        System.out.println(se.getInstructorRecordId());

        se.setEvent("_updateRemoveInsAss");

        try {
            GroupService.updateRecord(se, se.getEvent());

        } catch (Exception ex) {
            // Logger.getLogger(GroupPage.class.getName()).log(Level.SEVERE, null, ex);
        }
        fetchInstructorsAssigned();
    }

    @FXML
    protected void delPractice(ActionEvent event) {
        GroupEntry se = practice.getSelectionModel().getSelectedItem();

        se.setEvent("_updateRemoveInsAss");

        try {
            GroupService.updateRecord(se, se.getEvent());

        } catch (Exception ex) {
            //  Logger.getLogger(GroupPage.class.getName()).log(Level.SEVERE, null, ex);
        }
        fetchInstructorsAssigned();
    }

    private HashMap<String, GroupEntry> movableList = new HashMap<>();

    private void fetchMovableGroups() {
        movableList.clear();
        movableGroups.getItems().clear();
        GroupEntry ge = new GroupEntry();
        ge.setCourseId(selectedGroup.getCourseId());
        ge.setDateStart(selectedGroup.getDateStart());
        ge.setEvent("fetchMovableGroups");
        List<GroupEntry> glist = new ArrayList<>();

        try {
            glist = GroupService.fetchList(ge, ge.getEvent());

            for (GroupEntry gg : glist) {
                StringBuilder sb = new StringBuilder();
                sb.append(gg.getGroupName());
                sb.append(" [ ");
                sb.append(gg.getDateStart());
                sb.append(" to ");
                sb.append(gg.getDateEnd());
                sb.append(" ]");
                movableList.put(sb.toString(), gg);
                movableGroups.getItems().add(sb.toString());
            }

        } catch (Exception ee) {
            setError("Error");
        }

    }

    private void fetchInstructorsAssigned() {
        theory.getItems().clear();
        practice.getItems().clear();
        GroupEntry ge = new GroupEntry();
        ge.setGroupId(selectedGroup.getGroupId());
        ge.setEvent("fetchInstructorsAssigned");

        List<GroupEntry> glist = new ArrayList<>();

        try {
            glist = GroupService.fetchList(ge, ge.getEvent());

            for (GroupEntry gee : glist) {

                gee.setInstructorName(selectedInsId.get(gee.getInstructorId()).getNameSurname());
                gee.setAssistant1Name(selectedInsId.get(gee.getAssistant1()).getNameSurname());
                gee.setAssistant2Name(selectedInsId.get(gee.getAssistant2()).getNameSurname());
                gee.setAssistant3Name(selectedInsId.get(gee.getAssistant3()).getNameSurname());
                gee.setAssistant4Name(selectedInsId.get(gee.getAssistant4()).getNameSurname());

                if ("theory".equals(gee.getInstructorType())) {
                    theory.getItems().add(gee);
                } else if ("practice".equals(gee.getInstructorType())) {
                    practice.getItems().add(gee);
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(GroupPage.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    protected void enrollClients(ActionEvent event) {
        List<RegistrationEntry> selectedRegs = new ArrayList<RegistrationEntry>();
        selectedRegs.addAll(preregTable.getSelectionModel().getSelectedItems());
        final List<RegistrationEntry> sl = selectedRegs;

        Task<Integer> stop = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Clients registered");

                return 1;
            }
        };

        Task<Integer> start = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Registering client");
                return 1;
            }
        };

        Task<Integer> reg = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                for (RegistrationEntry re : sl) {

                    if (re.getPrice() == null || re.getPrice().trim().isEmpty()) {
                        dialogs.initUserError("NOT REGISTERED. Price for this client is EMPTY. Please, correct.");
                        continue;
                    }
                    registerClient(re);
                }
                fetchGroupData(selectedGroup);
                return 1;
            }
        };

        Platform.runLater(start);
        Platform.runLater(reg);
        Platform.runLater(stop);

    }
    private RegistrationEntry currentRegistration = new RegistrationEntry();
    private List<RegistrationEntry> selectedRegs = new ArrayList<>();

    private void registerClient(RegistrationEntry re) {
        final RegistrationEntry regEntry = re;
        GroupEntry tmp = groupsTable.getSelectionModel().getSelectedItem();
        currentRegistration = regEntry;

        currentRegistration.setGroupId(tmp.getGroupId());
        currentRegistration.setDateEnd(tmp.getDateEnd());
        currentRegistration.setDateStart(tmp.getDateStart());

        currentRegistration.setEvent("checkFutureRegistrations");

        try {
            selectedRegs = RegistrationService.fetchList(currentRegistration, currentRegistration.getEvent());
        } catch (Exception ex) {
            // Logger.getLogger(RegistrationCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (!selectedRegs.isEmpty()) {
            RegistrationEntry rer = new RegistrationEntry();
            rer = selectedRegs.get(0);
            StringBuilder tt = new StringBuilder().append(currentRegistration.getNameSurname()).append(" were already registered")
                    .append(" on date: ").append(rer.getDateStart()).append(" and course: ")
                    .append(currentRegistration.getCourseName());

            if (null != currentRegistration.getCertName()) {
                tt.append(" Cert NR: ").append(currentRegistration.getCertName());

            }
            tt.append("\nRegister client again?");
            dialogs.initConfirmDialog(tt.toString());

            if (1 == DialogState.getInstance().getState()) {
                return;
            }

        }

        JournalEntry je = new JournalEntry();
        je.setUsername(System.getProperty("n3.username"));
        je.setUserId(currentRegistration.getUserId());
        je.setGroupId(currentRegistration.getGroupId());
        je.setCourseId(currentRegistration.getCourseId());
        je.setOptype("insert");
        je.setRegistrationId(currentRegistration.getRegistrationId());
        je.setEvent("_updateInsertGroupRecord");
        je.setSysmsg("Username " + je.getUsername() + " has confirmed registration for userId=" + je.getUserId() + "");

        currentRegistration.setEvent("_updateRegisterUser");
        try {
            RegistrationService.updateRecord(currentRegistration, currentRegistration.getEvent());
            JournalService.insertRecord(je, je.getEvent());

            DateTime de = sqlDate.parseDateTime(currentRegistration.getDateEnd());
            DateTime today = sqlDate.parseDateTime(new DateTime().toString(sqlDate));

            if (today.isAfter(de)) {
                String messageText = "User " + System.getProperty("n3.username") + " has registered client <span style='color:red; font-weight: bold'>" + currentRegistration.getNameSurname() + " "
                        + currentRegistration.getPersonCode() + "</span> to some course with dateEnd=" + currentRegistration.getDateEnd();
                String subject = "Rogue registration!";
                String sendTo = "n3@novikontas.lv";
                String sendFrom = "training@novikontas.lv";
                MessageSender.sendMail(messageText, subject, sendTo, sendFrom);
            }

        } catch (Exception ex) {
            //  Logger.getLogger(RegistrationCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<RegistrationEntry> ls = new ArrayList<>();
        currentRegistration.setEvent("findPayment");

        try {
            ls = RegistrationService.fetchList(currentRegistration, currentRegistration.getEvent());

            if (ls.isEmpty()) {
                currentRegistration.setEvent("_updateInsertPaymentReg");
                RegistrationService.updateRecord(currentRegistration, currentRegistration.getEvent());
                je.setSysmsg("Payment inserted automatically for registrationId=" + currentRegistration.getRegistrationId());
                JournalService.insertRecord(je, je.getEvent());
            } else {
                je.setSysmsg("Payment was NOT inserted for registrationId=" + currentRegistration.getRegistrationId() + ". Payment record already exists (paymentId=" + ls.get(0).getPaymentId());
                JournalService.insertRecord(je, je.getEvent());
            }

        } catch (Exception ex) {
            //  Logger.getLogger(RegistrationCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }

        preregTable.getItems().remove(re);

    }

    @FXML
    private void moveToGroup(ActionEvent event) {

        String str = movableGroups.getValue();
        if (null == str) {
            dialogs.initUserError("Select group first!");
            return;
        }

        dialogs.initConfirmDialog("Sure to move selected clients?");
        Task<Integer> not = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working...");
                return 1;
            }
        };

        Task<Integer> done = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");
                return 1;
            }
        };

        Task<Integer> update = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                ObservableList<RegistrationEntry> toMove = regTable.getSelectionModel().getSelectedItems();
                Integer oldGroupId = 0;

                for (RegistrationEntry re : toMove) {
                    oldGroupId = re.getGroupId();
                    GroupEntry ge = movableList.get(movableGroups.getValue());
                    re.setEvent("_updateMoveToGroup");
                    re.setGroupId(ge.getGroupId());
                    re.setDateEnd(ge.getDateEnd());
                    re.setDateStart(ge.getDateStart());
                    RegistrationService.updateRecord(re, re.getEvent());

                    JournalEntry je = new JournalEntry();
                    je.setUsername(System.getProperty("n3.username"));
                    je.setUserId(re.getUserId());
                    je.setGroupId(re.getGroupId());
                    je.setCourseId(re.getCourseId());
                    je.setOptype("update");
                    je.setRegistrationId(re.getRegistrationId());
                    je.setEvent("_updateInsertGroupRecord");
                    je.setSysmsg("Username " + je.getUsername() + " has moved client from group " + oldGroupId + " to group " + ge.getGroupName() + " with groupId=" + ge.getGroupId());
                    JournalService.insertRecord(je, je.getEvent());

                }

                fetchGroupData(selectedGroup);
                return 1;
            }
        };

        if (DialogState.getInstance().getState() == 0) {
            Platform.runLater(not);
            Platform.runLater(update);
            Platform.runLater(done);

        }
    }

    @FXML
    protected void unregisterClients(ActionEvent event) {
        if (preregTable.getSelectionModel().getSelectedItems().isEmpty()) {
            return;
        }

        dialogs.initConfirmDialog("Sure to delete selected preregistrations?");
        Task<Integer> not = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working...");
                return 1;
            }
        };

        Task<Integer> done = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");
                return 1;
            }
        };

        Task<Integer> update = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                ObservableList<RegistrationEntry> toDelete = preregTable.getSelectionModel().getSelectedItems();
                for (RegistrationEntry re : toDelete) {
                    re.setEvent("_deletePreregistration");
                    RegistrationService.updateRecord(re, re.getEvent());

                    JournalEntry je = new JournalEntry();
                    je.setUsername(System.getProperty("n3.username"));
                    je.setUserId(re.getUserId());
                    je.setCourseId(re.getCourseId());
                    je.setOptype("delete");
                    je.setRegistrationId(re.getRegistrationId());
                    je.setEvent("_updateInsertGroupRecord");
                    je.setSysmsg("Username " + je.getUsername() + " has cancelled userId=" + re.getUserId() + " PREREGISTRATION. Registration info: registrationId="
                            + re.getRegistrationId() + " dateStart=" + re.getDateStart());
                    JournalService.insertRecord(je, je.getEvent());

                }
                fetchGroupData(selectedGroup);
                return 1;
            }
        };

        if (DialogState.getInstance().getState() == 0) {
            Platform.runLater(not);
            Platform.runLater(update);
            Platform.runLater(done);

        }

    }

    @FXML
    protected void banClient(ActionEvent event) {
        dialogs.initUserError("Not implemented");
    }

    @FXML
    protected void cancelRegistration(ActionEvent event) {

        List<RegistrationEntry> re = regTable.getSelectionModel().getSelectedItems();

        if (null == re) {
            dialogs.initUserError("Select client first");
            return;
        }

        dialogs.initConfirmDialog("Sure to cancel selected enrollments?");

        unenroll(re);

    }
    private RegistrationEntry selectedReg = new RegistrationEntry();

    private void unenroll(List<RegistrationEntry> reglist) {
        final List<RegistrationEntry> reglistF = regTable.getSelectionModel().getSelectedItems();
        //   System.out.println(reglistF.size());

        Task<Integer> not = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working...");
                return 1;
            }
        };

        Task<Integer> done = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");
                return 1;
            }
        };

        Task<Integer> update = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                for (RegistrationEntry ree : reglistF) {
                    ree.setEvent("_updateUnenrollClient");
                    RegistrationService.updateRecord(ree, ree.getEvent());
                    ree.setEvent("_updateDeletePayment");
                    RegistrationService.updateRecord(ree, ree.getEvent());
                    ree.setEvent("_updateDeleteCert");
                    RegistrationService.updateRecord(ree, ree.getEvent());

                    JournalEntry je = new JournalEntry();
                    je.setUsername(System.getProperty("n3.username"));
                    je.setUserId(ree.getUserId());
                    je.setGroupId(ree.getGroupId());
                    je.setCourseId(ree.getCourseId());
                    je.setOptype("delete");
                    je.setRegistrationId(ree.getRegistrationId());
                    je.setEvent("_updateInsertGroupRecord");
                    je.setSysmsg("Username " + je.getUsername() + " has cancelled REGISTRATION for registrationId=" + ree.getRegistrationId() + " User still remains in PREREGISTERED state.");
                    JournalService.insertRecord(je, je.getEvent());
                }
                fetchGroupData(selectedGroup);
                return 1;
            }
        };

        if (DialogState.getInstance().getState() == 0) {
            Platform.runLater(not);
            Platform.runLater(update);
            Platform.runLater(done);
        }

    }

    @FXML
    private void deleteCertificates(ActionEvent ev) {
        selectedRegs = regTable.getSelectionModel().getSelectedItems();

        for (RegistrationEntry re : selectedRegs) {
            selectedReg = re;
            deleteCerts();
        }
    }

    private void deleteCerts() {
        dialogs.initConfirmDialog("This will remove certificates. Sure?");

        Task<Integer> not = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working...");
                return 1;
            }
        };

        Task<Integer> done = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");
                return 1;
            }
        };

        Task<Integer> update = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                selectedReg.setEvent("_updateDeleteCert");
                RegistrationService.updateRecord(selectedReg, selectedReg.getEvent());
                return 1;
            }
        };

        if (DialogState.getInstance().getState() == 0) {
            Platform.runLater(not);
            Platform.runLater(update);
            Platform.runLater(done);
        }

        fetchGroupData(groupsTable.getSelectionModel().getSelectedItem());

    }

    @FXML
    protected void deleteRegistered(ActionEvent event) {
        selectedRegs = regTable.getSelectionModel().getSelectedItems();

        for (RegistrationEntry re : selectedRegs) {
            selectedReg = re;
            deleteRegistration();
        }
    }

    private void deleteRegistration() {
        dialogs.initConfirmDialog("This will remove registration COMPLETELY. Sure?");

        Task<Integer> not = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working...");
                return 1;
            }
        };

        Task<Integer> done = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");
                return 1;
            }
        };

        Task<Integer> update = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                selectedReg.setEvent("_updateDeleteRegistration");
                RegistrationService.updateRecord(selectedReg, selectedReg.getEvent());
                selectedReg.setEvent("_updateDeletePayment");
                RegistrationService.updateRecord(selectedReg, selectedReg.getEvent());
                selectedReg.setEvent("_updateDeleteCert");
                RegistrationService.updateRecord(selectedReg, selectedReg.getEvent());

                JournalEntry je = new JournalEntry();
                je.setUsername(System.getProperty("n3.username"));
                je.setUserId(selectedReg.getUserId());
                je.setGroupId(selectedReg.getGroupId());
                je.setCourseId(selectedReg.getCourseId());
                je.setOptype("delete");
                je.setRegistrationId(selectedReg.getRegistrationId());
                je.setEvent("_updateInsertGroupRecord");
                je.setSysmsg("Username " + je.getUsername() + " has COMPLETELY REMOVED registrationId=" + selectedReg.getRegistrationId());
                JournalService.insertRecord(je, je.getEvent());

                return 1;
            }
        };

        if (DialogState.getInstance().getState() == 0) {
            Platform.runLater(not);
            Platform.runLater(update);
            Platform.runLater(done);
        }

        fetchGroupData(groupsTable.getSelectionModel().getSelectedItem());

    }
    private final String nreg = ResourceBundle.getBundle("resources.urls").getString("NregDocs");
    private final String jb = ResourceBundle.getBundle("resources.urls").getString("JBdocs");

    @FXML
    protected void printDocument(ActionEvent event) {

        try {
            selectDocument();
        } catch (Exception ee) {

        }

    }

    private void selectDocument() throws Exception {
        switch (documentType.getValue()) {
            case "Preregistration form":
                try {
                    Runtime.getRuntime()
                            .exec("rundll32 url.dll,FileProtocolHandler " + nreg + "?_eventName=preRegistrationForm&groupId="
                                    + selectedGroup.getGroupId());
                } catch (Exception exception) {
                    dialogs.initErrorDialog(nreg + "?_eventName=preRegistrationForm&groupId="
                            + selectedGroup.getGroupId());
                }
                break;

            case "Registration request":
                try {
                    Runtime.getRuntime()
                            .exec("rundll32 url.dll,FileProtocolHandler " + nreg + "?groupId="
                                    + selectedGroup.getGroupId());
                } catch (Exception ee) {
                    dialogs.initErrorDialog(jb + "?paction=beig_prot&group_id="
                            + selectedGroup.getGroupId() + "&course_id=" + selectedGroup.getCourseId());
                }

                break;

            case "Finalization protocol":
                try {
                    Runtime.getRuntime()
                            .exec("rundll32 url.dll,FileProtocolHandler " + jb + "?paction=beig_prot&group_id="
                                    + selectedGroup.getGroupId() + "&course_id=" + selectedGroup.getCourseId());
                } catch (Exception ee) {
                    dialogs.initErrorDialog(jb + "?paction=beig_prot&group_id="
                            + selectedGroup.getGroupId() + "&course_id=" + selectedGroup.getCourseId());
                }

                break;

            case "List of proficiency":
                try {
                    Runtime.getRuntime()
                            .exec("rundll32 url.dll,FileProtocolHandler " + jb + "?paction=drills&group_id="
                                    + selectedGroup.getGroupId() + "&course_id=" + selectedGroup.getCourseId());
                } catch (Exception ee) {
                    dialogs.initErrorDialog(jb + "?paction=drills&group_id="
                            + selectedGroup.getGroupId() + "&course_id=" + selectedGroup.getCourseId());
                }
                break;

            case "Safety instructions list":
                try {
                    Runtime.getRuntime()
                            .exec("rundll32 url.dll,FileProtocolHandler " + jb + "?paction=safety_instr&group_id="
                                    + selectedGroup.getGroupId() + "&course_id=" + selectedGroup.getCourseId());
                } catch (Exception exception) {
                    dialogs.initErrorDialog(jb + "?paction=safety_instr&group_id="
                            + selectedGroup.getGroupId() + "&course_id=" + selectedGroup.getCourseId());
                }
                break;

            case "Port ticket":
                Runtime.getRuntime()
                        .exec("rundll32 url.dll,FileProtocolHandler " + jb + "?paction=ticket&group_id="
                                + selectedGroup.getGroupId() + "&course_id=" + selectedGroup.getCourseId());
                break;

            case "Certificate of proficiency":
                dialogs.initUserError("Sorry, not implemented");
                break;

            case "NVP":

                if (null == regTable.getSelectionModel().getSelectedItem()) {
                    dialogs.initUserError("Please, select client in table!");
                    return;
                }

                Runtime.getRuntime()
                        .exec("rundll32 url.dll,FileProtocolHandler " + jb + "?paction=nvp&group_id="
                                + selectedGroup.getGroupId() + "&user_id="
                                + regTable.getSelectionModel().getSelectedItem().getUserId().toString());
                break;

        }
    }

    @FXML
    protected void newRegistration(ActionEvent event) {
        final String dialog = "/n3/dialogs/RegistrationCalendar.fxml";
        final String tt = "nReg 3.0 - Calendar view";
        try {
            dialogs.switchPage((Stage) coursesList.getScene().getWindow(), dialog);
        } catch (Exception ex) {
            dialogs.initErrorDialog(ex.toString());
            // JOptionPane.showMessageDialog(null, ex.getMessage(), "IO Exception", JOptionPane.ERROR_MESSAGE);
        }
    }

    @FXML
    protected void switchGiveCerts(ActionEvent ev) {
        SharedData.getInstance().setGiveCerts(checkCertificate.isSelected());
    }

    @FXML
    private void addPayment(ActionEvent ev) {

    }

    private void setProgress(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: green");
        progressBar.setProgress(-1D);
    }

    private void setError(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: red");
        progressBar.setProgress(1D);
    }

    private void setMessage(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: green");
        progressBar.setProgress(1D);
    }

    @FXML
    private void f5Pressed(KeyEvent ev) {

        if (ev.getCode().equals(KeyCode.F5)) {
            fetchData();

        }
    }

}
