/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.groupstable;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import lv.nmc.entities.GroupEntry;
import lv.nmc.rest.GroupService;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;

/**
 *
 * @author User
 */
public class FinProtCell extends TableCell<GroupEntry, Boolean> {

    private CheckBox checkBox;
    private WinLoader dialogs = new WinLoader();

    public FinProtCell() {

        checkBox = new CheckBox();
        checkBox.setDisable(true);
        checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (isEditing()) {
                    commitEdit(newValue == null ? false : newValue);
                }
            }
        });
        this.setGraphic(checkBox);
        this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.setEditable(true);


    }

    @Override
    public void startEdit() {
        super.startEdit();
        if (isEmpty()) {
            return;
        }
        checkBox.setDisable(false);
        checkBox.requestFocus();

    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        checkBox.setDisable(true);
    }

    @Override
    public void commitEdit(Boolean value) {
        super.commitEdit(value);
        final Boolean val = value;
        checkBox.setDisable(true);
        
        dialogs.initConfirmDialog("Update record?");        
        int c = DialogState.getInstance().getState();
        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        GroupEntry ge = (GroupEntry) getTableRow().getTableView().getSelectionModel().getSelectedItem();
                        ge.setEvent("_updateFinProtPrinted");
                        ge.setFinPrinted(val);
                        GroupService.updateRecord(ge, ge.getEvent());
                        return 0;
                    }
                };
            }
        };
        if (c == 0) {
            ss.start();
        } else {
            checkBox.setSelected(!value);
        }
    }

    @Override
    public void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);
        if (!isEmpty()) {
            checkBox.setSelected(item);
        }
    }
}