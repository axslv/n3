/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import lv.nmc.entities.CourseEntry;
import lv.nmc.entities.DocumentEntry;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.rest.CourseService;
import lv.nmc.rest.DocumentService;
import lv.nmc.rest.RegistrationService;
import lv.nmc.shared.SharedData;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author User
 */
public class CertNrCell extends TableCell<RegistrationEntry, String> {

    private Hyperlink certLink;
    private WinLoader dialogs = new WinLoader();
    private int state;
    private final String jb = ResourceBundle.getBundle("resources.urls").getString("JBdocs");
    private BigDecimal payCompleted = BigDecimal.valueOf(0.00);
    private final DateTimeFormatter sqlDate = DateTimeFormat.forPattern("yyyy-MM-dd");

    public CertNrCell() {
        createHyperlink();
    }

    @Override
    public void updateItem(String item, boolean empty) {
        final String[] colors = {"#000080;", "#008000;"};
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {
                if (certLink != null) {
                    certLink.setText(getString());
                }
                
                setText(null);
                setGraphic(certLink);
            } else { 

                try {
                    String[] given = getString().split("\\_");
                    certLink.setText(given[0]);
                    state = Integer.parseInt(given[1]);
                    certLink.setStyle("-fx-text-fill: " + colors[state] + ";  -fx-border-width: 0px");
                    setGraphic(certLink);
                } catch (Exception ee) {

                }
            }
        }
    }

    private void createHyperlink() {

        certLink = new Hyperlink(getString());
        

        certLink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (!SharedData.getInstance().getGiveCerts()) {
                    openCertificate();
                    return;
                }

                List<RegistrationEntry> ls = new ArrayList<>();
                RegistrationEntry ge = (RegistrationEntry) getTableRow().getTableView().getItems().get(getIndex());

                try {

                    CourseEntry ce = new CourseEntry();
                    ce.setCourseId(ge.getCourseId());
                    ce.setEvent("courseDocuments");
                    List<CourseEntry> lsce = CourseService.fetchList(ce, ce.getEvent());

                    ge.setEvent("checkClientDocuments");
                    ls = RegistrationService.fetchList(ge, ge.getEvent());

                    if (lsce.size() != ls.size()) {
                        ArrayList<String> courseDocs = new ArrayList<>();
                        ArrayList<String> clientDocs = new ArrayList<>();

                        for (CourseEntry reg : lsce) {
                            courseDocs.add(reg.getDocumentName());
                        }

                        for (RegistrationEntry client : ls) {
                            clientDocs.add(client.getDocumentName());
                        }

                        courseDocs.removeAll(clientDocs);

                        dialogs.initConfirmDialog("Please, recheck client's documents. Couldn't find \n\n" + courseDocs.toString() + "\n\nIgnore this and give certificate?");
                        if (1 == DialogState.getInstance().getState()) {
                            return;
                        }

                    }

                    ge.setEvent("checkExamPassed");
                    ls = RegistrationService.fetchList(ge, ge.getEvent());
                    if (ls.isEmpty()) {
                        dialogs.initWarning("Exam record not found. Contact instructor.");
                        return;
                    }

                    RegistrationEntry ch = ls.get(0);

                    if ("not passed".equals(ch.getExamState())) {
                        dialogs.initWarning("Exam not passed. Can't give certificate.");
                        return;
                    }

                    ge.setEvent("checkPaymentsCompleted");
                    ls = RegistrationService.fetchList(ge, ge.getEvent());

                    if (ls.isEmpty()) {
                        dialogs.initWarning("Something went wrong. Process record manually or contact dba.");
                        return;
                    }
                    System.out.println(ls.get(0).getPaymentsComment());

                    if (null == ls.get(0).getPaymentId()) {

                        String ptext = "Payment for this course not found!";

                        
                        
                        if (null == ls.get(0).getPaymentsComment()) {
                            dialogs.initUserError("Comment is empty");
                            return;
                        }
                        
                        if (ls.get(0).getPaymentsComment().contains("apm")) {
                            certGiven();
                            return;
                        }
                        
                        if (ls.get(0).getPaymentsComment().contains("pays")) {
                            certGiven();
                            return;
                        }
                        
                      /*  if (null != ls.get(0).getPaymentsComment() && !ls.get(0).getPaymentsComment().trim().isEmpty()) {
                            ptext = ptext + "\nHowever, comment is found:\n\n\"" + ls.get(0).getPaymentsComment() + "\"\n\nGIVE CERTIFICATE?";
                            dialogs.initConfirmDialog(ptext);

                            if (0 == DialogState.getInstance().getState()) {
                                certGiven();
                            }
                            return;
                        }    */                    

                        dialogs.initWarning(ptext);
                        System.out.println(ls.get(0).getPaymentsComment());
                        return;
                    }

                    ch = ls.get(0);
                    BigDecimal coursePrice = BigDecimal.valueOf(Double.parseDouble(ch.getcPrice().split(" ")[0]));

                    for (RegistrationEntry re : ls) {
                        String[] prcs = re.getPrice().split(" ");
                        BigDecimal aug = BigDecimal.valueOf(Double.parseDouble(prcs[0]));
                        payCompleted = payCompleted.add(aug);
                    }

                    if (payCompleted.compareTo(coursePrice) < 0) {
                        dialogs.initConfirmDialog("Client paid only " + payCompleted.toString() + " of " + coursePrice.toString() + ""
                                + "\n\nGIVE CERTIFICATE?");
                        payCompleted = new BigDecimal(0.00);
                        if (0 == DialogState.getInstance().getState()) {
                            certGiven();
                        }
                        return;
                    }

                    dialogs.initConfirmDialog("Exam OK, payments OK, Documents OK. You can issue certificate to this client.\n\nGIVE CERTIFICATE?");

                    ge.setEvent("checkCertificate");
                    ge.setCertName(certLink.getText());

 
                    if (0 == DialogState.getInstance().getState()) {
                        certGiven();
                    }

                    payCompleted = new BigDecimal(0.00);

                } catch (Exception ex) {
                    Logger.getLogger(CertNrCell.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

    }

    private void certGiven() {
        
        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        RegistrationEntry ge = (RegistrationEntry) getTableRow().getTableView().getItems().get(getIndex());
                        ge.setEvent("_updateGiveCertificate");
                        RegistrationService.updateRecord(ge, ge.getEvent());
                        certLink.visitedProperty().setValue(Boolean.FALSE);
                        certLink.setStyle("-fx-text-fill: #008000;");
                        registerCert();
                        return 0;
                    }
                    
                    private void registerCert() throws Exception {
                        RegistrationEntry ge = (RegistrationEntry) getTableRow().getTableView().getItems().get(getIndex());
                        DocumentEntry de = new DocumentEntry();
                        de.setCourseId(ge.getCourseId());
                        de.setDocumentNr(ge.getCertName().split("_")[0]);
                        de.setIssuer("Novikontas");
                        de.setIssueDate(ge.getDateEnd());
                        de.setUserId(ge.getUserId());
                        de.setValidTill("2100-01-01");
                        
                        if (Integer.parseInt(ge.getValidTill()) != 0) {
                            String vt = sqlDate.parseDateTime(ge.getDateEnd()).plusYears(Integer.parseInt(ge.getValidTill())).toString(sqlDate);
                            de.setValidTill(vt);
                        }
                        
                        de.setEvent("_updateInsertDocument");
                        DocumentService.updateRecord(de, de.getEvent());
                         
                        System.out.println(de.getIssueDate());
                    }
                };
            }
        }; 
        ss.start();
    }

    private void openCertificate() {
        RegistrationEntry ge = (RegistrationEntry) getTableRow().getTableView().getItems().get(getIndex());
        try {
            Clipboard cp = Clipboard.getSystemClipboard();
            ClipboardContent cb = new ClipboardContent();
            cp.setContent(cb);
            System.out.println(jb + "?paction=print_cert&u_"
                            + ge.getUserId() + "=" + ge.getUserId() + "&course_id=" + ge.getCourseId()
                            + "&cuser_" + ge.getUserId() + "=" + ge.getUserId() + "&group_id=" + ge.getGroupId());
            cb.putString(jb + "?paction=print_cert&u_"
                            + ge.getUserId() + "=" + ge.getUserId() + "&course_id=" + ge.getCourseId()
                            + "&cuser_" + ge.getUserId() + "=" + ge.getUserId() + "&group_id=" + ge.getGroupId());
            Runtime.getRuntime()
                    .exec("rundll32 url.dll,FileProtocolHandler "
                            + jb + "?paction=print_cert&u_"
                            + ge.getUserId() + "=" + ge.getUserId() + "&course_id=" + ge.getCourseId()
                            + "&cuser_" + ge.getUserId() + "=" + ge.getUserId() + "&group_id=" + ge.getGroupId());
            certLink.visitedProperty().setValue(Boolean.TRUE);
        } catch (IOException ex) {
            dialogs.initWarning("Non-windows system. Certificate link has been copied to clipboard. Please, paste it in browser's address bar yourself.");
            Logger.getLogger(CertNrCell.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }
}
