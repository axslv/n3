/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lv.nmc.entities.GroupEntry;
import lv.nmc.entities.JournalEntry;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.rest.GroupService;
import lv.nmc.rest.JournalService;
import lv.nmc.rest.RegistrationService;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;

/**
 *
 * @author User
 */
public class DateStartCell extends TableCell<RegistrationEntry, String> {

    private TextField textField;
    private boolean mark = false;
    private WinLoader wl = new WinLoader();

    public DateStartCell() {

    }

    @Override
    public void startEdit() {
        super.startEdit();
        this.mark = false;
        if (textField == null) {
            createTextField();
        }
        setGraphic(textField);
        textField.setStyle("-fx-text-fill: #FF0000");
        textField.selectAll();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText((String) getItem());
        setGraphic(null);

        setStyle("-fx-text-fill: #FF8000");
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());

                }
                setText(null);
                setGraphic(textField);
            } else {
                setText(getString());
                setGraphic(null);
            }
        }

    }
private boolean noGroup = false;
    @Override
    public void commitEdit(String t) {
        this.mark = true;
        final String val = t;
        updateItem(t, false);
       RegistrationEntry ge = (RegistrationEntry) getTableRow().getItem();
        GroupEntry group = new GroupEntry();
        group.setDateStart(val);
        group.setCourseId(ge.getCourseId());
        group.setEvent("groupByDateStart");

        List<GroupEntry> gr = new ArrayList<>();
        try {
            gr = GroupService.fetchList(group, group.getEvent());
        } catch (Exception ex) {
            Logger.getLogger(DateStartCell.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (gr.isEmpty()) {
            WinLoader wl2 = new WinLoader();
            wl2.initConfirmDialog("Group with specified date and course not found. You have to create group later. Sure to change registration date?");
            if (DialogState.getInstance().getState() == 1) {
                return;
            }
            noGroup = true;
        }

        try {
            wl.initConfirmDialog("Sure to update?");
        } catch (Exception ex) {
            Logger.getLogger(DateStartCell.class.getName()).log(Level.SEVERE, null, ex);
        }

        int c = wl.getResult();

        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        RegistrationEntry re = (RegistrationEntry) getTableRow().getItem();
                        
                        JournalEntry je = new JournalEntry();
                        je.setUsername(System.getProperty("n3.username"));
                        je.setRegistrationId(re.getRegistrationId());
                        je.setOptype("update");
                        je.setSysmsg("Username " + je.getUsername() + " has changed client's registration date to " + val);
                        
                        if (noGroup) {
                            je.setSysmsg(je.getSysmsg() + ". WARNING! Group with date " + val + " still doesn't exist!");
                        }
                        
                        je.setEvent("_updateUpdateRecord");
                        JournalService.insertRecord(je, je.getEvent());

                        
                        re.setDateC(val);
                        re.setDateStart(val);
                        RegistrationService.updateRecord(re, "_updateChangeRegdate");
                        return 0;
                    }
                };
            }
        };
        if (c == 0) {
            ss.start();
            setStyle("-fx-text-fill: #008000");
        }

    }

    private void createTextField() {
        textField = new TextField(getString());
        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if (t.getCode() == KeyCode.ENTER) {
                    commitEdit(textField.getText());
                } else if (t.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }
            }
        });

        textField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue.booleanValue() && !mark && !textField.isHover()) {
                    cancelEdit();
                }
            }

        });

    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

}
