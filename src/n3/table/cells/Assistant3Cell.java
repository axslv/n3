/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import lv.nmc.entities.GroupEntry;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.entities.StaffEntry;
import lv.nmc.rest.GroupService;
import lv.nmc.rest.StaffService;
import lv.nmc.shared.SharedData;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;

/**
 *
 * @author User
 */
public class Assistant3Cell extends TableCell<GroupEntry, String> {

    private ComboBox<String> comboBox;
    private boolean mark = false;
    private List<RegistrationEntry> instructorsList = new ArrayList<>();
    private List<StaffEntry> staffList = new ArrayList<>();
    private String oldvalue = "";
    private WinLoader dialogs = new WinLoader();

    public Assistant3Cell() {
    }

    @Override
    public void startEdit() {


        oldvalue = getString();
        super.startEdit();
        this.mark = false;
        if (comboBox == null) {
            createTextField();
        }
        setGraphic(comboBox);
        comboBox.setStyle("-fx-text-fill: #FF0000");
        comboBox.setMinWidth(256);
        //textField.selectAll();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setText(getItem());
        setGraphic(null);
        setStyle("-fx-text-fill: #FF0000");
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {
                if (comboBox != null) {
                    comboBox.setPromptText(getString());

                }
                setText(null);
                setGraphic(comboBox);
            } else {
                setText(getString());
                setGraphic(null);
            }
        }
    }

    @Override
    public void commitEdit(String t) {
        super.commitEdit(t);
        this.mark = true;
        final String val = comboBox.getSelectionModel().getSelectedItem();
        updateItem(t, false);

        dialogs.initConfirmDialog("Update record?");

        int c = DialogState.getInstance().getState();
        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        GroupEntry ge = (GroupEntry) getTableRow().getTableView().getItems().get(getIndex());                       
                        ge.setEvent("_updateChangeInsAssistants3");
                        StaffEntry get = SharedData.getInstance().getSelectedInsName().get(val.trim());                        
                        ge.setAssistant3(get.getStaffId()); 
                
                        GroupService.updateRecord(ge, ge.getEvent());
                        return 0;
                    }
                };
            }
        };
        if (c == 0) {
            ss.start();
        } else {
            setStyle("-fx-text-fill: #FF8000");
            setText(oldvalue);
        }


    }

    private void createTextField() {
        comboBox = new ComboBox<>();
        comboBox.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);

        Task ss;
        ss = new Task() {
            @Override
            protected Object call() throws Exception {
                StaffEntry se = new StaffEntry();
                se.setStaffId(0);
                se.setEvent("activeInstructorsList");




                try {

                    staffList = StaffService.fetchList(se, se.getEvent());

                    for (StaffEntry inner : staffList) {
                        comboBox.getItems().add(inner.getNameSurname());
                        //Companies.getInstance().getLastCompanies().put(inner.getCompany(), inner);

                    }
                    comboBox.setPromptText("Select Instructor");

                    return 0;

                } catch (Exception ex) {
                    Logger.getLogger(Assistant3Cell.class.getName()).log(Level.SEVERE, null, ex);
                    return 1;
                }
            }
        };

        Platform.runLater(ss);

        comboBox.setMinWidth(256d);
        comboBox.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue.booleanValue() && !mark && !comboBox.isHover()) {
                    //cancelEdit();
                    commitEdit(comboBox.getSelectionModel().getSelectedItem());
                }
            }
        });

    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }
}