/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import lv.nmc.entities.JournalEntry;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.rest.JournalService;
import lv.nmc.rest.RegistrationService;
import lv.nmc.shared.Companies;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;

/**
 *
 * @author User
 */
public class CompanyCell extends TableCell<RegistrationEntry, String> {

    private ComboBox<String> comboBox;
    private boolean mark = false;
    private List<RegistrationEntry> companiesList = new ArrayList<>();
    private String oldvalue = "";
    private WinLoader dialogs = new WinLoader();

    public CompanyCell() {
    }

    @Override
    public void startEdit() {   
       
        
        oldvalue = getString();
        super.startEdit();
        this.mark = false;
        if (comboBox == null) {
            createTextField();
        }
        setGraphic(comboBox);
        comboBox.setStyle("-fx-text-fill: #FF0000");
        comboBox.setMinWidth(128);
        //textField.selectAll();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setText(getItem());
        setGraphic(null);
        setStyle("-fx-text-fill: #FF0000");
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {
                if (comboBox != null) {
                    comboBox.setPromptText(getString());

                }
                setText(null);
                setGraphic(comboBox);
            } else {
                setText(getString());
                setGraphic(null);
            }
        }


    }

    @Override
    public void commitEdit(String t) {
        super.commitEdit(t);
        this.mark = true;
        final String val = comboBox.getEditor().getText();
        updateItem(t, false);

        dialogs.initConfirmDialog("Update record?");

        int c = DialogState.getInstance().getState();
        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        
                        if (null == val) {
                            
                            return 1;
                        }
                        
                        if (val.trim().isEmpty()) {
                            
                            return 1;
                        }
                        
                        RegistrationEntry ge = (RegistrationEntry) getTableRow().getItem();

                        RegistrationService.updateRecord(ge, "_deleteCrewComment");
                        ge.setCompany(val);
                        ge.setCompanyId(Companies.getInstance().getLastCompanies().get(val).getCompanyId());                        
                        JournalEntry je = new JournalEntry();
                        je.setUsername(System.getProperty("n3.username"));
                        je.setRegistrationId(ge.getRegistrationId());
                        je.setOptype("update");
                        je.setSysmsg("Username " + je.getUsername() + " has changed client's company to \"" + val + "\"");                   
                        je.setEvent("_updateUpdateRecord");
                        JournalService.insertRecord(je, je.getEvent());
                        RegistrationService.updateRecord(ge, "_insertCrewComment");                        
                        RegistrationService.updateRecord(ge, "_updateChangePaymentsCompany");
                        setStyle("-fx-text-fill: #008000");
                        return 0;
                    }
                };
            }
        };
        if (c == 0) {
            ss.start();
        } else {
            setStyle("-fx-text-fill: #FF8000");
            //setText(oldvalue);
        }


    }

    private void createTextField() {
        comboBox = new ComboBox();
        comboBox.setEditable(true);
        comboBox.setMaxWidth(48d);
        AutoCompleteComboBoxListener lst = new AutoCompleteComboBoxListener(comboBox);        

        Task ss;
        ss = new Task() {
            @Override
            protected Object call() throws Exception {
               
                try {
                   
                    for (RegistrationEntry inner : Companies.getInstance().getCompaniesList()) {
                        comboBox.getItems().add(inner.getCompany());                        
                    }
                    comboBox.setPromptText("Select company");

                    return 0;

                } catch (Exception ex) {
                    Logger.getLogger(CompanyCell.class.getName()).log(Level.SEVERE, null, ex);
                    return 1;
                }
            }
        };

        Platform.runLater(ss);
        
        comboBox.setMinWidth(256d);
        comboBox.getEditor().focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue.booleanValue() && !mark) {
                    commitEdit(comboBox.getEditor().getText());
                }
            }
        });

    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }
}