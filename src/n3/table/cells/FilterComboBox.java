package n3.table.cells;

import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class FilterComboBox.
 */
public class FilterComboBox extends ComboBox< String > 
{

    /** The items. */
  //  private ObservableList< String >    items = getItems();

    /**
     * The Class KeyHandler.
     */
    private class KeyHandler implements EventHandler< KeyEvent > 
    {

        /** The sm. */
        private SingleSelectionModel< String >  sm;

        /** The s. */
        private String                          s;

        /**
         * Instantiates a new key handler.
         */
        public KeyHandler() 
        {
            sm = getSelectionModel();
            s = "";
        }

        /* (non-Javadoc)
         * @see javafx.event.EventHandler#handle(javafx.event.Event)
         */
        @Override
        public void handle( KeyEvent event ) 
        {
            // handle non alphanumeric keys like backspace, delete etc
            if( event.getCode() == KeyCode.BACK_SPACE && s.length()>0)
            {
                s = s.substring( 0, s.length() - 1 );
            }
            else if(event.getCode() != KeyCode.TAB )
            {
                s += event.getText();
            }

            if( s.length() == 0 ) 
            {
                sm.selectFirst();
                return;
            }
        //   System.out.println("ncc " + s );
            for( String item: getItems() ) 
            {
                if( item.startsWith( s ) || item.startsWith(s.toUpperCase())) 
                {
                    
                    sm.select( item );
                    
                    return;
                }
            }
        }

    }
    
    public FilterComboBox( ) 
    {
        super(  );
       // this.items = items;
        setOnKeyReleased( new KeyHandler() );
    }
    
}