/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lv.nmc.entities.JournalEntry;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.rest.JournalService;
import lv.nmc.rest.RegistrationService;
import n3.WinLoader;

/**
 *
 * @author User
 */
public class PriceCell extends TableCell<RegistrationEntry, String> {

    private TextField textField;
    private boolean mark = false;

    public PriceCell() {
        
    }

    @Override
    public void startEdit() {
        super.startEdit();
        this.mark = false;
        if (textField == null) {
            createTextField();
        }
        setGraphic(textField);
        textField.setStyle("-fx-text-fill: #FF0000");
        textField.selectAll();
    }

    @Override
    public void cancelEdit() {      
        super.cancelEdit();
        
        
        
        setText((String) getItem());
        setGraphic(null);
        
        setStyle("-fx-text-fill: #FF8000");
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());

                }
                setText(null);
                setGraphic(textField);
            } else {
                setText(getString());
                setGraphic(null);
            }
        }

    }

    @Override
    public void commitEdit(String t) {       
        this.mark = true;
        final String val = t;
        updateItem(t, false);
        
        
        
                WinLoader wl = new WinLoader();
        try {
            wl.initConfirmDialog("Sure to update?");
        } catch (Exception ex) {            
            Logger.getLogger(PriceCell.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int c = wl.getResult();
                
        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        
                        
                        RegistrationEntry ge = (RegistrationEntry) getTableRow().getItem();                        
                        RegistrationService.updateRecord(ge, "_deleteTmpDiscount"); 
                        ge.setPrice(val); 
                        RegistrationService.updateRecord(ge, "_insertTmpDiscount");
                        
                        JournalEntry je = new JournalEntry();
                        je.setUsername(System.getProperty("n3.username"));
                        je.setRegistrationId(ge.getRegistrationId());
                        je.setOptype("update");
                        je.setSysmsg("Username " + je.getUsername() + " has changed price to pay. New value: \"" + val + "\"");                   
                        je.setEvent("_updateUpdateRecord");
                        JournalService.insertRecord(je, je.getEvent());
                        
                        return 0;
                    }
                };
            }
        };
        if (c==0) {
            ss.start();
            setStyle("-fx-text-fill: #008000");    
        }
        
        
    }

    private void createTextField() {
        textField = new TextField(getString());
        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if (t.getCode() == KeyCode.ENTER) {
                    commitEdit(textField.getText());
                } else if (t.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }
            }
        });

        textField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue.booleanValue() && !mark && !textField.isHover()) {
                    cancelEdit();                   
                }
            }           
            
        });   
       

    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }
    
}