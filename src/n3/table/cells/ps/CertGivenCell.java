package n3.table.cells.ps;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import lv.nmc.entities.CourseEntry;
import lv.nmc.entities.DocumentEntry;
import lv.nmc.entities.JournalEntry;
import lv.nmc.entities.Payment;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.rest.CourseService;
import lv.nmc.rest.DocumentService;
import lv.nmc.rest.JournalService;
import lv.nmc.rest.PaymentService;
import lv.nmc.rest.RegistrationService;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;
import n3.table.cells.CertNrCell;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author User
 */
public class CertGivenCell extends TableCell<Payment, Boolean> {

    private CheckBox checkBox;
    private WinLoader dialogs = new WinLoader();

    public CertGivenCell() {

        checkBox = new CheckBox();
        //  Payment ge = (Payment) getTableRow().getTableView().getItems().get(getIndex());                  

        checkBox.setDisable(true);
        checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (isEditing()) {
                    commitEdit(newValue == null ? false : newValue);
                }
            }
        });

        this.setGraphic(checkBox);
        this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.setEditable(true);
    }

    @Override
    public void startEdit() {
        super.startEdit();
        if (isEmpty()) {
            return;
        }
        checkBox.setDisable(false);
        checkBox.requestFocus();

    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        checkBox.setDisable(true);
    }

    private final String jb = ResourceBundle.getBundle("resources.urls").getString("JBdocs");
    private BigDecimal payCompleted = BigDecimal.valueOf(0.00);

    @Override
    public void commitEdit(Boolean value) {
        super.commitEdit(value);
        checkBox.setDisable(true);
        final Boolean val = value;
        dialogs.initConfirmDialog("Update record?");

        Payment ge = (Payment) getTableRow().getItem();

        try {

            CourseEntry ce = new CourseEntry();
            ce.setCourseId(ge.getCourseId());
            ce.setEvent("courseDocuments");
            List<CourseEntry> lsce = CourseService.fetchList(ce, ce.getEvent());
            List<RegistrationEntry> ls = new ArrayList<>();

            RegistrationEntry re = new RegistrationEntry();
            re.setUserId(ge.getUserId());
            re.setEvent("checkClientDocuments");
            re.setCourseId(ge.getCourseId());
            re.setRegistrationId(ge.getRegistrationId());

            ls = RegistrationService.fetchList(re, re.getEvent());

            if (lsce.size() != ls.size()) {
                ArrayList<String> courseDocs = new ArrayList<>();
                ArrayList<String> clientDocs = new ArrayList<>();

                for (CourseEntry reg : lsce) {
                    courseDocs.add(reg.getDocumentName());
                }

                for (RegistrationEntry client : ls) {
                    clientDocs.add(client.getDocumentName());
                }

                courseDocs.removeAll(clientDocs);

                dialogs.initConfirmDialog("Please, recheck client's documents. Couldn't find \n\n" + courseDocs.toString() + "\n\nIgnore this and give certificate?");
                if (1 == DialogState.getInstance().getState()) {
                    return;
                }

            }

            re.setEvent("checkExamPassed");
            ls = RegistrationService.fetchList(re, re.getEvent());
            if (ls.isEmpty()) {
                dialogs.initWarning("Exam record not found. Contact instructor.");
                return;
            }

            RegistrationEntry ch = ls.get(0);

            if ("not passed".equals(ch.getExamState())) {
                dialogs.initWarning("Exam not passed. Can't give certificate.");
                return;
            }

            re.setEvent("checkPaymentsCompleted");
            ls = RegistrationService.fetchList(re, re.getEvent());

            if (ls.isEmpty()) {
                dialogs.initWarning("Something went wrong. Process record manually or contact dba.");
                return;
            }
            // System.out.println(ls.get(0).getPaymentsComment());

            if (null == ls.get(0).getPaymentId()) {

                String ptext = "Payment for this course not found!";

                if (null == ls.get(0).getPaymentsComment()) {
                    dialogs.initUserError("Comment is empty");
                    return;
                }

                if (ls.get(0).getPaymentsComment().contains("apm")) {
                    certGiven();
                   return;
                }

                if (ls.get(0).getPaymentsComment().contains("pays")) {
                    certGiven();
                   return;
                }
                dialogs.initWarning(ptext);
                return;
                // System.out.println(ls.get(0).getPaymentsComment());
               
            }

            ch = ls.get(0);
            BigDecimal coursePrice = BigDecimal.valueOf(Double.parseDouble(ch.getcPrice().split(" ")[0]));

            for (RegistrationEntry ree : ls) {
                String[] prcs = ree.getPrice().split(" ");
                BigDecimal aug = BigDecimal.valueOf(Double.parseDouble(prcs[0]));
                payCompleted = payCompleted.add(aug);
            }

            if (payCompleted.compareTo(coursePrice) < 0) {
                dialogs.initConfirmDialog("Client paid only " + payCompleted.toString() + " of " + coursePrice.toString() + ""
                        + "\n\nGIVE CERTIFICATE?");
                payCompleted = new BigDecimal(0.00);
                if (0 == DialogState.getInstance().getState()) {
                    certGiven();
                }
                return;
            }

            dialogs.initConfirmDialog("Exam OK, payments OK, Documents OK. You can issue certificate to this client.\n\nGIVE CERTIFICATE?");

            /* ge.setEvent("checkCertificate");
             ge.setCertName(certLink.getText());*/
            if (1 == DialogState.getInstance().getState()) {
                return;
            }
            
            //@TODO
            //Dobavitj zanesenie dokumenta v bd pri vidache serta.
            
            payCompleted = new BigDecimal(0.00);

        } catch (Exception ex) {
            Logger.getLogger(CertNrCell.class.getName()).log(Level.SEVERE, null, ex);
        }

        certGiven();
        ge.setCrtIssued(val);
        try {
            PaymentService.updateRecord(ge, "_updateCertGiven");
        } catch (Exception ex) {
            Logger.getLogger(CertGivenCell.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private final DateTimeFormatter sqlDate = DateTimeFormat.forPattern("yyyy-MM-dd");

    private void certGiven() {
        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        Payment pe = (Payment) getTableRow().getTableView().getItems().get(getIndex());
                        pe.setEvent("selectCert");
                        List<Payment> ls = new ArrayList<>();
                        ls = PaymentService.fetchList(pe, pe.getEvent());
                        RegistrationEntry ge = new RegistrationEntry();
                        DocumentEntry de = new DocumentEntry();
                        de.setCourseId(pe.getCourseId());
                        de.setDocumentNr(ls.get(0).getCertNr());
                        de.setIssuer("Novikontas");
                        de.setIssueDate(ge.getDateEnd());
                        de.setUserId(ge.getUserId());
                       // de.setValidTill(new DateTime().plusYears(Integer.parseInt(ls.get(0).getDateEnd())).toString(sqlDate));

                        if (Integer.parseInt(ge.getValidTill()) != 0) {
                            String vt = sqlDate.parseDateTime(ge.getDateEnd()).plusYears(Integer.parseInt(ge.getValidTill())).toString(sqlDate);
                            de.setValidTill(vt);
                        }

                        de.setEvent("_updateInsertDocument");
                        DocumentService.updateRecord(de, de.getEvent());
                        
                        JournalEntry je = new JournalEntry();
                        je.setUsername(System.getProperty("n3.username"));
                        je.setOptype("insert");
                        je.setRegistrationId(pe.getRegistrationId());
                        je.setUserId(pe.getUserId());
                        je.setSysmsg("Username " + je.getUsername() + " has given certificate No: " + de.getDocumentNr() + " to client " + pe.getNameSurname() + "//" + pe.getPersonCode());
                        JournalService.insertRecord(je, je.getEvent());
                        

                        //System.out.println(de.getIssueDate());
                        return 0;
                    }
                };
            }
        };
        ss.start();
    }

    @Override
    public void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);
        if (!isEmpty()) {
            checkBox.setSelected(item);
        }
    }
}
