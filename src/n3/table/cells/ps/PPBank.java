/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells.ps;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import n3.table.cells.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import lv.nmc.entities.Payment;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.rest.PaymentService;
import lv.nmc.shared.Companies;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;

/**
 *
 * @author User
 */
public class PPBank extends TableCell<Payment, String> {

    private TextField textField = new TextField();
    private boolean mark = false;
    private WinLoader dialogs = new WinLoader();
    private ComboBox<String> comboBox;
    List<Payment> banks = new ArrayList<>();
    private String oldvalue = "";

    public PPBank() {

    }

    @Override
    public void startEdit() {
        oldvalue = getString();
        super.startEdit();
        this.mark = false;
        if (comboBox == null) {
            createTextField();
        }
        setGraphic(comboBox);
        comboBox.setStyle("-fx-text-fill: #FF0000");
        comboBox.setMinWidth(256);
        //textField.selectAll();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setText(getItem());
        setGraphic(null);
        setStyle("-fx-text-fill: #FF0000");
    }

    @Override
    public void updateItem(String item, boolean empty) {
         super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {
                if (comboBox != null) {
                    comboBox.setPromptText(getString());

                }
                setText(null);
                setGraphic(comboBox);
            } else {
                setText(getString());
                setGraphic(null);
            }
        }

    }

    @Override
       public void commitEdit(String t) {
        super.commitEdit(t);
        this.mark = true;
        final String val = comboBox.getEditor().getText();
        updateItem(t, false);

        dialogs.initConfirmDialog("Update record?");

        int c = DialogState.getInstance().getState();
        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        Payment ge = (Payment) getTableRow().getItem();
                        ge.setBank(val.trim());
                        ge.setEvent("_updateChangeBank");
                        
                        try {
                            PaymentService.updateRecord(ge, ge.getEvent());
                        } catch (Exception ee) {
                            dialogs.initErrorDialog(ee.getMessage());
                            ee.printStackTrace();
                        }

                        
                        setStyle("-fx-text-fill: #008000");
                        return 0;
                    }
                };
            }
        };
        if (c == 0) {
            ss.start();
        } else {
            setStyle("-fx-text-fill: #FF8000");
            setText(oldvalue);
        }


    }

    private void createTextField() {
        comboBox = new ComboBox();
        comboBox.setEditable(true);
        comboBox.setMaxWidth(64d);
        //comboBox.setVisibleRowCount(6);
        AutoCompleteComboBoxListener lst = new AutoCompleteComboBoxListener(comboBox);
        //comboBox.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);

        Task ss;
        ss = new Task() {
            @Override
            protected Object call() throws Exception {
                Payment re = new Payment();
                re.setEvent("selectBanks");

                try {
                    banks = PaymentService.fetchList(re, re.getEvent());

                    for (Payment inner : banks) {
                        comboBox.getItems().add(inner.getBank());
                    }
                    comboBox.setPromptText("Select bank");

                    return 0;

                } catch (Exception ex) {
                    Logger.getLogger(CompanyCell.class.getName()).log(Level.SEVERE, null, ex);
                    return 1;
                }
            }
        };

        Platform.runLater(ss);

        comboBox.setMinWidth(256d);
        comboBox.getEditor().focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue.booleanValue() && !mark) {
                    //cancelEdit();a
                    commitEdit(comboBox.getEditor().getText());
                    System.out.println("lost focus: " + comboBox.getEditor().getText());
                }
            }
        });

    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

}
