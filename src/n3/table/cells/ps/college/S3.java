/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells.ps.college;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import lv.nmc.entities.col.CollegeEntry;
import lv.nmc.shared.MessageSender;
import lv.nmc.shared.SharedData;
import n3.WinLoader;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class S3 extends TableCell<CollegeEntry, Integer> {

    private TextField textField;
    private boolean mark = false;
    private final WinLoader dialogs = new WinLoader();
    private String oldVal, newVal;
    private Notifier nm;
    DateTimeFormatter sqlDate = DateTimeFormat.forPattern("yyyy-MM-dd");

    public S3() {

    }

    @Override
    public void startEdit() {
        super.startEdit();
        //oldVal = ((CollegeEntry) this.getTableRow().getItem()).getDisplayPrice();
        this.mark = false;
        if (textField == null) {
            createTextField();
        }
        setGraphic(textField);
        textField.setStyle("-fx-text-fill: #FF0000");
        textField.selectAll();

    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText( getItem().toString());
        setGraphic(null);

        setStyle("-fx-text-fill: #FF8000");
    }

    @Override
    public void updateItem(Integer item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {

                if (textField != null) {
                    textField.setText(getString());
                }
                setText(null);
                setGraphic(textField);

            } else {
                setText(getString());
                newVal = getString();
                setGraphic(null);
            }
        }

    }

    @Override
    public void commitEdit(Integer t) {
        this.mark = true;
        final String val = t.toString();
        updateItem(t, false);

        try {
            dialogs.initConfirmDialog("Sure to update?");
        } catch (Exception ex) {
            Logger.getLogger(S3.class.getName()).log(Level.SEVERE, null, ex);
        }

        int c = dialogs.getResult();

        if (c != 0) {
            return;
        }
        CollegeEntry ge = (CollegeEntry) getTableRow().getItem();

        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        nm.sendMail();
                        return 0;
                    }
                };
            }
        };

       /* if (today.isAfter(dateEnd)) {
            nm = new Notifier(this.getTableRow().getItem());
            nm.sendTo = "n3@novikontas.lv";
            nm.sendFrom = "training@novikontas.lv";
            nm.subject = "Changing price for client!";
            nm.prepare();

            if (!SharedData.getInstance().getReasonProvided()) {
                dialogs.initWarning("We will not update this because you have to provide some reason for changing old data");
                cancelEdit();
                return;
            };
            ss.start();
        }*/

        try {
            //SEND data

         

        } catch (Exception ee) {
            dialogs.initErrorDialog(ee.getMessage());
        }

      /*  JournalEntry je = new JournalEntry();
        je.setUsername(System.getProperty("n3.username"));
        je.setRegistrationId(ge.getRegistrationId());
        je.setOptype("update");
        je.setSysmsg("Username " + je.getUsername() + " has changed price to pay. New value: \"" + val + "\"");
        je.setEvent("_updateUpdateRecord");
        try {
            JournalService.insertRecord(je, je.getEvent());
        } catch (Exception ex) {
            Logger.getLogger(PointsCell.class.getName()).log(Level.SEVERE, null, ex);
        }*/

        if (c == 0) {
            setStyle("-fx-text-fill: #008000");
        }

    }

    private void createTextField() {
        textField = new TextField(getString());
        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if (t.getCode() == KeyCode.ENTER) {
                    commitEdit(Integer.parseInt(textField.getText()));
                } else if (t.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }
            }
        });

        textField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue.booleanValue() && !mark && !textField.isHover()) {
                    cancelEdit();
                }
            }

        });

    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

    private class Notifier {

        private Object entry;
        public String messageText, subject, sendTo, sendFrom;
  

        public Notifier(Object et) {
            this.entry = et;
        }

        public void prepare() {

            CollegeEntry ge = (CollegeEntry) entry;

            dialogs.initReasonDialog();

            messageText = "Username <b>" + System.getProperty("n3.username") + "</b> has changed price to pay for client: <span style='color:red; font-weight: bold'>"
                    + ge.getNameSurname() + " " + ge.getPersonCode()
                    + "</span> with course <b>" + ge.getCourseName()
                 //   + "</b>. Date end: " + ge.getDateEnd() + "<br /><br />"
                    + "Reason provided from " + System.getProperty("n3.username") + ": <br /><br /><i>" + SharedData.getInstance().getReason() + "</i>"
                    + "<ul>"
                    + "<li>Previous value: " + oldVal + "</li>"
                    + "<li>New value: " + newVal + "</li></ul>";            
        }

        public void sendMail() {
            MessageSender.sendMail(messageText, subject, sendTo, sendFrom);
            SharedData.getInstance().setReasonProvided(false);
        } //

    }

}
