/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells.ps;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lv.nmc.entities.JournalEntry;
import lv.nmc.entities.Payment;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.rest.JournalService;
import lv.nmc.rest.RegistrationService;
import lv.nmc.shared.MessageSender;
import lv.nmc.shared.SharedData;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author User
 */
public class BanDateCell extends TableCell<Payment, String> {

    private TextField textField;
    private boolean mark = false;
    private WinLoader dialogs = new WinLoader();
    private DateTimeFormatter sqlDate = DateTimeFormat.forPattern("Y-M-d");
    private Notifier nm;

    public BanDateCell() {

    }

    @Override
    public void startEdit() {
        super.startEdit();
        this.mark = false;
        if (textField == null) {
            createTextField();
        }
        setGraphic(textField);
        textField.setStyle("-fx-text-fill: #FF0000");
        textField.selectAll();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText((String) getItem());
        setGraphic(null);

        setStyle("-fx-text-fill: #FF8000");
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());

                }
                setText(null);
                setGraphic(textField);
            } else {
                setText(getString());
                setGraphic(null);
            }
        }

    }

    @Override
    public void commitEdit(String t) {
        this.mark = true;
        final String val = t;
        updateItem(t, false);
        dialogs.initConfirmDialog("Update record?");

        int c = dialogs.getResult();

        if (c != 0) {
            return;
        }

        Payment pt = (Payment) getTableRow().getItem();
        RegistrationEntry ge = new RegistrationEntry();
        ge.setRegistrationId(pt.getRegistrationId());
        ge.setDelayDate(val);

        JournalEntry je = new JournalEntry();
        je.setUsername(System.getProperty("n3.username"));
        je.setUserId(ge.getUserId());
        je.setGroupId(ge.getGroupId());
        je.setCourseId(ge.getCourseId());
        je.setOptype("update");
        je.setRegistrationId(ge.getRegistrationId());
        je.setEvent("_updateInsertGroupRecord");

        try {
            if (val.isEmpty()) {
                RegistrationService.updateRecord(ge, "_updateUnBanUser");
                RegistrationService.updateRecord(ge, "_updateDeleteExam");
                RegistrationService.updateRecord(ge, "_updateRestorePayment");
                je.setSysmsg("Username " + je.getUsername() + " has UNBANNED registrationId=" + ge.getRegistrationId());
                JournalService.insertRecord(je, je.getEvent());
            } else {
                ge.setCrewComment("USER BANNED");
                RegistrationService.updateRecord(ge, "_updateBanUser");
                ge.setExam("not passed");
                RegistrationService.updateRecord(ge, "_updateInsertExam");
                ge.setcPrice("0.00 EUR");
                RegistrationService.updateRecord(ge, "_updateErasePayment");
                
                try {
                    DateTime paymentDate = sqlDate.parseDateTime(pt.getPaymentDate());
                    DateTime dateEnd = sqlDate.parseDateTime(pt.getDateEnd());

                    //negative if this is less, zero if equal, positive if greater
                    if (1 == paymentDate.toLocalDate().compareTo(dateEnd.toLocalDate())) { //
                        System.out.println("Reactivate advance here");
                    } else if (0 == paymentDate.toLocalDate().compareTo(dateEnd.toLocalDate())) {
                        System.out.println("And also reactivate advance here");
                    }
                } catch (NullPointerException np) {
                    System.out.println("No payment for this record found");
                }

                

                //TRANSFER PAID
                je.setSysmsg("Username " + je.getUsername() + " has BANNED registrationId=" + ge.getRegistrationId());
                JournalService.insertRecord(je, je.getEvent());
            }
            setStyle("-fx-text-fill: green");
        } catch (Exception ee) {
            ee.printStackTrace();
        }
        
        DateTime dateEnd = sqlDate.parseDateTime(pt.getDateEnd());
        DateTime today = sqlDate.parseDateTime(new DateTime().toString(sqlDate));

        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        nm.sendMail();
                        return 0;
                    }
                };
            }
        };
        
        if (today.isAfter(dateEnd)) {
            nm = new Notifier(this.getTableRow().getItem());
            nm.sendTo = "n3@novikontas.lv";
            nm.sendFrom = "training@novikontas.lv";
            nm.subject = "Client [un]banned!";
            nm.prepare();

            if (!SharedData.getInstance().getReasonProvided()) {
                dialogs.initWarning("We will not update this because you have to provide some reason for changing old data");
                cancelEdit();
                setStyle("-fx-text-fill: #FF0000");
                return;
            };
            ss.start();
        }
        
        

    }

    private void rectivateAdvance() {

    }

    private void createTextField() {
        textField = new TextField(getString());
        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if (t.getCode() == KeyCode.ENTER) {
                    commitEdit(textField.getText());
                } else if (t.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }
            }
        });

        textField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue.booleanValue() && !mark && !textField.isHover()) {
                    cancelEdit();
                }
            }

        });

    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

    private class Notifier {

        private Object entry;
        public String messageText, subject, sendTo, sendFrom;

        public Notifier(Object et) {
            this.entry = et;
        }

        public void prepare() {

            Payment ge = (Payment) entry;

            dialogs.initReasonDialog();

            messageText = "Username <b>" + System.getProperty("n3.username") + "</b> has BANNED or UNBANNED client: <span style='color:red; font-weight: bold'>"
                    + ge.getNameSurname() + " " + ge.getPersonCode()
                    + "</span> Date end: " + ge.getDateEnd() + "<br /><br />"
                    + "Reason provided from " + System.getProperty("n3.username") + ": <br /><br /><i>" + SharedData.getInstance().getReason() + "</i>";

        }

        public void sendMail() {
            MessageSender.sendMail(messageText, subject, sendTo, sendFrom);
            SharedData.getInstance().setReasonProvided(false);
        } //

    }

}
