/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells.ps;

import com.sun.prism.paint.Color;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import lv.nmc.entities.Payment;

/**
 *
 * @author User
 */
public class NameSurnamePs extends TableCell<Payment, String> {

    public NameSurnamePs() {
        // createHyperlink();
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {

                setText(null);
                //setGraphic(certLink);
            } else {
                if (((Payment) getTableRow().getItem()).getActive().equals(1)) {
                    this.setStyle("-fx-font-weight: bold; -fx-text-fill: #FF8000;");
                    //this.getStyle().
                  //  setTextFill(Color.RED);
                } else {
                    this.setStyle("-fx-text-fill: green;");
                }
                
                 setText(getString());
                // setGraphic(null);
               // certLink.setText(getString());
                // setGraphic(certLink);
            }
        }

    }

    /* private void createHyperlink() {
     certLink = new Hyperlink(getString());
     certLink.setStyle("-fx-text-fill: #000080; -fx-border-width: 0px");        
     certLink.setTooltip(new Tooltip("View profile"));
     StringBuilder sb = new StringBuilder();
     sb.append(certLink.getText());
     sb.append(" - client details");

     certLink.setOnAction(new EventHandler<ActionEvent>() {
     @Override
     public void handle(ActionEvent t) {
     RegistrationEntry ge = (RegistrationEntry) getTableRow().getItem();
     ClientEntry ce = new ClientEntry();
     ce.setUserId(ge.getUserId());
     ce.setEvent("clientDetails");
                  
     LastClient.getInstance().setLastClient(ce);
                  
                  
     final String dialog = "/n3/dialogs/UserDetails.fxml";
     final String tt = certLink.getText() + " - client details";
     Task<Integer> openWindow = new Task<Integer>() {
     @Override
     protected Integer call() throws Exception {

     try {
                    
     WinLoader wl = new WinLoader();
     wl.initModal(dialog, tt);              

     } catch (IOException ex) {
     ex.printStackTrace();
     }
     return 0;
     }
     };
     openWindow.run();
    
     }
     });
         
       

     }*/
    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

}
