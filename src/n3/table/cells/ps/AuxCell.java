/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells.ps;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lv.nmc.entities.Payment;
import lv.nmc.rest.PaymentService;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;

/**
 *
 * @author User
 */
public class AuxCell extends TableCell<Payment, String> {

    private TextField textField;
    private boolean mark = false;
    private WinLoader dialogs = new WinLoader();

    public AuxCell() {
        
    }

    @Override
    public void startEdit() {
        super.startEdit();
        this.mark = false;
        if (textField == null) {
            createTextField();
        }
        setGraphic(textField);
        textField.setStyle("-fx-text-fill: #FF0000");
        textField.selectAll();
    }

    @Override
    public void cancelEdit() {      
        super.cancelEdit();
        
        
        
        setText((String) getItem());
        setGraphic(null);
        
        setStyle("-fx-text-fill: #FF8000");
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());

                }
                setText(null);
                setGraphic(textField);
            } else {
                setText(getString());
                this.setTooltip(new Tooltip(getString()));
                setGraphic(null);
            }
        }

    }

    @Override
    public void commitEdit(String t) {       
        this.mark = true;
        final String val = t;
        updateItem(t, false);
        dialogs.initConfirmDialog("Update record?");
        
        int c = DialogState.getInstance().getState();
        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        Payment ge = (Payment) getTableRow().getItem();                        
                        ge.setAuxInfo(val);
                        PaymentService.updateRecord(ge, "_updateAux");
                        setStyle("-fx-text-fill: green");
                        
                        return 0;
                    }
                };
            }
        };
        if (c==0) {
            ss.start();
        } else {
            setStyle("-fx-text-fill: #FF0000"); 
        }
        
        
    }

    private void createTextField() {
        textField = new TextField(getString());
        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if (t.getCode() == KeyCode.ENTER) {
                    commitEdit(textField.getText());
                } else if (t.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }
            }
        });

        textField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue.booleanValue() && !mark && !textField.isHover()) {
                    cancelEdit();                   
                }
            }           
            
        });   
       

    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }
    
}