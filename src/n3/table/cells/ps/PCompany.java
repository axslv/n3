/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells.ps;

import n3.table.cells.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import lv.nmc.entities.JournalEntry;
import lv.nmc.entities.Payment;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.rest.JournalService;
import lv.nmc.rest.PaymentService;
import lv.nmc.shared.Companies;
import lv.nmc.shared.MessageSender;
import lv.nmc.shared.SharedData;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author User
 */
public class PCompany extends TableCell<Payment, String> {

    private ComboBox<String> comboBox;
    private boolean mark = false;
    private List<Payment> companiesList = new ArrayList<>();
    private String oldvalue = "";
    private WinLoader dialogs = new WinLoader();
    private String oldVal, newVal;
    private Notifier nm;
    DateTimeFormatter sqlDate = DateTimeFormat.forPattern("yyyy-MM-dd");

    public PCompany() {
    }

    @Override
    public void startEdit() {

        oldvalue = getString();
        oldVal = ((Payment) this.getTableRow().getItem()).getCompany();
        super.startEdit();
        this.mark = false;
        if (comboBox == null) {
            createTextField();
        }
        setGraphic(comboBox);
        comboBox.setStyle("-fx-text-fill: #FF0000");
        comboBox.setMinWidth(256);
        
        //textField.selectAll();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setText(getItem());
        setGraphic(null);
        setStyle("-fx-text-fill: #FF0000");
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {
                if (comboBox != null) {
                    comboBox.setPromptText(getString());

                }
                setText(null);
                setGraphic(comboBox);
            } else {
                setText(getString());
                setGraphic(null);
                newVal = getString();
            }
        }

    }

    @Override
    public void commitEdit(String t) {
        super.commitEdit(t);
        this.mark = true;
        final String val = comboBox.getEditor().getText();
        updateItem(t, false);

        dialogs.initConfirmDialog("Update record?");

        int c = dialogs.getResult();

        if (c != 0) {
            return;
        }

        if (null == val) {
            return;
        }

        if (val.trim().isEmpty()) {
            return;
        }

        Payment ge = (Payment) getTableRow().getItem();

        DateTime dateEnd = sqlDate.parseDateTime(ge.getDateEnd());
        DateTime today = sqlDate.parseDateTime(new DateTime().toString(sqlDate));

        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        nm.sendMail();
                        return 0;
                    }
                };
            }
        };

        if (today.isAfter(dateEnd)) {
            nm = new Notifier(this.getTableRow().getItem());
            nm.sendTo = "n3@novikontas.lv";
            nm.sendFrom = "training@novikontas.lv";
            nm.subject = "Changing company for client!";
            nm.prepare();

            if (!SharedData.getInstance().getReasonProvided()) {
                dialogs.initWarning("We will not update this because you have to provide some reason for changing old data");
                cancelEdit();
                return;
            };
            ss.start();
        }

        ge.setCompany(val.trim());
        ge.setEvent("_updateChangeCompany");

        try {
            PaymentService.updateRecord(ge, ge.getEvent());
            ge.setEvent("_updateRegCompany");
            PaymentService.updateRecord(ge, ge.getEvent());
            JournalEntry je = new JournalEntry();
            je.setUsername(System.getProperty("n3.username"));
            je.setRegistrationId(ge.getRegistrationId());
            je.setOptype("update");
            je.setSysmsg("Username " + je.getUsername() + " has changed client's company to \"" + val + "\"");
            je.setEvent("_updateUpdateRecord");
            JournalService.insertRecord(je, je.getEvent());

        } catch (Exception ee) {
            dialogs.initErrorDialog(ee.getMessage());
            ee.printStackTrace();
        }

        if (c == 0) {
            setStyle("-fx-text-fill: #008000");
        }

    }

    private void createTextField() {
        comboBox = new ComboBox();
        comboBox.setEditable(true);
        comboBox.setMaxWidth(64d);
        //comboBox.setVisibleRowCount(6);
        AutoCompleteComboBoxListener lst = new AutoCompleteComboBoxListener(comboBox);
        //comboBox.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);

        Task ss;
        ss = new Task() {
            @Override
            protected Object call() throws Exception {
                Payment re = new Payment();
                re.setEvent("fetchCompanies");

                try {

                    for (RegistrationEntry inner : Companies.getInstance().getCompaniesList()) {
                        comboBox.getItems().add(inner.getCompany());
                    }
                    comboBox.setPromptText("Select company");

                    return 0;

                } catch (Exception ex) {
                    Logger.getLogger(CompanyCell.class.getName()).log(Level.SEVERE, null, ex);
                    return 1;
                }
            }
        };

        Platform.runLater(ss);

        comboBox.setMinWidth(256d);
        comboBox.getEditor().focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue.booleanValue() && !mark) {
                    //cancelEdit();a
                    commitEdit(comboBox.getEditor().getText());
                    System.out.println("lost focus: " + comboBox.getEditor().getText());
                }
            }
        });

    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

    private class Notifier {

        private Object entry;
        public String messageText, subject, sendTo, sendFrom;

        public Notifier(Object et) {
            this.entry = et;
        }

        public void prepare() {

            Payment ge = (Payment) entry;

            dialogs.initReasonDialog();

            messageText = "Username <b>" + System.getProperty("n3.username") + "</b> has changed <b>company</b> for client: <span style='color:red; font-weight: bold'>"
                    + ge.getNameSurname() + " " + ge.getPersonCode()
                    + "</span> with course <b>" + ge.getCourseName()
                    + "</b>. Date end: " + ge.getDateEnd() + "<br /><br />"
                    + "Reason provided from " + System.getProperty("n3.username") + ": <br /><br /><i>" + SharedData.getInstance().getReason() + "</i>"
                    + "<ul>"
                    + "<li>Previous value: " + oldVal + "</li>"
                    + "<li>New value: " + newVal + "</li></ul>";

            //System.out.println(messageText);
        }

        public void sendMail() {
            MessageSender.sendMail(messageText, subject, sendTo, sendFrom);
            SharedData.getInstance().setReasonProvided(false);
        }

    }
}
