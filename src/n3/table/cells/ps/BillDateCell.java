package n3.table.cells.ps;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import lv.nmc.entities.Payment;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;

/**
 *
 * @author User
 */
public class BillDateCell extends TableCell<Payment, String> {

    private TextField textField;
    private boolean mark = false;
    private WinLoader dialogs = new WinLoader();

    public BillDateCell() {

    }

    @Override
    public void startEdit() {
        super.startEdit();
        this.mark = false;
        
        setGraphic(textField);
        textField.setStyle("-fx-text-fill: #FF0000");
        textField.selectAll();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText((String) getItem());
        setGraphic(null);

        setStyle("-fx-text-fill: #FF8000");
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {            
            
                setText(getString().replace("1970-01-01", ""));
                setGraphic(null);
            
        }

    }

    @Override
    public void commitEdit(String t) {
        this.mark = true;
        final String val = t;
        updateItem(t, false);
        dialogs.initConfirmDialog("Update record?");

        int c = DialogState.getInstance().getState();
        Service ss = new Service() {
            @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        Payment ge = (Payment) getTableRow().getTableView().getSelectionModel().getSelectedItem();
                        setStyle("-fx-text-fill: green");
                        return 0;
                    }
                };
            }
        };
        if (c == 0) {
            ss.start();
        } else {
            setStyle("-fx-text-fill: #FF0000");
        }

    }

   

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

}
