/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import lv.nmc.entities.Sms;
import lv.nmc.rest.SmsService;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;

/**
 *
 * @author User
 */
public class SmsRegCell extends TableCell<Sms, Boolean> {

    private CheckBox checkBox;
    private WinLoader dialogs = new WinLoader();

    public SmsRegCell() {

        checkBox = new CheckBox();
        
        

        checkBox.setDisable(true);
        checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {                
                if (isEditing()) {
                    commitEdit(newValue == null ? false : newValue);
                    //System.out.println(newValue);
                }
            }
        });      
       
        this.setGraphic(checkBox);
        this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.setEditable(true);
    }

    @Override
    public void startEdit() {
        super.startEdit();
        if (isEmpty()) {
            return;
        }
        checkBox.setDisable(false);
        checkBox.requestFocus();
        

    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        checkBox.setDisable(true);
    }

    @Override
    public void commitEdit(Boolean value) {
        super.commitEdit(value);
        checkBox.setDisable(true);
        final Boolean val = value;
           dialogs.initConfirmDialog("Update record?");        
        int c = DialogState.getInstance().getState();
           Service ss = new Service() {
           @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        Sms ge = (Sms) getTableRow().getItem();                        
                        ge.setRegistered(val);
                        SmsService.updateRecord(ge, "_updateRegisterSms");
                        return 0;
                    }
                };
            }
        };
        if (c == 0) {
            ss.start();
        } else {
           checkBox.setSelected(!value);
        }
        
    }

    @Override
    public void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);
        if (!isEmpty()) {
            checkBox.setSelected(item);
        }
    }
}