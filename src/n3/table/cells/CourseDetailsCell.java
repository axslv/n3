/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.Tooltip;
import lv.nmc.entities.RegistrationEntry;

/**
 *
 * @author User
 */
public class CourseDetailsCell extends TableCell<RegistrationEntry, String> {

    private Hyperlink certLink;

    public CourseDetailsCell() {
        createHyperlink();
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);


        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {
                if (certLink != null) {
                    certLink.setText(getString());

                }
                setText(null);
                setGraphic(certLink);
            } else {
                /*
                 setText(getString());
                 setGraphic(null);*/
                certLink.setText(getString());
                setGraphic(certLink);
            }
        }

    }

    private void createHyperlink() {
        certLink = new Hyperlink(getString());
        certLink.setStyle("-fx-text-fill: #000080; -fx-border-width: 0px");
        certLink.setTooltip(new Tooltip("View course"));
        StringBuilder sb = new StringBuilder();
        sb.append(certLink.getText());
        sb.append(" - client details");

        certLink.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
               
                Task<Integer> openWindow = new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {

                       
                        return 0;
                    }
                };
                openWindow.run();

            }
        });



    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }
}