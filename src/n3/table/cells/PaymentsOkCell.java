/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.table.cells;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import lv.nmc.entities.RegistrationEntry;

/**
 *
 * @author User
 */
public class PaymentsOkCell extends TableCell<RegistrationEntry, Boolean> {

    private CheckBox checkBox;

    public PaymentsOkCell() {

        checkBox = new CheckBox();
      //  RegistrationEntry ge = (RegistrationEntry) getTableRow().getTableView().getItems().get(getIndex());                  
        

        checkBox.setDisable(true);
        checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (isEditing()) {
                    commitEdit(newValue == null ? false : newValue);
                }
            }
        });
       
       
        this.setGraphic(checkBox);
        this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.setEditable(true);
    }

    @Override
    public void startEdit() {
        super.startEdit();
        if (isEmpty()) {
            return;
        }
        checkBox.setDisable(false);
        checkBox.requestFocus();

    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        checkBox.setDisable(true);
    }

    @Override
    public void commitEdit(Boolean value) {
        super.commitEdit(value);
        checkBox.setDisable(true);
   /*     final Boolean val = value;
           int c = JOptionPane.showConfirmDialog(null, "Update record?", "Question", JOptionPane.YES_NO_OPTION);
        Service ss = new Service() {
           @Override
            protected Task createTask() {
                return new Task<Integer>() {
                    @Override
                    protected Integer call() throws Exception {
                        RegistrationEntry ge = (RegistrationEntry) getTableRow().getTableView().getItems().get(getIndex());                        
                        ge.setCertGiven(val);
                        RegistrationService.updateRecord(ge, "_updateCertGiven");
                        return 0;
                    }
                };
            }
        };
        if (c == 0) {
            ss.start();
        } else {
           checkBox.setSelected(!value);
        }*/
        
    }

    @Override
    public void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);
        if (!isEmpty()) {
            checkBox.setSelected(item);
        }
    }
}