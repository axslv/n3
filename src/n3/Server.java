/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package n3;

/**
 *
 * @author Administrator
 */
import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.xml.ws.*;
import javax.xml.ws.http.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.ws.handler.MessageContext;

@WebServiceProvider
@ServiceMode(value = Service.Mode.PAYLOAD)
public class Server implements Provider<Source> {
    private String phoneNum = "";
    
@Resource WebServiceContext ctx;

    @Override
    public Source invoke(Source request) {
   
    MessageContext mc = ctx.getMessageContext();
    
    if (null == mc.get("javax.xml.ws.http.request.querystring")) {
        System.out.println("mc is null");
        return new StreamSource(new StringReader("<NULL />"));
    }
    
    try {     
       Map<String, String> qq =  new LinkedHashMap<String, String>();
       qq = splitQuery(new URL("http://localhost/?" + mc.get("javax.xml.ws.http.request.querystring").toString()));        
     /*   System.out.println(qq.get("phoneNum"));
        phoneNum = qq.get("phoneNum");*/
        
        for (String ss : qq.values()) {
            System.out.println(ss);
        }
      
        
    } catch (Exception ex) {
        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        return  new StreamSource(new StringReader("<ERROR />"));
    }
       return  new StreamSource(new StringReader("<SUCCESS />"));
    }

    public void launch() throws InterruptedException {

        String address = "http://192.168.0.128:9898/";
        Endpoint.create(HTTPBinding.HTTP_BINDING, new Server()).publish(address);
       // Thread.sleep(Long.MAX_VALUE);
    }
    
    private Map<String, String> splitQuery(URL url) throws UnsupportedEncodingException {
    Map<String, String> query_pairs = new LinkedHashMap<String, String>();
    String query = url.getQuery();
    String[] pairs = query.split("&");
    for (String pair : pairs) {
        int idx = pair.indexOf("=");
        query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
    }
    return query_pairs;
}

    /**
     * @return the phoneNum
     */
    public String getPhoneNum() {
        return phoneNum;
    }

    /**
     * @param phoneNum the phoneNum to set
     */
    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }
}