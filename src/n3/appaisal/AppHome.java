/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.appaisal;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import lv.nmc.entities.CourseEntry;
import lv.nmc.entities.StaffEntry;
import lv.nmc.rest.CourseService;
import lv.nmc.rest.StaffService;
import n3.WinLoader;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Pavel
 */
public class AppHome implements Initializable {

    @FXML
    private ComboBox<String> departments, courses, domains, presentCharts;
    @FXML
    private ListView instructors;
    @FXML
    private TextField dateFieldFrom, dateFieldTill;
    @FXML
    private Button addChartButton;

    @FXML
    private Button deleteChartsButton;
    @FXML
    private Button deleteChartButton;
    @FXML
    private Button addCourseChartButton;
    @FXML
    private Button addDepartmentChartButton;
    @FXML
    private Button addDivisionChartButton;

    @FXML
    private BarChart<String, Number> chartField;
    //private BarChart chartField;

    private List<CourseEntry> coursesList = new ArrayList<>();
    private CourseEntry currentCourse = new CourseEntry();
    private StaffEntry curentStaff = new StaffEntry();
    private List<StaffEntry> coursesInsList = new ArrayList<>();
    private List<StaffEntry> appaisalList = new ArrayList<>();
    private List<Double> appaisalListFinal = new ArrayList<>();
    private HashMap<String, Integer> idByInstructors = new HashMap<>();
    private HashMap<String, Integer> idByCourse = new HashMap<>();
    //private HashMap<String, Integer> presentChartsMap = new HashMap<>();
    private List<ChartData> plots = new ArrayList<>();
    private Double appaisalMark = 0.0;
    private Integer testtest = 0, polleeSum, chartSize;
    private XYChart.Series series1 = new XYChart.Series();
    private ArrayList presentChartsMap = new ArrayList();
    //private HashMap<String, Integer> chartList = new HashMap<>();
//private XYChart.Series series1 = new XYChart.Series();
    private Object chartList = new Object();
    private Object[] bbbbb;
    private WinLoader dialogs = new WinLoader();

    @FXML

    public void initialize(URL url, ResourceBundle rb) {
        DateTime dt = new DateTime();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("y-MM-d");
        domains.getItems().clear();
        presentCharts.getItems().clear();
        domains.getItems().add("tc");
        domains.getItems().add("ppp_eng");
        domains.getItems().add("ppp_nav");
        domains.getSelectionModel().select(0);
        loadMapForCourses();
        loadMapForInstructors();
        fetchDepartments();
        // loadInstructors();
        dateFieldFrom.setText("2010-01-01");
        dateFieldTill.setText(dt.toString(dtf));
        
        chartField.getData().add(getSeries1());
        chartField.getStylesheets().add("n3/style/appaisal.css");

    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @FXML
    private void fetchDepartments() {
        departments.getItems().clear();

        currentCourse.setDomain(domains.getValue());
        currentCourse.setEvent("fetchDptByDomain");

        try {
            coursesList = CourseService.fetchList(currentCourse, currentCourse.getEvent());

            for (CourseEntry cce : coursesList) {
                departments.getItems().add(cce.getDepartment());
            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void loadCourses(ActionEvent e) {
        courses.getItems().clear();
        currentCourse.setDepartment(departments.getValue());
        currentCourse.setEvent("coursesByDpt");
        currentCourse.setDomain(domains.getValue());

        try {
            coursesList = CourseService.fetchList(currentCourse, currentCourse.getEvent());

            for (CourseEntry ce : coursesList) {
                courses.getItems().add(ce.getCourseName());
            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void loadInstructors() {

        instructors.getItems().clear();
        curentStaff.setStaffId(100);
        curentStaff.setEvent("allInstructorsList");
        try {
            coursesInsList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

            for (StaffEntry ce : coursesInsList) {
                instructors.getItems().add(ce.getNameSurname());
            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    private void loadInstructorsByCourse() {

        instructors.getItems().clear();

        curentStaff.setEvent("fetchInstructorsByCourseIdByDateAppNotZero");

        curentStaff.setEndAppaisalDate(dateFieldTill.getText());
        curentStaff.setStartAppaisalDate(dateFieldFrom.getText());
        curentStaff.setCourseName(courses.getValue());
        curentStaff.setCourseId(idByCourse.get(courses.getValue()));
        coursesInsList.clear();

        try {
            coursesInsList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

            for (StaffEntry ce : coursesInsList) {
                instructors.getItems().add(ce.getNameSurname());
            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    @FXML
    private void loadInstructors(ActionEvent e) {

        loadInstructorsByCourse();

    }

    @FXML
    private void addChart(ActionEvent ext) {
        //System.out.println(instructors.getSelectionModel().getSelectedItem().toString()); 
        appaisalByCourseByInstructorByDate();

        //loadChart();
    }

    private void appaisalByCourseByInstructorByDate() {
        //System.out.println(instructors.getSelectionModel().getSelectedItem().toString());
        appaisalMark = 0.0;
        appaisalList.clear();

        curentStaff.setEndAppaisalDate(dateFieldTill.getText());
        curentStaff.setStartAppaisalDate(dateFieldFrom.getText());
        curentStaff.setCourseId(idByCourse.get(courses.getValue()));
        //curentStaff.setStaffId(idByInstructors.get(instructors.getSelectionModel().getSelectedItem().toString()));
        curentStaff.setNameSurname(instructors.getSelectionModel().getSelectedItem().toString());
        curentStaff.setEvent("getPolleeCountByCourseByInstructorByDateNotZero");
        try {
            appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

            for (StaffEntry cl : appaisalList) {
                polleeSum = cl.getPolleeSum();

                //System.out.println(cl.getPolleeSum().toString());
            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (polleeSum != 0) {

            curentStaff.setEvent("getAppaisalByCourseByInstructorByDate");
            try {
                appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

                for (StaffEntry ck : appaisalList) {

                    appaisalMark = ck.getAppData();
                //System.out.println(ck.getAppData().toString());

                }
            //System.out.println(appaisalMark);

                //System.out.println(instructors.getSelectionModel().getSelectedItem().toString());
            } catch (Exception ex) {
                Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
            }

            curentStaff.setEvent("getAppaisalCountByCourseByInstructorByDate");
            try {
                appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

                for (StaffEntry cb : appaisalList) {

                    appaisalMark = round(appaisalMark / cb.getAppCount(), 1);

               // System.out.println(cb.getAppCount().toString());
                }
            //System.out.println(appaisalMark);

                //System.out.println(instructors.getSelectionModel().getSelectedItem().toString());
            } catch (Exception ex) {
                Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
            }
            //System.out.println(appaisalMark.toString());
            loadChart(appaisalMark, instructors.getSelectionModel().getSelectedItem().toString());
        } else {
            appError("Sorry, no Appaisal registered for Instructor. Try renew.");
        }

    }

    private void loadMapForCourses() {

        currentCourse.setEvent("allCourses");
        coursesList.clear();
        try {
            coursesList = CourseService.fetchList(currentCourse, currentCourse.getEvent());

            for (CourseEntry ce : coursesList) {

                idByCourse.put(ce.getCourseName(), ce.getCourseId());
            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    private void loadMapForInstructors() {

        curentStaff.setEvent("allInstructorsList");
        curentStaff.setStaffId(1);
        coursesInsList.clear();
        try {
            coursesInsList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

            for (StaffEntry cd : coursesInsList) {

                idByInstructors.put(cd.getNameSurname(), cd.getStaffId());
            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    @FXML
    private void deleteCharts(ActionEvent e) {
        getSeries1().getData().clear();
        presentChartsMap.clear();
        presentCharts.getItems().clear();
    }

    @FXML
    private void deleteChart(ActionEvent e) {
        //getSeries1().getData().remove(presentChartsMap.get(presentCharts.getValue()));
        getSeries1().getData().remove(presentChartsMap.indexOf(presentCharts.getValue()));

        presentChartsMap.remove(presentCharts.getValue());
        presentChartsMap.trimToSize();
        presentCharts.getItems().remove(presentCharts.getValue());
//int k = presentChartsMap.get(presentCharts.getValue());
    /*switch (presentChartsMap.get(presentCharts.getValue())){
         case 0: getSeries1().getData().remove(0);
         break;
         case 1: getSeries1().getData().remove(1);
         break;
            
         default : getSeries1().getData().remove(2);
         break;
         };*/

    //getSeries1().getData().remove(k);
        //Integer.parseInt(presentChartsMap.get(presentCharts.getValue()).toString());
        //getSeries1().getData().remo
        //System.out.println(presentChartsMap.get(presentCharts.getValue()));
//getSeries1().getData().rem1);
    }

    @FXML
    private void addCourseChart(ActionEvent e) {
        appaisalMark = 0.0;
        appaisalList.clear();

        curentStaff.setEndAppaisalDate(dateFieldTill.getText());
        curentStaff.setStartAppaisalDate(dateFieldFrom.getText());
        curentStaff.setCourseId(idByCourse.get(courses.getValue()));
        curentStaff.setEvent("getPolleeCountByCourseByDateNotZero");
        try {
            appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

            for (StaffEntry cl : appaisalList) {
                polleeSum = cl.getPolleeSum();
                System.out.println(polleeSum);
            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (polleeSum != 0) {
            curentStaff.setEvent("getAppaisalByCourseByDate");

            try {
                appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

                for (StaffEntry ck : appaisalList) {

                    appaisalMark = ck.getAppData();
                    System.out.println(appaisalMark);

                }

            } catch (Exception ex) {
                Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
            }

            curentStaff.setEvent("getAppaisalCountByCourseByDate");
            try {
                appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

                for (StaffEntry cb : appaisalList) {

                    appaisalMark = round(appaisalMark / cb.getAppCount(), 1);
                    System.out.println(appaisalMark);
                }

            } catch (Exception ex) {
                Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
            }
         //System.out.println(appaisalMark.toString());

            loadChart(appaisalMark, courses.getValue());
        } else {
            appError("Sorry, no Appaisal registered at Course");
        }
    }

    @FXML
    private void addDepartmentChart(ActionEvent e) throws IOException {
        appaisalMark = 0.0;
        appaisalList.clear();

        curentStaff.setEndAppaisalDate(dateFieldTill.getText());
        curentStaff.setStartAppaisalDate(dateFieldFrom.getText());
        curentStaff.setDepartment(departments.getValue());

        curentStaff.setDomain(domains.getValue());
        curentStaff.setEvent("getPolleeCountByDepartmentByDateNotZero");
        try {
            appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

            for (StaffEntry cl : appaisalList) {
                polleeSum = cl.getPolleeSum();
                System.out.println(polleeSum);

            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (polleeSum != 0) {
            curentStaff.setEvent("getAppaisalByDepartmentByDate");
            try {
                appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

                for (StaffEntry ck : appaisalList) {

                    appaisalMark = ck.getAppData();
                    System.out.println(appaisalMark);

                }

            } catch (Exception ex) {
                Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
            }

            curentStaff.setEvent("getAppaisalCountByDepartmentByDate");
            try {
                appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

                for (StaffEntry cb : appaisalList) {

                    appaisalMark = round(appaisalMark / cb.getAppCount(), 1);
                    System.out.println(appaisalMark);
                }
            } catch (Exception ex) {
                Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
            }
         //System.out.println(appaisalMark.toString());

            loadChart(appaisalMark, departments.getValue());
        } else {
            appError("Sorry, no Appaisal registered at Department");
        }
    }

    @FXML
    private void appError(String errorMsg) {

        try {

            //dialogs.initUserError("Sorry, no Appaisals");
            dialogs.initUserError(errorMsg);
        } catch (Exception ex) {
            dialogs.initErrorDialog(ex.toString());
        }
    }

    @FXML
    private void addDivisionChart(ActionEvent e) {
        appaisalMark = 0.0;
        appaisalList.clear();
        curentStaff.setEvent("getAppaisalByDomainByDate");
        curentStaff.setEndAppaisalDate(dateFieldTill.getText());
        curentStaff.setStartAppaisalDate(dateFieldFrom.getText());
        curentStaff.setDomain(domains.getValue());

        try {
            appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

            for (StaffEntry ck : appaisalList) {

                appaisalMark = ck.getAppData();
                System.out.println(appaisalMark);

            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
        }

        curentStaff.setEvent("getPolleeCountByDomainByDateNotZero");
        try {
            appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

            for (StaffEntry cl : appaisalList) {
                polleeSum = cl.getPolleeSum();
                System.out.println(polleeSum);

            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
        }
        curentStaff.setEvent("getAppaisalCountByDomainByDate");
        try {
            appaisalList = StaffService.fetchList(curentStaff, curentStaff.getEvent());

            for (StaffEntry cb : appaisalList) {

                appaisalMark = round(appaisalMark / cb.getAppCount(), 1);
                System.out.println(appaisalMark);

            }

        } catch (Exception ex) {
            Logger.getLogger(AppHome.class.getName()).log(Level.SEVERE, null, ex);
        }
         //System.out.println(appaisalMark.toString());

        loadChart(appaisalMark, domains.getValue());
    }

    @FXML
    private void selectPresentChart() {

    //chartSize = getSeries1().getData().size();
        //System.out.println(getSeries1().getData().get);
        //chartList=getSeries1().getData().get(0);
        //System.out.println(chartList);
    //System.out.println(chartList.toString());
    }

    private void loadChart(Double Appaisal, String chartName) {
         //XYChart gggg = new XYChart() {};

//final CategoryAxis xAxis = new CategoryAxis();
        //final NumberAxis yAxis = new NumberAxis();
        //bc.setTitle("Country Summary");
        //xAxis.setLabel("Country");
        //yAxis.setLabel("Value");
        //chartField.getData().clear();
        //series1.setName("2003");
        //XYChart.Series series1 = new XYChart.Series();
        //chartField.getData().clear();
        //getSeries1().getData().clear();
        //chartField.getData().clear();
        //System.out.println(Appaisal.toString()+chartName);
        //getSeries1().getData().add(new XYChart.Data(chartName+Appaisal.toString(),Appaisal));
        if (presentChartsMap.size() < 5) {
            chartName = chartName.concat(" " + dateFieldFrom.getText() + " to " + dateFieldFrom.getText() + " ");
            if (presentChartsMap.contains(chartName)) {
                appError("You already have this Chart");
            } else {

                XYChart.Data kk = new XYChart.Data(chartName + Appaisal.toString(), Appaisal);
//getSeries1().getChart().setStyle("-fx-padding: 10px");
                getSeries1().getData().add(kk);

                presentCharts.getItems().add(chartName);
                presentChartsMap.add(chartName);
        //getSeries1().getChart().setLegendSide(TOP);
                //kk.getNode().setScaleY(0.5);
                //chartField.getData().add(kk.
//System.out.print(getSeries1().getData().get(0));

        //getSeries1().getData().add(new XYChart.Data("grgr",5.1));
        //series1.getData().clear();
                //getSeries1().getData().add(new XYChart.Data(instructors.getSelectionModel().getSelectedItem().toString(), appaisalMark));
                //series1.getData().add(new XYChart.Data("lala", appaisalMark));
                //series1.getData().add(new XYChart.Data("brazil", 20148.82));
                //series1.getData().add(new XYChart.Data("france", 10000));
                //series1.getData().add(new XYChart.Data("italy", 35407.15));
                //series1.getData().add(new XYChart.Data("usa", 12000));
                //chartField.getData().addAll(series1);
                //chartField.getData().addAll(series1);
        //chartField.getData().add(getSeries1());
         //stage.setScene(scene);
                //stage.show();
            }
        } else {
            appError("Only 5 Charts possible");
        }
    }

    /**
     * @return the series1
     */
    public XYChart.Series getSeries1() {
        return series1;
    }

    /**
     * @param series1 the series1 to set
     */
    public void setSeries1(XYChart.Series series1) {
        this.series1 = series1;
    }
}
