/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.appaisal;

/**
 *
 * @author Pavel
 */
public class ChartData {
    private String chartName,startAppaisalDate, endAppaisalDate;
    private Double chartValue;

    /**
     * @return the chartName
     */
    public String getChartName() {
        return chartName;
    }

    /**
     * @param chartName the chartName to set
     */
    public void setChartName(String chartName) {
        this.chartName = chartName;
    }

    /**
     * @return the chartValue
     */
    public Double getChartValue() {
        return chartValue;
    }

    /**
     * @param chartValue the chartValue to set
     */
    public void setChartValue(Double chartValue) {
        this.chartValue = chartValue;
    }

    /**
     * @return the startAppaisalDate
     */
    public String getStartAppaisalDate() {
        return startAppaisalDate;
    }

    /**
     * @param startAppaisalDate the startAppaisalDate to set
     */
    public void setStartAppaisalDate(String startAppaisalDate) {
        this.startAppaisalDate = startAppaisalDate;
    }

    /**
     * @return the endAppaisalDate
     */
    public String getEndAppaisalDate() {
        return endAppaisalDate;
    }

    /**
     * @param endAppaisalDate the endAppaisalDate to set
     */
    public void setEndAppaisalDate(String endAppaisalDate) {
        this.endAppaisalDate = endAppaisalDate;
    }
    
}
