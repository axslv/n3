/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3;

import javafx.application.Preloader;
import javafx.application.Preloader.ProgressNotification;
import javafx.application.Preloader.StateChangeNotification;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Simple Preloader Using the ProgressBar Control
 *
 * @author User
 */
public class NvLoad extends Preloader {
    
    ProgressBar bar;
    Stage stage;
    
    private Scene createPreloaderScene() {
        VBox cn = new VBox();
        cn.setPrefWidth(455.00);
        cn.setPrefHeight(150.00);
        cn.setAlignment(Pos.TOP_CENTER);
        bar = new ProgressBar();
        bar.setPrefWidth(450.00);
        ImageView img = new ImageView();
        img.setImage(new Image(getClass().getResourceAsStream("style/nv128.png")));
        cn.getChildren().add(img);
        cn.getChildren().add(bar);
        return new Scene(cn, 455, 150);        
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        stage.setScene(createPreloaderScene());        
        stage.show();
    }
    
    @Override
    public void handleStateChangeNotification(StateChangeNotification scn) {
        if (scn.getType() == StateChangeNotification.Type.BEFORE_START) {
            stage.hide();
        }
    }
    
    @Override
    public void handleProgressNotification(ProgressNotification pn) {
        bar.setProgress(pn.getProgress());
    }    
}
