package n3;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;
import lv.nmc.entities.StaffEntry;

import lv.nmc.rest.LoginService;

/**
 *
 * @author jm
 */
public class LoginWindow implements Initializable {

    @FXML
    private Circle testCircle, testCircle2, testCircle3;
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private ImageView logoImage;
    private WinLoader dialogs = new WinLoader();
   // private Session session;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // Platform.runLater(http);
    }

   /* public void makeTunnel() throws Exception {
        String host = ResourceBundle.getBundle("resources.settings").getString("tunnelHost");
        String user = ResourceBundle.getBundle("resources.settings").getString("sshUsername");
        String password = ResourceBundle.getBundle("resources.settings").getString("sshPassword");
        Integer port = Integer.parseInt(ResourceBundle.getBundle("resources.settings").getString("tunnelPort"));
        JSch jsch = new JSch();
        int tunnelLocalPort = Integer.parseInt(ResourceBundle.getBundle("resources.settings").getString("userPort"));
        String tunnelRemoteHost = ResourceBundle.getBundle("resources.settings").getString("serviceHost");
        int tunnelRemotePort = Integer.parseInt(ResourceBundle.getBundle("resources.settings").getString("apiPort"));

        this.session = jsch.getSession(user, host, port);
        this.session.setPassword(password);

        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");

        LocalUserInfo lui = new LocalUserInfo();
        lui.passwd = ResourceBundle.getBundle("resources.settings").getString("sshPassword");

        this.session.setUserInfo(lui);

        try {
            this.session.setServerAliveInterval(300000);
            this.session.setTimeout(300000);
            this.session.setDaemonThread(true);
            this.session.connect();
            this.session.setPortForwardingL(tunnelLocalPort, tunnelRemoteHost, tunnelRemotePort);

        } catch (Exception ee) {
           // ee.printStackTrace();
        }

    }*/

    @FXML
    void loginUser(ActionEvent ev) {

       /* Task<Integer> tnl = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                makeTunnel();
                return 1;
            }
        };*/

        Task<Integer> login = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                StaffEntry ee = new StaffEntry();
                ee.setEvent("checkLogin");
                ee.setPassword(passwordField.getText());
                ee.setUsername(usernameField.getText());
                List<StaffEntry> fl = new ArrayList<>();
                List<StaffEntry> groups = new ArrayList<>();
                StringBuilder sb = new StringBuilder();

                try {
                    fl = LoginService.fetchList(ee, ee.getEvent());
                    ee.setEvent("selectGroups");
                    groups = LoginService.fetchList(ee, ee.getEvent());
                } catch (Exception ex) {                    
                    Logger.getLogger(LoginWindow.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (fl.isEmpty() || groups.isEmpty()) {
                   // session.disconnect();
                    dialogs.initMessage("Can't verify identity. Check your username and password or contact support team.");
                    return 0;
                }
                
                for (StaffEntry sse : groups) {
                    sb.append(sse.getGroupName());
                }
                
                System.getProperties().put("n3.username", ee.getUsername());
                System.getProperties().put("n3.password", ee.getPassword());
                System.getProperties().put("n3.groups", ee.getPassword());
                
                try {
                    
                } catch(Exception ese) {
                    Logger.getLogger(LoginWindow.class.getName()).log(Level.SEVERE, null, ese);
                }
                
                if (fl.isEmpty()) {
                    
                }

                try {
                    Parent root = FXMLLoader.load(getClass().getResource("/n3/dialogs/RegistrationCalendar.fxml"));
                    Stage stage = (Stage) usernameField.getScene().getWindow();
                    Scene scene = new Scene(root);
                    stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv32.png")));
                    stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv48.png")));
                    stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv64.png")));
                    stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv128.png")));
                    stage.setScene(scene);
                    stage.setTitle("nReg 3.0 - Novikontas Maritime College");
                    stage.show();                   

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return 1;
            }
        };
        
        //Platform.runLater(tnl);
        Platform.runLater(login);

    }

    @FXML
    private void lostAccount(ActionEvent ev) {
        dialogs.initMessage("Contact our support team via it@novikontas.lv");
    }

    private void testFire() throws Exception {
    }

    @FXML
    private void mouseEntered2(MouseEvent event) {
        FadeTransition ft = new FadeTransition(Duration.millis(300), testCircle2);
        ft.setFromValue(0.4);
        ft.setToValue(1.0);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);

        ft.play();
    }

    @FXML
    private void mouseExited2(MouseEvent event) {
        FadeTransition ft = new FadeTransition(Duration.millis(300), testCircle2);
        ft.setFromValue(1.0);
        ft.setToValue(0.4);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);

        ft.play();
    }

    @FXML
    private void mouseEntered3(MouseEvent event) {
        FadeTransition ft = new FadeTransition(Duration.millis(300), testCircle3);

        ft.setFromValue(0.6);
        ft.setToValue(1.0);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ScaleTransition sc = new ScaleTransition(Duration.millis(300), testCircle);
        sc.setToX(2d);
        sc.setToY(2d);
        sc.setAutoReverse(true);
        sc.setCycleCount(1);

        ft.play();
        sc.play();
    }

    @FXML
    private void mouseExited3(MouseEvent event) {
        FadeTransition ft = new FadeTransition(Duration.millis(300), testCircle3);
        ft.setFromValue(1.0);
        ft.setToValue(0.6);
        ft.setCycleCount(1);
        ft.setAutoReverse(true);
        ScaleTransition sc = new ScaleTransition(Duration.millis(300), testCircle);
        sc.setToX(1d);
        sc.setToY(1d);
        sc.setAutoReverse(true);
        sc.setCycleCount(1);

        ft.play();
        sc.play();
    }
}
