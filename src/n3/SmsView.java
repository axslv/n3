package n3;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.HTMLEditor;
import javafx.util.Callback;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.entities.Sms;
import lv.nmc.rest.SmsService;
import n3.table.cells.SmsCommentCell;
import n3.table.cells.SmsHiddenCell;
import n3.table.cells.SmsNotifiedCell;
import n3.table.cells.SmsRegCell;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;

public class SmsView
        implements Initializable {

    @FXML
    private Label statusLabel; // Value injected by FXMLLoader    
    @FXML
    private ProgressBar progressBar; // Value injected by FXMLLoader   
    private WinLoader dialogs = new WinLoader();
    @FXML
    private TableView<Sms> SmsTable;
    @FXML
    private TableColumn<Sms, Integer> smsId;
    @FXML
    private TableColumn<Sms, String> nameSurname;
    @FXML
    private TableColumn<Sms, String> documentType;
    @FXML
    private TableColumn<Sms, String> smsDate;
    @FXML
    private TableColumn<Sms, String> smsStatus;
    @FXML
    private TableColumn<Sms, String> phoneNumber;
    @FXML
    private TableColumn<Sms, String> smsComment;
    @FXML
    private TableColumn<Sms, Boolean> clientNotified;
    @FXML
    private TableColumn<Sms, String> tvalidTill;
    @FXML
    private TableColumn<Sms, Boolean> clientRegistered;
    @FXML
    private TableColumn<Sms, Boolean> clientHidden;
    @FXML
    private TextArea smsText;
    @FXML
    private ComboBox smsFilter;
    @FXML
    private CheckBox showHidden;
    private Sms selectedSms = new Sms();
    private List<Sms> smsList = new ArrayList<>();
    private DateTimeFormatter dtf = DateTimeFormat.forPattern("YYYY-MM-dd");
    private LinkedHashMap<String, Integer> filterMap = new LinkedHashMap<>();
    @FXML
    private Label emailsForYear, emailsForMonth, emailsForToday;
    @FXML
    private TableView<RegistrationEntry> appTable;
    @FXML
    private TableColumn<RegistrationEntry, Integer> apptId;
    @FXML
    private TableColumn<RegistrationEntry, String> apptNameSurname, 
            apptPersonCode, apptGroupName, apptDateEnd, apptEmailDate, 
            apptEmailAddress, apptEmailStatus;
    @FXML
    private TextArea spamOutput;

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        initSmsTable();
        smsFilter.getItems().clear();
        smsFilter.getItems().add("=< 1 month");
        smsFilter.getItems().add("2 month");
        smsFilter.getItems().add("3 monthes");
        smsFilter.getItems().add("ALL");

        filterMap.put("=< 1 month", 1);
        filterMap.put("2 month", 3);
        filterMap.put("3 monthes", 4);
        filterMap.put("ALL", 0);
        initAppTable();
        initSpamTable();
    }

    private void initAppTable() {
        appTable.getSelectionModel().setCellSelectionEnabled(true);
        appTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        appTable.setEditable(true);

        apptId.setCellValueFactory(
                new PropertyValueFactory<RegistrationEntry, Integer>("registrationId"));
        apptNameSurname.setCellValueFactory(
                new PropertyValueFactory<RegistrationEntry, String>("nameSurname"));
        apptPersonCode.setCellValueFactory(
                new PropertyValueFactory<RegistrationEntry, String>("personCode"));
        apptGroupName.setCellValueFactory(
                new PropertyValueFactory<RegistrationEntry, String>("groupName"));
        apptDateEnd.setCellValueFactory(
                new PropertyValueFactory<RegistrationEntry, String>("dateEnd"));
        apptEmailDate.setCellValueFactory(
                new PropertyValueFactory<RegistrationEntry, String>("dateC"));
        apptEmailAddress.setCellValueFactory(
                new PropertyValueFactory<RegistrationEntry, String>("phone"));
        apptEmailStatus.setCellValueFactory(
                new PropertyValueFactory<RegistrationEntry, String>("rank"));

    }

    private void initSmsTable() {
        SmsTable.getSelectionModel().setCellSelectionEnabled(true);
        SmsTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        SmsTable.setEditable(true);
        clientNotified.setEditable(true);
        smsComment.setEditable(true);

        smsId.setCellValueFactory(
                new PropertyValueFactory<Sms, Integer>("sms_id"));
        nameSurname.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("name_surname"));
        documentType.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("document_type"));
        smsDate.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("date_start"));
        smsStatus.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("status_verbose"));
        phoneNumber.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("phone"));
        clientNotified.setCellValueFactory(
                new PropertyValueFactory<Sms, Boolean>("notified"));
        clientRegistered.setCellValueFactory(
                new PropertyValueFactory<Sms, Boolean>("registered"));
        clientHidden.setCellValueFactory(
                new PropertyValueFactory<Sms, Boolean>("hidden"));
        smsComment.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("comment"));
        tvalidTill.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("valid_till"));

        Callback<TableColumn<Sms, Boolean>, TableCell<Sms, Boolean>> notaFactory
                = new Callback<TableColumn<Sms, Boolean>, TableCell<Sms, Boolean>>() {
            @Override
            public TableCell<Sms, Boolean> call(TableColumn<Sms, Boolean> p) {
                return new SmsNotifiedCell();
            }
        };

        Callback<TableColumn<Sms, Boolean>, TableCell<Sms, Boolean>> regFactory
                = new Callback<TableColumn<Sms, Boolean>, TableCell<Sms, Boolean>>() {
            @Override
            public TableCell<Sms, Boolean> call(TableColumn<Sms, Boolean> p) {
                return new SmsRegCell();
            }
        };

        Callback<TableColumn<Sms, Boolean>, TableCell<Sms, Boolean>> hiddenFactory
                = new Callback<TableColumn<Sms, Boolean>, TableCell<Sms, Boolean>>() {
            @Override
            public TableCell<Sms, Boolean> call(TableColumn<Sms, Boolean> p) {
                return new SmsHiddenCell();
            }
        };

        Callback<TableColumn<Sms, String>, TableCell<Sms, String>> commentFactory
                = new Callback<TableColumn<Sms, String>, TableCell<Sms, String>>() {
            @Override
            public TableCell<Sms, String> call(TableColumn<Sms, String> p) {
                return new SmsCommentCell();
            }
        };

        smsComment.setCellFactory(commentFactory);
        clientNotified.setCellFactory(notaFactory);
        clientRegistered.setCellFactory(regFactory);
        clientHidden.setCellFactory(hiddenFactory);

        try {
            selectedSms.setEvent("selectReports");
            smsList = SmsService.fetchList(selectedSms, selectedSms.getEvent());
            SmsTable.getItems().addAll(smsList);
        } catch (Exception ex) {
            Logger.getLogger(SmsView.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void filterSms(ActionEvent ev) {
        String option = smsFilter.getValue().toString();
        Integer interval = filterMap.get(option);
        DateTime dtNow = new DateTime();

        if (0 == filterMap.get(option)) {
            initSmsTable();
            return;
        }

        DateTime dtPlus = dtNow.plusMonths(filterMap.get(option));
        selectedSms.setEvent("selectFilteredReports");
        selectedSms.setDate_start(dtNow.toString(dtf));
        selectedSms.setReoccurence(dtPlus.toString(dtf));

        try {
            smsList = SmsService.fetchList(selectedSms, selectedSms.getEvent());
            SmsTable.getItems().clear();
            SmsTable.getItems().addAll(smsList);
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    private Boolean isHidden = false;

    @FXML
    private void fetchHidden(ActionEvent ev) {
        List<Sms> lst = SmsTable.getItems();

        if (isHidden) {
            initSmsTable();
            isHidden = false;
            return;
        }

        try {
            selectedSms.setEvent("selectHiddenReports");
            smsList = SmsService.fetchList(selectedSms, selectedSms.getEvent());
            SmsTable.getItems().clear();
            SmsTable.getItems().addAll(smsList);
            isHidden = true;
        } catch (Exception ex) {
            Logger.getLogger(SmsView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void f5Pressed(KeyEvent ev) {

    }

    private void setMessage(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: green");
        progressBar.setProgress(1D);

    }

    private void setError(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: red");
        progressBar.setProgress(1D);
    }

    private void setProgress(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: #0000FF");
        progressBar.setProgress(-1D);
    }

    private void setWarning(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: #F25509");
        progressBar.setProgress(1D);
    }

    @FXML
    private TableView<Sms> spamTable;
    @FXML
    private TableColumn<Sms, Integer> spId;
    @FXML
    private TableColumn<Sms, String> spText, spName, spType, spFrom, spReplyTo, spSubject, spTargets;
    @FXML
    private HTMLEditor notaText;
    @FXML
    private ComboBox notaType, notaCriteria;
    @FXML
    private TextField notaSubject, notaMailFrom, notaReplyTo, notaSchedule, notaAgeLimit, notaSysName;
    @FXML
    private CheckBox limitByAge;
    @FXML
    private Button notaSaveButton;
    @FXML
    private TextArea notaWho, msgOutput;
    private Boolean filterAges = false;

    private void initSpamTable() {
        spamTable.getSelectionModel().setCellSelectionEnabled(true);
        spamTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        spamTable.setEditable(false);

        spId.setCellValueFactory(
                new PropertyValueFactory<Sms, Integer>("sms_id"));
        spText.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("preview"));
        spName.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("sys_name"));
        spType.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("nota_type"));
        spFrom.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("mail_from"));
        spReplyTo.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("reply_to"));
        spSubject.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("email_subject"));
        spTargets.setCellValueFactory(
                new PropertyValueFactory<Sms, String>("nota_target"));
        notaType.getItems().clear();
        notaType.getItems().add("EMAIL");
        notaType.getItems().add("SMS");

        notaCriteria.getItems().clear();
        notaCriteria.getItems().add("By rank");
        notaCriteria.getItems().add("By country");
        notaCriteria.getItems().add("By rank AND country");

        Sms sms = new Sms();
        sms.setEvent("selectNotifications");

        spamTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observale, Object oldValue, Object newValue) {
                Sms ge = (Sms) newValue;

                if (null == ge) {
                    return;
                }
                selectedSms = ge;
                notaText.setHtmlText(ge.getMessage());
                notaSubject.setText(ge.getEmail_subject());
                notaMailFrom.setText(ge.getMail_from());
                notaReplyTo.setText(ge.getReply_to());
                notaSchedule.setText(ge.getDate_start());

                if (ge.getAge_limit() != 0) {
                    notaAgeLimit.setText(ge.getAge_limit().toString());
                    limitByAge.setSelected(true);
                    notaAgeLimit.setEditable(false);
                } else {
                    limitByAge.setSelected(false);
                    notaAgeLimit.clear();
                    notaAgeLimit.setEditable(true);
                }
                notaWho.setText(ge.getNota_filter());
                notaSaveButton.setDisable(false);
                notaType.getSelectionModel().select(ge.getNota_type().toUpperCase());
                notaCriteria.getSelectionModel().select(ge.getNota_criteria());
                notaSysName.setText(ge.getSys_name());
//                selectClients.setDisable(false);
            }
        });

        try {
            spamTable.getItems().addAll(SmsService.fetchList(sms, sms.getEvent()));
        } catch (Exception eee) {
            dialogs.initUserError(eee.getMessage());
        }

    }

    @FXML
    private void criteriaSelected(ActionEvent ev) {
        if (notaCriteria.getValue().toString().equalsIgnoreCase("By rank AND country")) {
            dialogs.initMessage("Example:\nCountry: 371,370,44\nRank: ch/off, 2off, 3off, etc");
        }
    }

    @FXML
    private void pushTheLimits(ActionEvent ev) {
        notaAgeLimit.setEditable(limitByAge.isSelected());
        System.out.println("push the limits");
    }

    @FXML
    private void publishNota(ActionEvent ev) {

    }

    @FXML
    private void saveNota(ActionEvent ev) {
        Sms push = spamTable.getSelectionModel().getSelectedItem();
        push.setMessage(notaText.getHtmlText());
        push.setNota_type(notaType.getValue().toString());
        push.setEmail_subject(notaSubject.getText());
        push.setMail_from(notaMailFrom.getText());
        push.setReply_to(notaReplyTo.getText());
        push.setNota_criteria(notaCriteria.getValue().toString());
        push.setNota_filter(notaWho.getText());
        push.setDate_start(notaSchedule.getText());
        push.setSys_name(notaSysName.getText());
        push.setEvent("_updateNota");

        if (limitByAge.isSelected()) {
            Pattern pt = Pattern.compile("[0-9]{2}-[0-9]{2}");
            Pattern pt1 = Pattern.compile("[0-9]{2}");
            if (!pt.matcher(notaAgeLimit.getText()).matches() && !pt1.matcher(notaAgeLimit.getText()).matches()) {
                dialogs.initWarning("Allowed formats: e.g. 25-45 OR 25");
                return;
            }
            push.setAge_limit(Integer.parseInt(notaAgeLimit.getText()));
        }
        // System.out.println(selectedSms.getNota_type());
        if (notaType.getValue().toString().equalsIgnoreCase("sms")) {
            push.setMessage(Jsoup.parse(push.getMessage()).text());
            //System.out.println(Jsoup.parse(push.getMessage()).text());
        }

        try {
            SmsService.updateRecord(push, push.getEvent());
            int i = spamTable.getSelectionModel().getSelectedIndex();
            spamTable.getItems().add(spamTable.getSelectionModel().getSelectedIndex(), push);
            spamTable.getItems().remove(i + 1);
            spamTable.getColumns().get(0).setVisible(false);
            spamTable.getColumns().get(0).setVisible(true);
            //  spamTable.getItems().add(push);
            // initSpamTable();
        } catch (Exception e) {
            dialogs.initUserError(e.getMessage());
        }
        
        selectClients();
    }

    
    @FXML
    private TextField uriField;
    @FXML
    private Button selectClients, performSend, checkDetails;

    private List<Sms> msgList = new ArrayList<>();
    private List<Sms> msgCache = new ArrayList<>();
    private List<String> crit = new ArrayList<>();
    private StringBuilder sb = new StringBuilder();

    private void extractTargets() {
        crit.clear();
        msgCache.clear();
        String[] ranks = notaWho.getText().split(",");
        Collections.addAll(crit, ranks);
        for (String str : crit) {
            sb.append(str.trim());
            sb.append(",");
        }
    }

    private void selectByCountry() {
        extractTargets();
        try {
            msgList = SmsService.fetchByLongList(replaceLast(sb.toString(), ",", ""), "list", "byCountry");
            msgCache.addAll(msgList);
            msgList.clear();
        } catch (Exception ex) {
            Logger.getLogger(SmsView.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void selectByRanks() {
        extractTargets();
        try {
            msgList = SmsService.fetchByLongList(replaceLast(sb.toString(), ",", ""), "list", "byRank");
            msgCache.addAll(msgList);
            msgList.clear();
        } catch (Exception ex) {
            Logger.getLogger(SmsView.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void selectByRankAndCountry() {
        String[] filters = notaWho.getText().replace("Rank: ", "").replace("Country: ", "").split("\\n" );
        String[] fieldNames = {"list1", "list"};

        try {
            msgList = SmsService.fetchByMultipleLists(filters, fieldNames, "byRankAndCountry");
            msgCache.addAll(msgList);
            msgList.clear();
        } catch (Exception ex) {
            Logger.getLogger(SmsView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void selectClients() {

        if (null == selectedSms.getEmail_subject()) {
            dialogs.initWarning("EMAILS CAN'T BE SENT unless email subject is present. Switch to \"Properties\" page and correct it.");
            return;
        }

        switch (notaCriteria.getValue().toString()) {

            case "By rank":
                if (notaWho.getText().equals("*")) {
                    //perform sending to ALL ranks
                } else {

                    if (notaWho.getText().isEmpty()) {
                        dialogs.initWarning("No ranks provided. Stop.");
                        return;
                    }

                    selectByRanks();
                }
                break;

            case "By country":
                if (notaWho.getText().equals("*")) {
                    //perform sending to ALL ranks
                } else {

                    if (notaWho.getText().isEmpty()) {
                        dialogs.initWarning("No country code provided. Stop.");
                        return;
                    }

                    selectByCountry();
                    // checkDetails.setDisable(false);
                    //System.out.println(msgCache.size());
                }
                break;

            case "By rank AND country":
                String filter = notaWho.getText();
                String[] filters = filter.split("\\n");

                if (filters.length != 2) {
                    dialogs.initMessage("What exactly you can't understand? Allowed format:\nCountry: 371, 370, 44\nRank: ch/off, 2off, 3off, etc");
                }

                Pattern pt = Pattern.compile("(Country:) [0-9]{2,4}(,\\s?[0-9]{2,4})+$");
                Pattern pt1 = Pattern.compile("(Rank:) .{2,12}(,\\s?.{2,12})+$");

                if (!pt.matcher(filters[0]).matches()) {
                    dialogs.initMessage("What exactly you can't understand? Allowed format:\nCountry: 371, 370, 44\nRank: ch/off, 2off, 3off, etc");
                    return;
                }

                if (!pt1.matcher(filters[1]).matches()) {
                    dialogs.initMessage("What exactly you can't understand? Allowed format:\nCountry: 371, 370, 44\nRank: ch/off, 2off, 3off, etc");
                    return;
                }

                selectByRankAndCountry();
                break;

        }

        if (limitByAge.isSelected()) {
            this.filterAges = true;
        }
//        checkDetails.setDisable(false);
        msgOutput.appendText("CAREFULLY check message details (such as address, subject and so on)\n");
        msgOutput.appendText("Please, note! You can't cancel sending process. So, DOUBLE CHECK or SUFFER CONSEQUENCES\n");
        msgOutput.appendText("You MAY close this window anytime\n");
        msgOutput.appendText("Messages will be sent exactly on " + notaSchedule.getText() + "\n");
        msgOutput.appendText("------------------------------------------------------------------------\n");
        msgOutput.appendText("Estimated message count: " + msgCache.size() + "\n");
        selectMessage();

    }

    private void selectMessage() {
        sb = new StringBuilder();
        String htmlMessage = notaText.getHtmlText();
        String plainMessage = Jsoup.parse(htmlMessage).text();
        String nType = notaType.getValue().toString();

        for (Sms sm : msgCache) {
            if (nType.equalsIgnoreCase("email")) {
                sm.setMessage(notaText.getHtmlText());
                continue;
            }
            sm.setMessage(plainMessage);
        }
    }

    @FXML
    private void checkDetails() {
        performSend.setDisable(false);
        msgOutput.appendText("User " + System.getProperty("n3.username") + " has confirmed that messaging details are correct\n");
    }
    
    @FXML
    private void createClient(ActionEvent ec) {
        
    }




    private <T> List<List<T>> chopped(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }

    private String replaceLast(String string, String substring, String replacement) {
        int index = string.lastIndexOf(substring);
        if (index == -1) {
            return string;
        }
        return string.substring(0, index) + replacement
                + string.substring(index + substring.length());
    }

}

//Library1337
