/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import n3.dialogs.shared.DialogState;

/**
 *
 * @author User
 */
public class WinLoader {

    private List<File> files = new ArrayList<>();
    private File selectedFile;

    public File showSaveDialog(Stage st) {
        FileChooser sf = new FileChooser();

        List<FileChooser.ExtensionFilter> filters = new ArrayList<>();

        filters.add(new FileChooser.ExtensionFilter("CAT Structure (*.catx)", "*.catx", "*.CATX"));
        sf.getExtensionFilters().addAll(filters);

        sf.setTitle("Save CAT template");
        selectedFile = sf.showSaveDialog(st);
        return selectedFile;
    }

    public File showOpenDialog(Stage st) {
        FileChooser sf = new FileChooser();

        List<FileChooser.ExtensionFilter> filters = new ArrayList<>();

        filters.add(new FileChooser.ExtensionFilter("CAT Structure (*.catx)", "*.catx", "*.CATX"));
        sf.getExtensionFilters().addAll(filters);

        sf.setTitle("Open CAT template");
        selectedFile = sf.showOpenDialog(st);
        return selectedFile;
    }

    public File showOpenPdf(Stage st) {
        FileChooser sf = new FileChooser();

        List<FileChooser.ExtensionFilter> filters = new ArrayList<>();

        filters.add(new FileChooser.ExtensionFilter("CAT Structure (*.pdf)", "*.pdf", "*.PDF"));
        sf.getExtensionFilters().addAll(filters);

        sf.setTitle("Open PDF document");
        selectedFile = sf.showOpenDialog(st);
        return selectedFile;
    }

    public List<File> chooseMultipleFiles(Stage st) {
        FileChooser sf = new FileChooser();

        List<FileChooser.ExtensionFilter> filters = new ArrayList<>();

        filters.add(new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.jpg", "*.JPG", "*.jpeg", "*.JPEG"));
        filters.add(new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png", "*.PNG"));
        filters.add(new FileChooser.ExtensionFilter("SVG files (*.svg)", "*.svg", "*.SVG"));
        sf.getExtensionFilters().addAll(filters);

        sf.setTitle("Select images");
        files = sf.showOpenMultipleDialog(st);
        return files;
    }

    public void switchPage(Stage st, String fxFile) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(fxFile));
        Scene scene = new Scene(root);
        scene.getStylesheets().addAll(N3.class.getResource("style/simple_calendar.css").toExternalForm());
        scene.getStylesheets().addAll(N3.class.getResource("/com/sai/javafx/calendar/styles/calendar_styles.css").toExternalForm());

        try {
            st.setScene(scene);
            st.show();
        } catch (Exception ee) {
            ee.printStackTrace();
        }

    }

    public void initModal(String fxFile, String title) throws Exception {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource(fxFile));
        Scene scene = new Scene(root);
        scene.getStylesheets().addAll(N3.class.getResource("style/simple_calendar.css").toExternalForm());
        scene.getStylesheets().addAll(N3.class.getResource("/com/sai/javafx/calendar/styles/calendar_styles.css").toExternalForm());
        stage.setResizable(false);
        stage.setScene(scene);
        stage.setTitle(title);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv48.png")));
        stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv64.png")));
        stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv128.png")));

        stage.show();
    }

    public void initSimpleWindow(String fxFile, String title) throws Exception {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource(fxFile));

        Scene scene = new Scene(root);
        
        scene.getStylesheets().addAll(N3.class.getResource("style/simple_calendar.css").toExternalForm());
        scene.getStylesheets().addAll(N3.class.getResource("/com/sai/javafx/calendar/styles/calendar_styles.css").toExternalForm());
        stage.setScene(scene);
        stage.setTitle(title);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv32.png")));
        stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv48.png")));
        stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv64.png")));
        stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv128.png")));
        stage.show();
    }

    public void initConfirmDialog(String message) {
        try {
            DialogState.getInstance().setLastMessage(message);
            Stage stage = new Stage();

            stage.initModality(Modality.APPLICATION_MODAL);
            Parent root = FXMLLoader.load(getClass().getResource("/n3/dialogs/ConfirmDialog.fxml"));
            Scene scene = new Scene(root);
            stage.setResizable(false);
            stage.initStyle(StageStyle.UTILITY);
            stage.setScene(scene);
            stage.setTitle("Please confirm");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv32.png")));
            stage.showAndWait();
        } catch (IOException ex) {
            Logger.getLogger(WinLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
        public void initReasonDialog() {
        try {
            
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            Parent root = FXMLLoader.load(getClass().getResource("/n3/dialogs/ReasonDialog.fxml"));
            Scene scene = new Scene(root);
            stage.setResizable(false);
            stage.initStyle(StageStyle.UTILITY);
            stage.setScene(scene);
            stage.setTitle("Data changing reason");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv32.png")));
            stage.showAndWait();
        } catch (IOException ex) {
            Logger.getLogger(WinLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initErrorDialog(String message) {
        DialogState.getInstance().setLastError(message);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);

        try {
            Parent root = FXMLLoader.load(getClass().getResource("/n3/dialogs/ErrorDialog.fxml"));
            Scene scene = new Scene(root);
            stage.setResizable(false);
            stage.initStyle(StageStyle.UTILITY);
            stage.setScene(scene);
            stage.setTitle("Application error");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv32.png")));
        } catch (IOException ex) {
            Logger.getLogger(WinLoader.class.getName()).log(Level.SEVERE, null, ex);
        }

        stage.showAndWait();
    }

    public void initUserError(String message) {
        try {
            DialogState.getInstance().setLastError(message);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            Parent root = FXMLLoader.load(getClass().getResource("/n3/dialogs/UserError.fxml"));
            Scene scene = new Scene(root);
            stage.setResizable(false);
            stage.initStyle(StageStyle.UTILITY);
            stage.setScene(scene);
            stage.setTitle("User error");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv32.png")));
            stage.showAndWait();
        } catch (IOException ex) {
            Logger.getLogger(WinLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initWarning(String message) {
        try {
            DialogState.getInstance().setLastError(message);
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            Parent root = FXMLLoader.load(getClass().getResource("/n3/dialogs/UserError.fxml"));
            Scene scene = new Scene(root);
            stage.setResizable(false);
            stage.initStyle(StageStyle.UTILITY);
            stage.setScene(scene);
            stage.setTitle("User error");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("style/nv32.png")));
            stage.showAndWait();
        } catch (IOException ex) {
            Logger.getLogger(WinLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initMessage(String message) {
        try {
            DialogState.getInstance().setLastError(message);
            Parent root = FXMLLoader.load(getClass().getResource("/n3/dialogs/UserMessage.fxml"));

            Stage stage = new Stage();
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/n3/style/nv32.png")));
            stage.initStyle(StageStyle.UTILITY);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);

            stage.setTitle("Message");

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.showAndWait();

        } catch (IOException ex) {
            Logger.getLogger(WinLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Integer getResult() {
        return DialogState.getInstance().getState();
    }
}
