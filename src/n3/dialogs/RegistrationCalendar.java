package n3.dialogs;

//<editor-fold desc="import-block">
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Callback;
import lv.nmc.entities.ClientEntry;
import lv.nmc.entities.CourseEntry;
import lv.nmc.entities.GroupEntry;
import lv.nmc.entities.JournalEntry;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.rest.ClientService;
import lv.nmc.rest.CourseService;
import lv.nmc.rest.GroupService;
import lv.nmc.rest.JournalService;
import lv.nmc.rest.RegistrationService;
import lv.nmc.shared.Companies;
import lv.nmc.shared.LastClient;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;
import n3.dialogs.shared.Registrations;
import n3.helpers.Enrollment;
import n3.style.EmptyCalendar;
import n3.table.cells.CompanyCell;
import n3.table.cells.CourseDetailsCell;
import n3.table.cells.CrewCommentCell;
import n3.table.cells.DateStartCell;
import n3.table.cells.NameSurnameCell;
import n3.table.cells.PriceCell;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
//</editor-fold>

public class RegistrationCalendar
        implements Initializable {

    //<editor-fold desc="Variable declaration block">
    private HashMap<String, Button> courseButtons = new HashMap<>();
    private ClientEntry selectedClient = new ClientEntry();
    private WinLoader windows = new WinLoader();
    @FXML
    private WebView reglistView;
    @FXML
    private Button tcRegisterAll;

    @FXML
    private Button tcDeleteSelected;
    @FXML
    private Button tcSaveTable;
    @FXML
    private Button pppRegisterAll;
    @FXML
    private Button pppRegisterSelected;
    @FXML
    private Button pppDeleteSelected;
    @FXML
    private Button pppSaveTable;
    @FXML //  fx:id="registerButton"
    private Hyperlink selectedUser; // Value injected by FXMLLoader    
    @FXML //  fx:id="searchResults"
    private VBox searchResults; // Value injected by FXMLLoader
    @FXML //  fx:id="searchResults"
    private VBox searchResults2; // Value injected by FXMLLoader   
    @FXML //  fx:id="webView"
    private TextField searchString; // Value injected by FXMLLoader 
    @FXML //  fx:id="webView"
    private TextField fIntervalStart; // Value injected by FXMLLoader    

    @FXML //  fx:id="webView"
    private SplitPane splitPane; // Value injected by FXMLLoader  
    @FXML // fx:id="tcCourseTitle"
    private TableColumn<RegistrationEntry, String> tcCourseTitle; // Value injected by FXMLLoader
    @FXML // fx:id="tcCrewComment"
    private TableColumn<RegistrationEntry, String> tcCrewComment; // Value injected by FXMLLoader
    @FXML // fx:id="tcCrewCompany"
    private TableColumn<RegistrationEntry, String> tcCrewCompany; // Value injected by FXMLLoader
    @FXML // fx:id="tcPreregPrice"
    private TableColumn<RegistrationEntry, String> tcPreregPrice; // Value injected by FXMLLoader
    @FXML // fx:id="tcPreregSelected"
    private TableColumn<RegistrationEntry, Boolean> tcPreregSelected; // Value injected by FXMLLoader
    @FXML // fx:id="tcRegistrationDate"
    private TableColumn<RegistrationEntry, String> tcRegistrationDate; // Value injected by FXMLLoader
    @FXML // fx:id="tcCourseTitle"
    private TableColumn<RegistrationEntry, String> pppCourseTitle; // Value injected by FXMLLoader
    @FXML // fx:id="tcCrewComment"
    private TableColumn<RegistrationEntry, String> pppCrewComment; // Value injected by FXMLLoader
    @FXML // fx:id="tcCrewCompany"
    private TableColumn<RegistrationEntry, String> pppCrewCompany; // Value injected by FXMLLoader
    @FXML // fx:id="tcPreregPrice"
    private TableColumn<RegistrationEntry, String> pppPreregPrice; // Value injected by FXMLLoader
    @FXML // fx:id="tcPreregSelected"
    private TableColumn<RegistrationEntry, Boolean> pppPreregSelected; // Value injected by FXMLLoader
    @FXML // fx:id="tcRegistrationDate"
    private TableColumn<RegistrationEntry, String> pppRegistrationDate; // Value injected by FXMLLoader  
    @FXML
    private TableColumn<RegistrationEntry, String> tcNameSurname;
    @FXML // fx:id="tcRegistrationDate"
    private TextField cardScan, courseTitle1c; // Value injected by FXMLLoader      
    @FXML
    private TableView<RegistrationEntry> tcPrereg;
    @FXML
    private TableView pppPrereg;
    @FXML
    private VBox calendarBox;
    @FXML
    private Hyperlink selectedTime;
    @FXML
    private Label statusLabel, currentDomain;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private TextField regdate;
    @FXML
    private TextField preNameSurname;
    @FXML
    private TextField fGroupAbbr;
    @FXML
    private WebView addScheduleView;
    @FXML
    private ListView<String> requiredDocuments;
    @FXML
    private ComboBox<String> documentsList;
    @FXML
    private ComboBox certTemplate;

    private DateTimeFormatter dtf = DateTimeFormat.forPattern("YYYY-MM-dd");
    private DateTimeFormatter dtt = DateTimeFormat.forPattern("[EEEE] MMM dd");
    private DateTimeFormatter dtfLv = DateTimeFormat.forPattern("dd/MM/YYYY");
    private DateTimeFormatter dtfD = DateTimeFormat.forPattern("E");
    private List<CourseEntry> selectedCourses = new ArrayList<CourseEntry>();
    private CourseEntry selectedCourse = new CourseEntry();
    private List<GroupEntry> selectedGroups = new ArrayList<GroupEntry>();
    private GroupEntry selectedGroup = new GroupEntry();
    private String domain = "tc";
    private Integer maxDays = 43;
    private HashMap<Integer, List<String>> dateMap = new HashMap<>();
    private DateTime dateTime = new DateTime();
    private WinLoader dialogs = new WinLoader();
    private RegistrationEntry currentRegistration = new RegistrationEntry();
    List<RegistrationEntry> selectedRegs = new ArrayList<>();
    private List<RegistrationEntry> companiesList = new ArrayList<>();
    @FXML
    private StackPane stp;

//</editor-fold>   
    
    // <editor-fold desc="Application init">
    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        regdate.setText(dateTime.toString(dtf));
        searchString.setStyle("-fx-font-size: 15px");
        initTcTable();
        updateTimeHeader();
        fetchCalendar(new ActionEvent());
        fetchPreregistered(new ActionEvent());
        Tooltip tp = new Tooltip("Refresh view, F5");
        tp.setStyle("-fx-font: Normal 18 monospace");
        selectedTime.setTooltip(tp);
        fetchCompanies();

        CourseEntry ce = new CourseEntry();
        ce.setEvent("selectTemplates");
        List<CourseEntry> ls = new ArrayList<>();
        certTemplate.getItems().clear();

        try {
            ls = CourseService.fetchList(ce, ce.getEvent());
        } catch (Exception ee) {
            ee.printStackTrace();
        }

        for (CourseEntry cec : ls) {
            certTemplate.getItems().add(cec.getCourseName());
        }

        certTemplate.getItems().add("");
        domainSelector.getItems().clear();
        domainSelector.getItems().add("TC");
        domainSelector.getItems().add("PPP NAV");
        domainSelector.getItems().add("PPP ENG");
        domainSelector.getItems().add("COL ENG");
        domainSelector.getItems().add("COL NAV");
        domainSelector.getItems().add("PVN");
        domainSelector.getItems().add("GWO");
        
        cbDomain.getItems().clear();
        cbDomain.getItems().addAll(domainSelector.getItems());

    }

    private void fetchCompanies() {
        RegistrationEntry re = new RegistrationEntry();
        re.setEvent("fetchCompanies");

        try {
            companiesList = RegistrationService.fetchList(re, re.getEvent());
            for (RegistrationEntry inner : companiesList) {
                Companies.getInstance().getLastCompanies().put(inner.getCompany(), inner);
                Companies.getInstance().setCompaniesList(companiesList);
            }

        } catch (Exception ex) {
            Logger.getLogger(CompanyCell.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    @FXML
    private void openApp(ActionEvent ev) {
        final String dialog = "/n3/appaisal/AppHome.fxml";
        final String tt = "nReg 3.0 - Appaisal forms";

        try {

            dialogs.initSimpleWindow(dialog, tt);

        } catch (Exception ex) {
            dialogs.initErrorDialog(ex.toString());
        }
    }

    @FXML
    private void removeDocument(ActionEvent ev) { //remove document from course requirements

        if (null == requiredDocuments.getSelectionModel().getSelectedItem()) {
            return;
        }

        Task<Integer> start = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Saving documents ...");
                return 1;
            }
        };

        Task<Integer> stop = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Ready");
                return 1;
            }
        };

        Task<Integer> progress = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                selectedCourse.setDocumentName(requiredDocuments.getSelectionModel().getSelectedItem());
                selectedCourse.setEvent("_deleteDeleteCourseDocument");
                CourseService.updateRecord(selectedCourse, selectedCourse.getEvent());
                requiredDocuments.getItems().remove(requiredDocuments.getSelectionModel().getSelectedItem());
                return 1;
            }
        };

        Platform.runLater(start);
        Platform.runLater(progress);
        Platform.runLater(stop);

    }

    @FXML
    private void addDocument(ActionEvent ev) { //add document to course requirements
        if (null == documentsList.getValue()) {
            return;
        }

        if (requiredDocuments.getItems().contains(documentsList.getValue())) {
            return;
        }

        Task<Integer> start = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Saving documents ...");
                return 1;
            }
        };

        Task<Integer> stop = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Ready");
                return 1;
            }
        };

        Task<Integer> progress = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                selectedCourse.setEvent("_updateInsertCourseDocument");
                selectedCourse.setDocumentName(documentsList.getValue());
                CourseService.updateRecord(selectedCourse, selectedCourse.getEvent());
                requiredDocuments.getItems().add(documentsList.getValue());
                return 1;
            }
        };

        Platform.runLater(start);
        Platform.runLater(progress);
        Platform.runLater(stop);

    }

    private void fetchCourseDocuments() { //fetch list of required documents
        CourseEntry cs = new CourseEntry();

        cs.setEvent("allCourseDocuments");
        List<CourseEntry> allist = new ArrayList<>();
        try {
            allist = CourseService.fetchList(cs, cs.getEvent());
        } catch (Exception ex) {
            Logger.getLogger(RegistrationCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }

        documentsList.getItems().clear();

        for (CourseEntry cse : allist) {
            documentsList.getItems().add(cse.getDocumentName());
        }

        cs.setEvent("courseDocuments");
        cs.setCourseId(selectedCourse.getCourseId());

        try {
            List<CourseEntry> doclist = new ArrayList<>();

            doclist = CourseService.fetchList(cs, cs.getEvent());

            requiredDocuments.getItems().clear();

            for (CourseEntry docs : doclist) {
                requiredDocuments.getItems().add(docs.getDocumentName());
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }

    }

    @FXML
    private void searchPreregistered(KeyEvent ev) { //search preregistered client (those who have registration date, but still with unconfirmed registration) by name and surname

        if (preNameSurname.getText().length() <= 5) {
            return;
        }

        Task<Integer> http = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                currentRegistration.setNameSurname(preNameSurname.getText());
                currentRegistration.setEvent("searchPreregistered");
                selectedRegs = RegistrationService.fetchList(currentRegistration, currentRegistration.getEvent());
                tcPrereg.getItems().clear();
                tcPrereg.getItems().addAll(selectedRegs);
                return 1;
            }
        };

        Task<Integer> not = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working...");
                return 1;
            }
        };

        Task<Integer> done = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");
                return 1;
            }
        };

        Platform.runLater(not);
        Platform.runLater(http);
        Platform.runLater(done);
    }

    @FXML
    private void fetchPreregistered(ActionEvent ev) { //fetch all preregistered clients by registration date
        Task<Integer> http = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                // System.out.println("Why so butthurt?");

                currentRegistration.setDateEnd(regdate.getText());
                currentRegistration.setEvent("selectPreregistrations");
                selectedRegs = RegistrationService.fetchList(currentRegistration, currentRegistration.getEvent());
                tcPrereg.getItems().clear();
                tcPrereg.getItems().addAll(selectedRegs);
                return 1;
            }
        };

        Task<Integer> not = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Working...");
                return 1;
            }
        };

        Task<Integer> done = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Done");
                return 1;
            }
        };

        Platform.runLater(not);
        Platform.runLater(http);
        Platform.runLater(done);
    }
    //</editor-fold> 

    // <editor-fold desc="Calendar construction and navigation">
    @FXML
    private void f5Pressed(KeyEvent ev) { //refresh all views

        if (ev.getCode().equals(KeyCode.F5)) {
            ActionEvent eva = new ActionEvent();

            fetchCalendar(new ActionEvent());
            fetchPreregistered(new ActionEvent());
        }
    }

    @FXML
    private void fetchCalendar(ActionEvent ev) { //fetch course calendar

        final Task<Integer> gc = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                try {
                    if (!byCourse) {
                        selectedCourse.setEvent("coursesByDomain");
                        selectedCourse.setDomain(domain);
                        selectedCourses = CourseService.fetchList(selectedCourse, selectedCourse.getEvent());
                    }

                    for (CourseEntry ce : selectedCourses) {
                        selectedGroup.getCourseIdsList().add(ce.getCourseId());
                    }
                    selectedGroup.setEvent("fetchSchedule");

                    selectedGroup.setDateStart(dateTime.toString(dtf));
                    selectedGroup.setDateEnd(dateTime.plusDays(maxDays).toString(dtf));
                    selectedGroups = GroupService.fetchList(selectedGroup, selectedGroup.getEvent());

                    for (GroupEntry ge : selectedGroups) {
                        if (null == dateMap.get(ge.getCourseId())) {
                            dateMap.put(ge.getCourseId(), new ArrayList<String>());
                            dateMap.get(ge.getCourseId()).add(ge.getDateStart());
                        } else {
                            dateMap.get(ge.getCourseId()).add(ge.getDateStart());
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return 0;
            }
        };

        Task<Integer> gc2 = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                calendarBox.getChildren().clear();
                selectedGroup.getCourseIdsList().clear();
                for (CourseEntry ce : selectedCourses) {

                    if (!byCourse) {
                        constructRow(ce, (dateTime.toString(dtf)));
                        continue;
                    }

                    if (null != fGroupAbbr.getText() && ce.getGroupAbbr().toLowerCase().contains(fGroupAbbr.getText().toLowerCase())) {
                        constructRow(ce, (dateTime.toString(dtf)));
                    }

                }
                return 1;
            }
        };

        Task<Integer> gc3 = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Loading schedule...");
                return 1;
            }
        };

        Task<Integer> gc4 = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                byCourse = false;
                setMessage("Schedule ready");
                return 1;
            }
        };

        Platform.runLater(gc3);

        Platform.runLater(gc);

        Platform.runLater(gc2);

        Platform.runLater(gc4);
    }

    @FXML
    private TextField fCourseTitle, fLength, fLengthH, fProtocolNumber, ftGroupAbbr, fCertPrefix, fValid;
    @FXML
    private TextArea fCourseDescriptionLv, fCourseDescriptionEn, fCertHeaderLv, fCertHeaderEn, fStcw, tCourseDescription;
    @FXML
    private ComboBox cbDepartment, cbDomain;
    @FXML
    private TextField price1, price2, price3, pvn, room, auditor, address;
    private CourseEntry courseSend = new CourseEntry();

    @FXML
    protected void saveCourseDetails(ActionEvent event) { //apply changes to course details

        try {
            courseSend.setCourseName(fCourseTitle.getText());
            courseSend.setGroupAbbr(ftGroupAbbr.getText());
            courseSend.setDescription(tCourseDescription.getText());
            courseSend.setLength(Integer.parseInt(fLength.getText()));
            courseSend.setHours(Integer.parseInt(fLengthH.getText()));
            courseSend.setAttAbbr(fProtocolNumber.getText());
            courseSend.setCertNr(fProtocolNumber.getText());
            courseSend.setDescriptionLv(fCourseDescriptionLv.getText());
            courseSend.setDescriptionEn(fCourseDescriptionEn.getText());
            courseSend.setCertHeaderLv(fCertHeaderLv.getText());
            courseSend.setCertHeaderEn(fCertHeaderEn.getText());
            courseSend.setCertId(fCertPrefix.getText());
            courseSend.setValidity(fValid.getText());
            courseSend.setStcwCodes(fStcw.getText());
            courseSend.setDepartment(cbDepartment.getValue().toString());
            courseSend.setDomain(cbDomain.getValue().toString().toLowerCase().replace(" ", "_"));

            if (null != certTemplate.getValue()) {
                courseSend.setDocumentName(certTemplate.getValue().toString());
            }

            if (cbDepartment.getValue().toString().equals("Department")) {
                courseSend.setDepartment("n/a");
            }

            courseSend.setCurrentPrice(price1.getText());
            courseSend.setPriceFirstLvl(price2.getText());
            courseSend.setPriceSecondLvl(price3.getText());

            courseSend.setPvn(pvn.getText());
            courseSend.setRoom(room.getText());
            courseSend.setAuditor(auditor.getText());
            courseSend.setAddress(address.getText());
            courseSend.setEvent("_updateChangeCourseDetails");
            courseSend.setCourseTitle1c(courseTitle1c.getText());
        } catch (Exception ee) {
            ee.printStackTrace();
        }
        //  courseSend = selectedCourse;

        try {
            CourseService.updateRecord(courseSend, courseSend.getEvent());
        } catch (Exception ex) {
            Logger.getLogger(RegistrationCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    protected void saveNewCourse(ActionEvent event) { //add and save new course

        courseSend.setCourseName(fCourseTitle.getText());
        courseSend.setGroupAbbr(ftGroupAbbr.getText());
        courseSend.setDescription(tCourseDescription.getText());
        courseSend.setLength(Integer.parseInt(fLength.getText()));
        courseSend.setHours(Integer.parseInt(fLengthH.getText()));
        courseSend.setAttAbbr(fProtocolNumber.getText());
        courseSend.setCertNr(fProtocolNumber.getText());
        courseSend.setDescriptionLv(fCourseDescriptionLv.getText());
        courseSend.setDescriptionEn(fCourseDescriptionEn.getText());
        courseSend.setCertHeaderLv(fCertHeaderLv.getText());
        courseSend.setCertHeaderEn(fCertHeaderEn.getText());
        courseSend.setCertId(fCertPrefix.getText());
        courseSend.setValidity(fValid.getText());
        courseSend.setStcwCodes(fStcw.getText());
        courseSend.setDepartment(cbDepartment.getValue().toString());
        courseSend.setDomain(cbDomain.getValue().toString().toLowerCase().replace(" ", "_"));

        if (cbDepartment.getValue().toString().equals("Department")) {
            courseSend.setDepartment("n/a");
        }

        courseSend.setCurrentPrice(price1.getText());
        courseSend.setPriceFirstLvl(price2.getText());
        courseSend.setPriceSecondLvl(price3.getText());
        courseSend.setPvn(pvn.getText());
        courseSend.setRoom(room.getText());
        courseSend.setAuditor(auditor.getText());
        courseSend.setAddress(address.getText());
        courseSend.setEvent("_updateInsertCourse");
        courseSend.setDocumentName(certTemplate.getValue().toString());
        courseSend.setCourseTitle1c(courseTitle1c.getText());

        try {
            CourseService.updateRecord(courseSend, courseSend.getEvent());
        } catch (Exception ex) {
            Logger.getLogger(RegistrationCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void fetchCourseDetails(String cs) { //fetch and display course details
        final String tt = cs;
        Task<Integer> start = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setProgress("Loading ...");
                return 1;
            }
        };

        Task<Integer> stop = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                setMessage("Ready");
                return 1;
            }
        };

        Task<Integer> process = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                try {
                    selectedCourse.setGroupAbbr(tt);
                    selectedCourse.setEvent("courseDetailsByAbbr");
                    List<CourseEntry> ls = CourseService.fetchList(selectedCourse, selectedCourse.getEvent());

                    if (null == ls) {
                        return 1;
                    }
                    CourseEntry selectedCourse1 = new CourseEntry();

                    selectedCourse1 = ls.get(0);
                    courseSend.setCourseId(selectedCourse1.getCourseId());
                    fCourseTitle.setText(selectedCourse1.getCourseName());
                    ftGroupAbbr.setText(selectedCourse1.getGroupAbbr());
                    tCourseDescription.setText(selectedCourse1.getDescription());
                    fLength.setText(selectedCourse1.getLength().toString());
                    fLengthH.setText(selectedCourse1.getHours().toString());
                    fProtocolNumber.setText(selectedCourse1.getCertNr());
                    fCourseDescriptionLv.setText(selectedCourse1.getDescriptionLv());
                    fCourseDescriptionEn.setText(selectedCourse1.getDescriptionEn());
                    fCertHeaderLv.setText(selectedCourse1.getCertHeaderLv());
                    fCertHeaderEn.setText(selectedCourse1.getCertHeaderEn());
                    fCertPrefix.setText(selectedCourse1.getCertId());
                    fValid.setText(selectedCourse1.getValidity());
                    fStcw.setText(selectedCourse1.getStcwCodes());
                    cbDepartment.getSelectionModel().select(selectedCourse1.getDepartment());
                    cbDomain.getSelectionModel().select(selectedCourse1.getDomain().toUpperCase().replace("_", " "));
                    price1.setText(selectedCourse1.getCurrentPrice());
                    price2.setText(selectedCourse1.getPriceFirstLvl());
                    price3.setText(selectedCourse1.getPriceSecondLvl());
                    pvn.setText(selectedCourse1.getPvn());
                    room.setText(selectedCourse1.getRoom());
                    auditor.setText(selectedCourse1.getAuditor());
                    address.setText(selectedCourse1.getAddress());
                    selectedCourse.setCourseId(selectedCourse1.getCourseId());
                    certTemplate.setValue(selectedCourse1.getDocumentName());
                    courseTitle1c.setText(selectedCourse1.getCourseTitle1c());
                    fetchCourseDocuments();
                } catch (Exception ee) {
                    ee.printStackTrace();

                }
                return 1;
            }
        };

        Platform.runLater(start);
        Platform.runLater(process);
        Platform.runLater(stop);

    }

    private boolean daysConstructed = false;

    private void constructRow(CourseEntry ge, String dateStart) { //calendar cells construction

        DateTime intervalStart = dtf.parseDateTime(dateStart);
        Integer courseId = ge.getCourseId();
        int days = maxDays;
        HashMap<Boolean, String> searchMap = new HashMap();

        HBox hb = new HBox();
        hb.setSpacing(3d);
        hb.setMaxWidth(1275);
        hb.setMinWidth(1275);

        Button cs = new Button(ge.getGroupAbbr());
        Tooltip tp = new Tooltip(ge.getCourseName());
        tp.getStyleClass().add("tpFont");
        tp.setAutoHide(false);
        cs.setMaxWidth(68d);
        cs.setMinWidth(68d);
        cs.getStyleClass().add("titleButton");
        cs.setTooltip(tp);
        final String tt = cs.getText();
        cs.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                displaySchAdder(tt);
                fetchCourseDetails(tt);
            }
        });

        courseButtons.put(ge.getCourseId().toString(), cs);

        hb.setId(ge.getCourseId().toString());
        hb.getChildren().add(cs);

        searchMap.put(false, "defaultLink");
        searchMap.put(true, "greenLink");

        for (int f = 0; f <= days; f++) {
            DateTime display = intervalStart.plusDays(f);

            EmptyCalendar hp = new EmptyCalendar(String.format("%02d", display.getDayOfMonth()));

            Tooltip tp0 = new Tooltip();
            tp0.getStyleClass().add("tpFont");

            tp0.setText(display.toString(dtt));
            tp.setAutoHide(false);
            tp0.setAutoHide(false);
            hp.setTooltip(tp0);

            if (null != dateMap.get(courseId)) {
                if (dateMap.get(courseId).contains(display.toString(dtf))) {

                    hp.getStyleClass().add("greenLink");
                    hp.setId(hb.getId() + "-" + display.toString(dtf));

                    //  System.out.println(display.toString(dtfD));
                    final String id = hp.getId();
                    hp.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent t) {
                            new ClientActions(id).registerUser();
                        }
                    });
                }
                hp.checkHoliday(display.toString(dtfD));
                hb.getChildren().add(hp);
            }
        }
        calendarBox.getChildren().add(hb);
    }

    private void displaySchAdder(String course) { //display schedule adding webpage

        for (CourseEntry ce : selectedCourses) {
            if (course.equals(ce.getGroupAbbr())) {
                addScheduleView.getEngine().load(ResourceBundle.getBundle("resources.urls").getString("WebIncludes")
                        + ResourceBundle.getBundle("resources.urls").getString("ScheduleInclude")
                        + "?_eventName=courseSchedule&courseId=" + ce.getCourseId().toString());

                reglistView.getEngine().load(new StringBuilder().append(ResourceBundle.getBundle("resources.urls").getString("WebIncludes")).
                        append(ResourceBundle.getBundle("resources.urls").getString("RegInclude")).
                        append("?_eventName=showReglist&courseId=").
                        append(ce.getCourseId().toString()).
                        append("&courseName=").
                        append(ce.getGroupAbbr())
                        .toString());

                selectedCourse.setEvent("courseDetailsByAbbr");
                selectedCourse.setGroupAbbr(course);

                fetchCourseDetails(course);

                break;
            }
        }
    }

    private boolean byCourse = false;

    @FXML
    private void scheduleByCourse(KeyEvent ev) { //display particular course schedule
        byCourse = true;
        fetchCalendar(new ActionEvent());
    }

    @FXML
    private void selectSearch(MouseEvent ev) { // wtf?
        fGroupAbbr.selectAll();
    }

    @FXML
    private void nextInterval(ActionEvent ev) { //scroll calendar by particular time ahead
        this.dateTime = this.dateTime.plusDays(maxDays);
        updateTimeHeader();
        fetchCalendar(ev);
    }

    @FXML
    private void prevInterval(ActionEvent ev) { //scroll calendar by particular time back
        this.dateTime = this.dateTime.minusDays(maxDays);
        updateTimeHeader();
        fetchCalendar(ev);
    }

    @FXML
    private void calendarJump(ActionEvent ev) { //display calendar on particular date

        try {
            this.dateTime = dtf.parseDateTime(fIntervalStart.getText());
        } catch (Exception eef) {
            setError("Invalid date format. Should be YYYY-MM-DD");
            return;
        }
        updateTimeHeader();
        fetchCalendar(ev);

    }

    @FXML
    private void saveRecord(ActionEvent ev) {

    }

    private void updateTimeHeader() { //wtf?
        selectedTime.setText(dateTime.toString(dtfLv) + " to " + dateTime.plusDays(maxDays).toString(dtfLv));

    }

    @FXML
    private ComboBox domainSelector;

    @FXML
    private void changeDomain(ActionEvent ev) { //change domain (tc, ppp or college) to display calendar with courses in that domain
        this.domain = domainSelector.getValue().toString().replace(" ", "_").toLowerCase();
        setMessage("Domain changed to " + domain);
        // currentDomain.setText("Current domain: " + domain);
    }
    //</editor-fold>       

    // <editor-fold desc="Preregistration table init">
    private void initTcTable() { //Preregistration table init
        tcPrereg.getSelectionModel().setCellSelectionEnabled(true);
        tcPrereg.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        //   tcRegistrationDate.setCellValueFactory(new PropertyValueFactory<RegistrationEntry, String>("dateStart"));
        tcPrereg.setEditable(true);

        tcRegistrationDate.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<RegistrationEntry, String> p) {
                return new SimpleStringProperty(p.getValue().getDateStart());
            }
        });

        tcCourseTitle.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<RegistrationEntry, String> p) {
                return new SimpleStringProperty(p.getValue().getCourseName());
            }
        });

        tcNameSurname.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<RegistrationEntry, String> p) {
                return new SimpleStringProperty(p.getValue().getNameSurname());
            }
        });

        tcPreregPrice.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<RegistrationEntry, String> p) {
                return new SimpleStringProperty(p.getValue().getPrice());
            }
        });

        tcCrewComment.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<RegistrationEntry, String> p) {
                return new SimpleStringProperty(p.getValue().getCrewComment());
            }
        });

        tcCrewCompany.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<RegistrationEntry, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<RegistrationEntry, String> p) {
                return new SimpleStringProperty(p.getValue().getCompany());
            }
        });

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> courseFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new CourseDetailsCell();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> dateFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new DateStartCell();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> priceFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new PriceCell();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> nameFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new NameSurnameCell();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> companyFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new CompanyCell();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> crewCommentFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn p) {
                return new CrewCommentCell();
            }
        };

        tcNameSurname.setCellFactory(nameFactory);
        tcCrewCompany.setCellFactory(companyFactory);
        tcCrewComment.setCellFactory(crewCommentFactory);
        tcPreregPrice.setCellFactory(priceFactory);
        tcCourseTitle.setCellFactory(courseFactory);
        tcRegistrationDate.setCellFactory(dateFactory);

    }

    // </editor-fold>  
    
    //<editor-fold desc="User search and display">
    @FXML
    private void searchUser(KeyEvent ev) {
        searchResults.getChildren().clear();
        searchResults2.getChildren().clear();
        Task<Integer> ts = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                List<ClientEntry> clients = new ArrayList<>();
                ClientEntry ce = new ClientEntry();
                ce.setNameSurname(searchString.getText());
                ce.setPersonCode(searchString.getText());
                ce.setPhone(searchString.getText());
                ce.setnConnect(searchString.getText());
                //camelCaseOnly!!!

                clients = ClientService.fetchList(ce, "searchUser");

                if (clients.isEmpty()) {
                    displayUserAdd();
                    searchString.setEditable(true);
                    searchString.end();
                    return 0;
                }

                pCount = 0;
                for (ClientEntry ice : clients) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(ice.getNameSurname());
                    sb.append(" (");
                    sb.append(ice.getPersonCode());
                    sb.append(")");
                    sb.append(" [");
                    sb.append(ice.getLevel());
                    sb.append("]");

                    Hyperlink hp = new Hyperlink(sb.toString());
                    hp.setStyle("-fx-font-size: 11px; -fx-text-fill: navy");
                    // final String usr = sb.toString();
                    final ClientEntry usr = ice;
                    hp.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent t) {
                            selectedClient = usr;
                            selectUser();
                        }
                    });

                    Hyperlink detaLink = new Hyperlink("[i]");
                    detaLink.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent t) {
                            LastClient.getInstance().setLastClient(usr);
                            usr.setEvent("clientDetails");
                            quickDetails(usr);
                        }
                    });

                    HBox quickBox = new HBox();
                    quickBox.getChildren().add(hp);
                    quickBox.getChildren().add(detaLink);

                    if (pCount > 4) {
                        searchResults2.getChildren().add(quickBox);

                    } else {
                        searchResults.getChildren().add(quickBox);
                    }
                    pCount++;
                }

                searchString.setEditable(true);
                searchString.end();

                return 0;

            }
        };

        if (null != searchString.getText() && searchString.getText().length() >= 4) {
            searchString.setEditable(false);

            Task<Integer> not = new Task<Integer>() {
                @Override
                protected Integer call() throws Exception {
                    setProgress("Working...");
                    return 1;
                }
            };

            Task<Integer> done = new Task<Integer>() {
                @Override
                protected Integer call() throws Exception {
                    setMessage("Done");
                    return 1;
                }
            };

            Platform.runLater(not);
            Platform.runLater(ts);
            Platform.runLater(done);
            preNameSurname.setText(searchString.getText());
            searchPreregistered(ev);

        }
    }

    private void displayUserAdd() {
        String st = "Add user: " + searchString.getText();
        Hyperlink hr = new Hyperlink(st);
        hr.setStyle("-fx-font-size: 16px; -fx-text-fill: navy");

        hr.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                WinLoader wl = new WinLoader();
                try {
                    ClientEntry ce = new ClientEntry();
                    ce.setNameSurname(searchString.getText());
                    ce.setPersonCode("000000-00000");
                    Registrations.getInstance().setSharedClient(ce);
                    wl.initModal("/n3/dialogs/UserAdd.fxml", "Add user: " + searchString.getText());
                    //label.setText("Accepted");
                } catch (Exception ex) {
                    wl.initErrorDialog(ex.getMessage());
                    Logger.getLogger(RegistrationCalendar.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        searchResults.getChildren().clear();
        searchResults.getChildren().add(hr);
    }

    private void selectUser() {
        StringBuilder sb = new StringBuilder();
        sb.append(selectedClient.getNameSurname());
        sb.append(" (");
        sb.append(selectedClient.getPersonCode());
        sb.append(")");
        sb.append(" [");
        sb.append(selectedClient.getLevel());
        sb.append("]");
        selectedUser.setText(sb.toString());
        LastClient.getInstance().setLastClient(selectedClient);
        selectedClient.setEvent("clientDetails");
    }

    private void quickDetails(ClientEntry toRegister) {
        final String dialog = "/n3/dialogs/UserDetails.fxml";
        final String tt = toRegister.getNameSurname() + " - client details";

        Task<Integer> openWindow = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                try {

                    WinLoader wl = new WinLoader();
                    wl.initModal(dialog, tt);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                return 0;
            }
        };
        openWindow.run();
    }

    @FXML
    private void userDetails(ActionEvent ev) {
        quickDetails(selectedClient);
    }

    //</editor-fold>
    
    //<editor-fold desc="Event handlers">
    @FXML
    private void openCollege(ActionEvent ev) {

        final String dialog = "/n3/dialogs/RegistrationCalendarCol.fxml";
        final String tt = "nReg 3.0 - COLLEGE";

        try {
            // dialogs.switchPage((Stage) tcPrereg.getScene().getWindow(), dialog);
            dialogs.initSimpleWindow(dialog, tt);

        } catch (Exception ex) {
            dialogs.initErrorDialog(ex.toString());
            ex.printStackTrace();

        }
    }
    
        @FXML
    private void openPayments(ActionEvent ev) {

        final String dialog = "/n3/dialogs/Payments.fxml";
        final String tt = "nReg 3.0 - Payments";

        try {
            // dialogs.switchPage((Stage) tcPrereg.getScene().getWindow(), dialog);
            dialogs.initSimpleWindow(dialog, tt);

        } catch (Exception ex) {
            dialogs.initErrorDialog(ex.toString());
            ex.printStackTrace();

        }
    }

    @FXML
    private void openGroupView(ActionEvent ev) {

        final String dialog = "/n3/grouplist.fxml";
        //   final String tt = "nReg 3.0 - Group view";

        try {
            dialogs.switchPage((Stage) tcPrereg.getScene().getWindow(), dialog);

        } catch (Exception ex) {
            dialogs.initErrorDialog(ex.toString());
            ex.printStackTrace();
        }
    }

    @FXML
    private void openRei(ActionEvent ev) {
        final String dialog = "/n3/dialogs/Reissue.fxml";
        final String tt = "nReg 3.0 - Certificate reissue";

        try {
            // dialogs.switchPage((Stage) tcPrereg.getScene().getWindow(), dialog);
            dialogs.initSimpleWindow(dialog, tt);

        } catch (Exception ex) {
            dialogs.initErrorDialog(ex.toString());
        }
    }

    @FXML
    private void openGroupViewN(ActionEvent ev) {
        final String dialog = "/n3/grouplist.fxml";
        final String tt = "nReg 3.0 - Group view";

        try {
            // dialogs.switchPage((Stage) tcPrereg.getScene().getWindow(), dialog);
            dialogs.initSimpleWindow(dialog, tt);

        } catch (Exception ex) {
            dialogs.initErrorDialog(ex.toString());

        }
    }
    
    @FXML    
    private void openSms(ActionEvent ev) {
        final String dialog = "/n3/sms.fxml";
        final String tt = "N3 - SMS notifications";

        try {
            // dialogs.switchPage((Stage) tcPrereg.getScene().getWindow(), dialog);
            dialogs.initSimpleWindow(dialog, tt);

        } catch (Exception ex) {
            dialogs.initErrorDialog(ex.toString());

        }
    }

    @FXML
    private void deleteTcSelected(ActionEvent event) {

        if (tcPrereg.getSelectionModel().getSelectedItems().isEmpty()) {
            return;
        }

        dialogs.initConfirmDialog("Sure to delete selected preregistrations?");

        if (DialogState.getInstance().getState() == 1) {
            return;
        }

        ObservableList<RegistrationEntry> toDelete = tcPrereg.getSelectionModel().getSelectedItems();

        JournalEntry je = new JournalEntry();
        je.setUsername(System.getProperty("n3.username"));
        je.setOptype("delete");

        try {
            for (RegistrationEntry re : toDelete) {
                je.setRegistrationId(re.getRegistrationId());
                je.setEvent("_updateUpdateRecord");
                je.setSysmsg("Username " + je.getUsername() + " deleted preregistration record with registrationId=" + re.getRegistrationId());
                JournalService.insertRecord(je, je.getEvent());
                re.setEvent("_deletePreregistration");
                RegistrationService.updateRecord(re, re.getEvent());
            }
        } catch (Exception ee) {
            setError("Error deleting preregistration");
            ee.printStackTrace();
        }

        fetchPreregistered(new ActionEvent());

    }

    @FXML
    private void collapseCalendar(ActionEvent event) {
        splitPane.setDividerPosition(0, 0.05);
    }

    @FXML
    private void expandCalendar(ActionEvent event) {
        splitPane.setDividerPosition(0, 0.4979);
    }
    private Integer pCount = 0;

    @FXML
    private void cardScanned(ActionEvent ev) {
    }

    private void setProgress(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: green");
        progressIndicator.setVisible(true);
    }

    private void setError(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: red");
        progressIndicator.setVisible(false);
    }

    private void setMessage(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: green");
        progressIndicator.setVisible(false);
    }
//</editor-fold>

    private class ClientActions {

        private String cellId;
        private RegistrationEntry currentReg = new RegistrationEntry();
        //   private DateTime currentTime = new DateTime();
        private CourseEntry selectedCourse = new CourseEntry();

        public ClientActions(String id) {
            this.cellId = id;

        }

        private void fetchCourseDetails() {

            selectedCourse.setCourseId(currentReg.getCourseId());
            selectedCourse.setEvent("courseDetails");
            List<CourseEntry> ls = new ArrayList<>();
            try {
                ls = CourseService.fetchList(selectedCourse, selectedCourse.getEvent());

                if (!ls.isEmpty()) {
                    selectedCourse = ls.get(0);
                }
                //    System.out.println(selectedCourse.getCourseName());

            } catch (Exception ex) {
                Logger.getLogger(RegistrationCalendar.class.getName()).log(Level.SEVERE, null, ex);
            }

            //toRegister.getLevel();
        }

        public void registerUser() {

            if (null == selectedClient.getUserId()) {
                return;
            }

            Task<Integer> update = new Task<Integer>() {
                @Override
                protected Integer call() throws Exception {
                    List<RegistrationEntry> re = new ArrayList<>();
                    String[] spl = cellId.split("\\-");
                    currentReg.setCourseId(Integer.parseInt(spl[0]));
                    StringBuilder sb = new StringBuilder().append(spl[1]).append("-").append(spl[2]).append("-").append(spl[3]);
                    DateTime dt = dtf.parseDateTime(sb.toString());
                    fetchCourseDetails();
                    currentReg.setCourseName(selectedCourse.getCourseName());
                    currentReg.setDateStart(dt.toString(dtf));
                    currentReg.setDateC(dt.toString(dtf));
                    currentReg.setUserId(selectedClient.getUserId());
                    currentReg.setNameSurname(selectedClient.getNameSurname());
                    currentReg.setPrice(selectedCourse.getPriceFirstLvl());
                    currentReg.setEvent("checkPreRegistrations");
                    
                    re = RegistrationService.fetchList(currentReg, currentReg.getEvent());
                    

                    if (!re.isEmpty()) {
                        String date = re.get(0).getDateStart();
                        dialogs.initUserError("Client already PREREGISTERED on: " + date + " Course: " + currentReg.getCourseName());
                        return 1;
                    }

                    JournalEntry je = new JournalEntry();
                    je.setUsername(System.getProperty("n3.username"));
                    je.setOptype("insert");
                    je.setUserId(currentReg.getUserId());
                    je.setCourseId(currentReg.getCourseId());
                    je.setDateStart(currentReg.getDateC());
                    je.setSysmsg("[PREREGISTRATION] Username " + System.getProperty("n3.username") + " has registered client with ID=" + currentReg.getUserId()
                            + " to group with dateStart=" + currentReg.getDateStart() + " and course \"" + currentReg.getCourseName() + "\"");

                    currentReg.setEvent("_updateSaveRegistration");
                    RegistrationService.updateRecord(currentReg, currentReg.getEvent());
                    JournalService.insertRecord(je, je.getEvent());

                    currentReg.setEvent("_insertInitialCrew");
                    RegistrationService.updateRecord(currentReg, currentReg.getEvent());
                    currentReg.setEvent("_insertInitialDiscount");
                    RegistrationService.updateRecord(currentReg, currentReg.getEvent());
                    
                    if (!domain.contains("col")) {
                        return 0;
                    }
                    
                    
                    
                    GroupEntry ge = new GroupEntry();
                    List<GroupEntry> lge = new ArrayList<>();
                    ge.setCourseId(currentReg.getCourseId());
                    ge.setDateStart(currentReg.getDateStart());
                    ge.setEvent("groupByDateStart");
                    
                    try {
                        lge = GroupService.fetchList(ge, ge.getEvent());
                        System.out.println(lge.get(0).getGroupId());
                    } catch (Exception ee) {
                        
                    }
                    
                    Enrollment.entrollRecord(currentReg, selectedGroup);

                    
                    return 0;
                }
            };

            Task<Integer> not = new Task<Integer>() {
                @Override
                protected Integer call() throws Exception {
                    setProgress("Working...");
                    return 1;
                }
            };

            Task<Integer> done = new Task<Integer>() {
                @Override
                protected Integer call() throws Exception {
                    setMessage("Done");
                    return 1;
                }
            };

            Task<Integer> fetchPre = new Task<Integer>() {
                @Override
                protected Integer call() throws Exception {
                    fetchPreregistered(new ActionEvent());
                    return 1;
                }
            };

            Platform.runLater(not);
            Platform.runLater(update);
            Platform.runLater(done);
            Platform.runLater(fetchPre);

        }
    }
}
