package n3.dialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.web.WebView;

public class UserCert
        implements Initializable {

    @FXML //  fx:id="userCertWeb"
    private WebView userCertWeb; // Value injected by FXMLLoader
    @FXML //  fx:id="backView"
    private Button backView; // Value injected by FXMLLoader
    @FXML //  fx:id="frontView"
    private Button frontView; // Value injected by FXMLLoader
    @FXML //  fx:id="printView"
    private Button printView; // Value injected by FXMLLoader

    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        assert userCertWeb != null : "fx:id=\"userCertWeb\" was not injected: check your FXML file 'UserCert.fxml'.";

        ImageView front = new ImageView();
        ImageView back = new ImageView();
        ImageView print = new ImageView();

        front.setImage(new Image(getClass().getResourceAsStream("images/front.png")));
        back.setImage(new Image(getClass().getResourceAsStream("images/back.png")));
        print.setImage(new Image(getClass().getResourceAsStream("images/print.png")));

        frontView.setGraphic(front);
        backView.setGraphic(back);
        printView.setGraphic(print);

        userCertWeb.getEngine().load(ResourceBundle.getBundle("resources.urls").getString("CertFactoryHtml"));
        // initialize your logic here: all @FXML variables will have been injected


    }
}
