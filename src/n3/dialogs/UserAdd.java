package n3.dialogs;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.web.WebView;
import lv.nmc.entities.ClientEntry;
import lv.nmc.rest.ClientService;
import lv.nmc.shared.LastClient;
import n3.WinLoader;
import n3.dialogs.shared.Registrations;

public class UserAdd
        implements Initializable {

    @FXML //  fx:id="company"  
    private Label pLang; // Value injected by FXMLLoader
    @FXML //  fx:id="company"  
    private TextField company; // Value injected by FXMLLoader
    @FXML //  fx:id="email"
    private TextField email; // Value injected by FXMLLoader
    @FXML //  fx:id="homeAddress"
    private TextField homeAddress; // Value injected by FXMLLoader
    @FXML //  fx:id="nameSurname"
    private TextField nameSurname; // Value injected by FXMLLoader
    @FXML //  fx:id="ncPhone"
    private TextField ncPhone; // Value injected by FXMLLoader
    @FXML //  fx:id="personCode"
    private TextField personCode; // Value injected by FXMLLoader
    @FXML //  fx:id="phoneNumber"
    private TextField phoneNumber; // Value injected by FXMLLoader
    @FXML //  fx:id="rank"
    private TextField rank, ccode, secPhoneNumber, contract; // Value injected by FXMLLoader
    @FXML //  fx:id="userComments"
    private TextArea userComments; // Value injected by FXMLLoader
    @FXML //  fx:id="userDocumentsWeb"
    private WebView userDocumentsWeb; // Value injected by FXMLLoader
    @FXML //  fx:id="vipCheckBox"
    private CheckBox vipCheckBox; // Value injected by FXMLLoader
    @FXML //  fx:id="vipCheckBox"
    private CheckBox smsCheckBox; // Value injected by FXMLLoader   
    @FXML //  fx:id="vipCheckBox"
    private ComboBox domainBox; // Value injected by FXMLLoader
    @FXML //  fx:id="vipCheckBox"
    private ComboBox<String> prefLangBox; // Value injected by FXMLLoader    
    @FXML //  fx:id="vipCheckBox"
    private Button saveButton; // Value injected by FXMLLoader   
    @FXML //  fx:id="vipCheckBox"
    private Button cancelButton; // Value injected by FXMLLoader  
    @FXML //  fx:id="vipCheckBox"
    private Label statusLabel; // Value injected by FXMLLoader    
    @FXML //  fx:id="vipCheckBox"
    private ProgressBar progressBar; // Value injected by FXMLLoader   
    @FXML
    private TextField sbNum;
    private ClientEntry currentEntry = LastClient.getInstance().getLastClient();
    private String oldPersonCode = "";
    private boolean unique = true;
    private WinLoader dialogs = new WinLoader();

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        currentEntry = Registrations.getInstance().getSharedClient();
        //   currentEntry.setPrefLang("");
        pLang.setText("(lv)");
        nameSurname.setText(currentEntry.getNameSurname());
        personCode.setText(currentEntry.getPersonCode());
        sbNum.setText(currentEntry.getMarineBookId());

        ImageView ok = new ImageView();
        ImageView cancel = new ImageView();

        ok.setImage(new Image(getClass().getResourceAsStream("images/ok.png")));
        cancel.setImage(new Image(getClass().getResourceAsStream("images/cancel.png")));

        saveButton.setGraphic(ok);
        cancelButton.setGraphic(cancel);
        // userDocumentsWeb.getEngine().load(ResourceBundle.getBundle("resources.urls").getString("UserDocumentsHtml") + "?user_id=" + LastClient.getInstance().getLastClient().getUserId());
    }

    @FXML
    protected void exitDialog(ActionEvent ev) {
        ((Button) ev.getSource()).getScene().getWindow().hide();
    }

    @FXML
    protected void saveRecord(ActionEvent ev) {

        if (nameSurname.getText().isEmpty()) {
            dialogs.initUserError("Name and surname can't be empty!");
            //JOptionPane.showMessageDialog(null, "Name and surname cannot be empty!", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (personCode.getText().isEmpty()) {
            dialogs.initUserError("Person code can't be empty!");
            // JOptionPane.showMessageDialog(null, "Person code cannot be empty!", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        currentEntry.setNameSurname(nameSurname.getText());
        currentEntry.setRank(rank.getText());
        currentEntry.setCcode(ccode.getText());
        currentEntry.setSecPhone(secPhoneNumber.getText());
        currentEntry.setCompany(company.getText());
        currentEntry.setCcode(ccode.getText());
        currentEntry.setPhone(phoneNumber.getText());
        currentEntry.setSecPhone(secPhoneNumber.getText());
        currentEntry.setnConnect(ncPhone.getText());
        currentEntry.setEmail(email.getText());
        currentEntry.setAddress(homeAddress.getText());
        currentEntry.setComments(userComments.getText());
        currentEntry.setVip(vipCheckBox.isSelected());
        currentEntry.setSkip(smsCheckBox.isSelected());
        currentEntry.setPersonCode(personCode.getText());
        currentEntry.setMarineBookId(sbNum.getText());
        currentEntry.setContractNum(contract.getText());

        Task<Integer> tIns = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                ClientService.updateRecord(currentEntry, "_updateInsertClient");
                setMessage("User profile created");
                return 0;
            }
        };

        setProgress("Creating user profile...");

        try {
            tIns.run();
        } catch (Exception e) {
            setError("Error creating profile");
            dialogs.initErrorDialog(e.getMessage());
        }

        if (!oldPersonCode.equals(personCode.getText())) {
            isPkUnique();

            if (!unique) {
                setWarning("Person code not unique! Please, merge possible duplicates!");
                dialogs.initWarning("Duplicate person code found!");
                //JOptionPane.showMessageDialog(null, "", "Warning", JOptionPane.WARNING_MESSAGE);
            } else {
                updatePersonCode();
            }
        }

    }

    private void isPkUnique() {

        Task<Integer> t = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                List<ClientEntry> cli = new ArrayList<>();

                try {
                    cli = ClientService.fetchList(currentEntry, "idByPersonCode");
                } catch (Exception e) {
                    setError("Error checking person code!");
                    return 0;
                }

                if (cli.size() >= 1) {
                    unique = false;
                    return 0;
                } else {
                    unique = true;
                }

                return 0;
            }
        };

        Pattern lvFormat = Pattern.compile("[0-9]{6}-[0-9]{5}");
        Pattern intFormat = Pattern.compile(".{5}/[0-9]{4}-00000");

        Matcher pCode = lvFormat.matcher(personCode.getText());
        Matcher intCode = intFormat.matcher(personCode.getText());

        if (pCode.matches() && !"000000-00000".equals(personCode.getText()) && !oldPersonCode.equals(personCode.getText())) {
            t.run();
        }

        if (unique) {
            updatePersonCode();
        }

    }

    private void updatePersonCode() {

        currentEntry.setPersonCode(personCode.getText());

        Task<Integer> up = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                ClientService.updateRecord(currentEntry, "_updatePersonCode");
                setMessage("Profile saved and person code updated");
                return 0;
            }
        };

        setProgress("Updating person code");
        try {
            up.run();
        } catch (Exception e) {
            setError("Person code update failed");
        }

    }

    @FXML
    protected void updatePrefLang(ActionEvent ev) {

        Task<Integer> ts = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                HashMap<String, String> langs = new HashMap();
                langs.put("English", "en");
                langs.put("Russian", "ru");
                langs.put("Latvian", "lv");

                String lng = "en";

                if (null != prefLangBox.getValue()) {
                    lng = langs.get(prefLangBox.getValue());
                }

                ClientEntry ce = new ClientEntry();
                ce.setUserId(currentEntry.getUserId());
                ce.setPrefLang(lng);
                ClientService.updateRecord(ce, "_updatePrefLang");
                setMessage("Client language updated");
                pLang.setText("(" + lng + ")");
                return 0;

            }
        };

        setProgress("Saving default language");

        try {
            ts.run();
        } catch (Exception e) {
            setError("Update error");
        }

    }

    @FXML
    protected void openUserList(ActionEvent ev) {
    }

    private void setMessage(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: green");
        progressBar.setProgress(1D);

    }

    private void setError(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: red");
        progressBar.setProgress(1D);
    }

    private void setProgress(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: #0000FF");
        progressBar.setProgress(-1D);
    }

    private void setWarning(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: #F25509");
        progressBar.setProgress(1D);
    }
}
//Library1337
