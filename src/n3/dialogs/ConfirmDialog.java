package n3.dialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import n3.dialogs.shared.DialogState;

public class ConfirmDialog
        implements Initializable {
    
    @FXML
    private Label msgLabel;


    
    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
       msgLabel.setText(DialogState.getInstance().getLastMessage());
    }
    
    @FXML
    protected void closeDialog(ActionEvent ev) {
        DialogState.getInstance().setState(1);
        ((Button)ev.getSource()).getScene().getWindow().hide();
    }
    
    @FXML
    protected void applyAction(ActionEvent ev) {
        DialogState.getInstance().setState(0);
        ((Button)ev.getSource()).getScene().getWindow().hide();
    }

}
