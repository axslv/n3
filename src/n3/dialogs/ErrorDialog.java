package n3.dialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import n3.dialogs.shared.DialogState;

public class ErrorDialog
        implements Initializable {
    
    private boolean mark = false;

    @FXML
    private TextArea errorMsg;
    @FXML
    private VBox errorContainer;

    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
    }

    @FXML
    protected void closeDialog(ActionEvent ev) {
        ((Button) ev.getSource()).getScene().getWindow().hide();
    }

    @FXML
    protected void viewReport(ActionEvent ev) {

        if (mark) {
            return;
        }
        errorMsg.setText(getLastError());
        errorMsg.setPrefHeight(70.00);
        errorContainer.setVisible(true);
        errorContainer.getScene().getWindow().setHeight(errorContainer.getScene().getWindow().getHeight() + 64.00);
        errorContainer.setPrefHeight(74.00);
        errorMsg.setTooltip(new Tooltip("Copy report and send it to program vendor"));
        errorMsg.setVisible(true);
        mark = true;
    }
    
    private String getLastError() {        
        return DialogState.getInstance().getLastError();
    }
}
