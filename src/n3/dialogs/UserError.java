package n3.dialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import n3.dialogs.shared.DialogState;

public class UserError
        implements Initializable {
    
    @FXML
    private Label msgLabel;


    
    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
       msgLabel.setText(DialogState.getInstance().getLastError());
    }
    
    @FXML
    protected void closeDialog(ActionEvent ev) {
        ((Button)ev.getSource()).getScene().getWindow().hide();
    }
    

}
