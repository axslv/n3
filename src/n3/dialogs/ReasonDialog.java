package n3.dialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import lv.nmc.shared.SharedData;
import n3.WinLoader;

public class ReasonDialog
        implements Initializable {

    @FXML
    private TextArea reasonText;
    WinLoader dialogs = new WinLoader();

    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {

    }

    @FXML
    private void closeWindow(ActionEvent ev) {
        ((Button) ev.getSource()).getScene().getWindow().hide();
        SharedData.getInstance().setReasonProvided(Boolean.FALSE);
    }

    @FXML
    private void applyReason(ActionEvent ev) {

        if (null == reasonText.getText()) {
            dialogs.initWarning("Reason can't be empty");
            return;
        }

        if (reasonText.getText().isEmpty()) {
            dialogs.initWarning("Reason can't be empty");
            return;
        }
        SharedData.getInstance().setReason(reasonText.getText());
        SharedData.getInstance().setReasonProvided(Boolean.TRUE);
        ((Button) ev.getSource()).getScene().getWindow().hide();
    }

}
