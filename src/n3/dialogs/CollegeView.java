package n3.dialogs;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import lv.nmc.entities.col.CollegeEntry;
import lv.nmc.rest.CollegeService;
import n3.table.cells.ps.college.ExamCell;
import n3.table.cells.ps.college.NumCell;
import n3.table.cells.ps.college.QuizCell;
import n3.table.cells.ps.college.S0;
import n3.table.cells.ps.college.S1;
import n3.table.cells.ps.college.S2;
import n3.table.cells.ps.college.S3;
import n3.table.cells.ps.college.S4;
import n3.table.cells.ps.college.S5;
import n3.table.cells.ps.college.S6;

public class CollegeView
        implements Initializable {

    @FXML
    private TableView<CollegeEntry> collegeTable;
    @FXML
    private TableColumn<CollegeEntry, String> ccourseNum, ccourseName;
    @FXML
    private TableColumn<CollegeEntry, Integer> cPts, cPts1, cPts2, cPts3, cPts4, cPts5, cPts6, cQuiz, cExam;
    private Tables tbl = new Tables();
    private List<CollegeEntry> selectedRecords = new ArrayList<>();
    private CollegeEntry selectedRecord = new CollegeEntry();

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        tbl.initCollegeTable();
        tbl.fetchCollegeData();
    }

    
    
    private class Tables {
        public void initCollegeTable() {
            collegeTable.setEditable(true);
            collegeTable.getSelectionModel().setCellSelectionEnabled(true);
            ccourseNum.setEditable(true);
            cQuiz.setEditable(true);
            cExam.setEditable(true);
            cPts.setEditable(true);
            cPts1.setEditable(true);
            cPts2.setEditable(true);
            cPts3.setEditable(true);
            cPts4.setEditable(true);
            cPts5.setEditable(true);
            cPts6.setEditable(true);

            ccourseNum.setCellValueFactory(
                    new PropertyValueFactory<CollegeEntry, String>("courseNum"));
            ccourseName.setCellValueFactory(
                    new PropertyValueFactory<CollegeEntry, String>("courseName"));
            cQuiz.setCellValueFactory(
                    new PropertyValueFactory<CollegeEntry, Integer>("quiz"));
            cExam.setCellValueFactory(
                    new PropertyValueFactory<CollegeEntry, Integer>("exam"));

            cPts.setCellValueFactory(
                    new PropertyValueFactory<CollegeEntry, Integer>("s0"));
            cPts1.setCellValueFactory(
                    new PropertyValueFactory<CollegeEntry, Integer>("s1"));
            cPts2.setCellValueFactory(
                    new PropertyValueFactory<CollegeEntry, Integer>("s2"));
            cPts3.setCellValueFactory(
                    new PropertyValueFactory<CollegeEntry, Integer>("s3"));
            cPts4.setCellValueFactory(
                    new PropertyValueFactory<CollegeEntry, Integer>("s4"));
            cPts5.setCellValueFactory(
                    new PropertyValueFactory<CollegeEntry, Integer>("s5"));
            cPts6.setCellValueFactory(
                    new PropertyValueFactory<CollegeEntry, Integer>("s6"));
            
            Callback<TableColumn<CollegeEntry, String>, TableCell<CollegeEntry, String>> cnum
                    = new Callback<TableColumn<CollegeEntry, String>, TableCell<CollegeEntry, String>>() {
                @Override
                public TableCell<CollegeEntry, String> call(TableColumn<CollegeEntry, String> p) {
                    return new NumCell();
                }
            };
            ccourseNum.setCellFactory(cnum);

            Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>> totals
                    = new Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>>() {
                @Override
                public TableCell<CollegeEntry, Integer> call(TableColumn<CollegeEntry, Integer> p) {
                    return new S0();
                }
            };
            cPts.setCellFactory(totals);

            Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>> quiz
                    = new Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>>() {
                @Override
                public TableCell<CollegeEntry, Integer> call(TableColumn<CollegeEntry, Integer> p) {
                    return new QuizCell();
                }
            };
            cQuiz.setCellFactory(quiz);

            Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>> exam
                    = new Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>>() {
                @Override
                public TableCell<CollegeEntry, Integer> call(TableColumn<CollegeEntry, Integer> p) {
                    return new ExamCell();
                }
            };
            cExam.setCellFactory(exam);

            Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>> s1
                    = new Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>>() {
                @Override
                public TableCell<CollegeEntry, Integer> call(TableColumn<CollegeEntry, Integer> p) {
                    return new S1();
                }
            };
            cPts1.setCellFactory(s1);

            Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>> s2
                    = new Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>>() {
                @Override
                public TableCell<CollegeEntry, Integer> call(TableColumn<CollegeEntry, Integer> p) {
                    return new S2();
                }
            };
            cPts2.setCellFactory(s2);

            Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>> s3
                    = new Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>>() {
                @Override
                public TableCell<CollegeEntry, Integer> call(TableColumn<CollegeEntry, Integer> p) {
                    return new S3();
                }
            };
            cPts3.setCellFactory(s3);

            Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>> s4
                    = new Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>>() {
                @Override
                public TableCell<CollegeEntry, Integer> call(TableColumn<CollegeEntry, Integer> p) {
                    return new S4();
                }
            };
            cPts4.setCellFactory(s4);

            Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>> s5
                    = new Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>>() {
                @Override
                public TableCell<CollegeEntry, Integer> call(TableColumn<CollegeEntry, Integer> p) {
                    return new S5();
                }
            };
            cPts5.setCellFactory(s5);

            Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>> s6
                    = new Callback<TableColumn<CollegeEntry, Integer>, TableCell<CollegeEntry, Integer>>() {
                @Override
                public TableCell<CollegeEntry, Integer> call(TableColumn<CollegeEntry, Integer> p) {
                    return new S6();
                }
            };
            cPts6.setCellFactory(s6);
        }
        
        public void fetchCollegeData() {
            selectedRecord.setEvent("testFire");
            selectedRecord.setDomain("col_nav");
            
            try {
                selectedRecords = CollegeService.fetchList(selectedRecord, selectedRecord.getEvent());
                collegeTable.getItems().addAll(selectedRecords);
            } catch (Exception ex) {
                Logger.getLogger(CollegeView.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        
    }
}
