/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.dialogs;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import lv.nmc.entities.ReissueEntry;

import lv.nmc.rest.IssueService;
import n3.WinLoader;
import n3.table.cells.CertIssuedCell;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Reissue {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;
    @FXML // fx:id="certNr"
    private TextField certNr; // Value injected by FXMLLoader
    @FXML // fx:id="courseId"
    private ComboBox<String> courseId; // Value injected by FXMLLoader
    @FXML // fx:id="courseId"
    private ComboBox<String> vCourseId; // Value injected by FXMLLoader
    @FXML // fx:id="dateIssued"
    private TextField dateIssued; // Value injected by FXMLLoader
    @FXML // fx:id="nameSurname"
    private TextField nameSurname; // Value injected by FXMLLoader
    @FXML // fx:id="oldCertNr"
    private TextField oldCertNr; // Value injected by FXMLLoader
    @FXML // fx:id="oldDateIssued"
    private TextField oldDateIssued; // Value injected by FXMLLoader
    @FXML // fx:id="oldIssuer"
    private TextField oldIssuer; // Value injected by FXMLLoader
    @FXML // fx:id="personCode"
    private TextField personCode; // Value injected by FXMLLoader
    @FXML // fx:id="progressBar"
    private ProgressBar progressBar; // Value injected by FXMLLoader
    @FXML // fx:id="progressLabel"
    private Label progressLabel; // Value injected by FXMLLoader
    @FXML // fx:id="progressLabel"
    private Label labelProtoCount; // Value injected by FXMLLoader  
    @FXML // fx:id="seamansBook"
    private TextField seamansBook; // Value injected by FXMLLoader
    @FXML
    private TableView<ReissueEntry> newClients;
    @FXML
    private TableColumn<ReissueEntry, String> tNameSurname;
    @FXML
    private TableColumn<ReissueEntry, String> tSeamansBook;
    @FXML
    private TableColumn<ReissueEntry, String> tReissueDate;
    @FXML
    private TableColumn<ReissueEntry, String> tCertNr;
    @FXML
    private TableColumn<ReissueEntry, String> tOldCertNr;
    @FXML
    private TableColumn<ReissueEntry, String> tOldIssuer;
    @FXML
    private TableColumn<ReissueEntry, String> tOldDateIssued;
    @FXML
    private ComboBox<String> protoNum;
    private ReissueEntry reEntry = new ReissueEntry();
    private HashMap<String, Integer> courseMap = new HashMap<>();
    private HashMap<String, Integer> protoMap = new HashMap<>();
    WinLoader nota = new WinLoader();

    private DateTime currentDate = new DateTime();
    DateTimeFormatter dtf = DateTimeFormat.forPattern("Y-MM-d");

    //DateTimeFormatter yearDigits = DateTimeFormat.forPattern("y");
    @FXML
    protected void writeToday(ActionEvent ev) {
        DateTime dt = new DateTime();
        dateIssued.setText(dt.toString(dtf));

    }

    @FXML
    protected void createProtocol(ActionEvent ev) {

        setProgress("Fetching list...");
        Task<Integer> tt = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                reEntry.setEvent("lastGroup");
                reEntry.setCourseId(courseMap.get(vCourseId.getValue()));

                //reEntry.setGroupName(generateName);
                List<ReissueEntry> lss = new ArrayList<>();
                lss = IssueService.fetchList(reEntry, reEntry.getEvent());

                if (lss.isEmpty()) {
                    reEntry.setGroupName(generateGroup(0));
                } else {
                    String[] gn = lss.get(0).getGroupName().split("-");
                    String number = gn[2].split("\\/")[0];
                    reEntry.setGroupName(generateGroup(Integer.parseInt(number)));
                    //  nota.initUserError(reEntry.getGroupName() + " " + number);
                    // return 1;
                }

                ObservableList<ReissueEntry> items = newClients.getItems();
                Iterator iter = items.iterator();

                while (iter.hasNext()) {
                    ReissueEntry re = (ReissueEntry) iter.next();

                    if (!courseMap.get(vCourseId.getValue()).equals(re.getCourseId())) {
                        continue;
                    }

                    reEntry.setEvent("_updateUnOrphan");
                    reEntry.setNameSurname(re.getNameSurname());
                    reEntry.setSeamansBook(re.getSeamansBook());
                    //  reEntry.setCourseId(re.getCourseId());

                    try {
                        IssueService.updateRecord(reEntry, reEntry.getEvent());
                        //return 0;
                    } catch (Exception e) {
                        nota.initErrorDialog(e.getMessage());
                        setError("Action failed");
                        return 1;
                    }
                }
                setMessage("Protocol created");
                return 0;
            }
        };

        tt.run();
    }

    private String generateGroup(Integer nn) {
        nn = nn + 1;
        String prefix = "DMP-" + vCourseId.getValue() + "-" + String.format("%03d", nn) + "/17";
        return prefix;
        //  "lalalala".concat("kkkk");
    }

    private void initTable() {
        newClients.setEditable(true);
        newClients.getSelectionModel().setCellSelectionEnabled(true);
        tReissueDate.setEditable(true);

        tNameSurname.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("nameSurname"));
        tSeamansBook.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("seamansBook"));
        tReissueDate.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("dateIssued"));
        tCertNr.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("certNr"));
        tOldCertNr.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("oldCertNr"));
        tOldIssuer.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("oldIssuer"));
        tOldDateIssued.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("oldDateIssued"));

        Callback<TableColumn<ReissueEntry, String>, TableCell<ReissueEntry, String>> instructorFactory
                = new Callback<TableColumn<ReissueEntry, String>, TableCell<ReissueEntry, String>>() {
                    @Override
                    public TableCell<ReissueEntry, String> call(TableColumn<ReissueEntry, String> p) {
                        return new CertIssuedCell();
                    }
                };
        tReissueDate.setCellFactory(instructorFactory);

    }

    @FXML
    protected void openOrphans(ActionEvent ev) {
        try {
            java.awt.Desktop.getDesktop().browse(new URI("https://apps.novikontas.lv/nReg/helper.orb?_eventName=showOrphans"));
        } catch (IOException ex) {
            Logger.getLogger(Reissue.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(Reissue.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void fillTable() {
        reEntry.setEvent("newUsers");
        reEntry.setCourseId(courseMap.get(vCourseId.getValue()));

        setProgress("Fetching list...");
        Task<Integer> tt = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                try {
                    List<ReissueEntry> ls = new ArrayList<>();
                    ls = IssueService.fetchList(reEntry, reEntry.getEvent());
                    newClients.getItems().clear();
                    newClients.getItems().addAll(ls);

                    setMessage("List ready");
                    return 0;
                } catch (Exception e) {
                    nota.initErrorDialog(e.getMessage());
                    setError("Action failed");
                    return 1;
                }
            }
        };

        tt.run();
    }

    @FXML
    protected void generateCert(ActionEvent ev) {
        generateTmpCert();
        certNr.setText(tmpCert);
    }
    private String tmpCert = "";

    private void generateTmpCert() {
        if (null == courseId.getValue()) {
            nota.initUserError("Please, select course");
            return;
        }

        reEntry.setEvent("selectLastCert");
        reEntry.setCourseId(courseMap.get(courseId.getValue()));

        setProgress("Generating cert");
        Task<Integer> tt = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                try {
                    List<ReissueEntry> ls = new ArrayList<>();
                    ls = IssueService.fetchList(reEntry, reEntry.getEvent());

                    if (ls.isEmpty()) {
                        nota.initUserError("No certificates for this course");
                        return 0;
                    }

                    ReissueEntry re = ls.get(0);
                    String[] cname = re.getCertNr().split("-");

                    String[] cnum = cname[1].split("\\/");
                    Integer nn = new Integer(cnum[0]) + 1;

                    tmpCert = cname[0] + "-" + String.format("%03d", nn) + "/" + cnum[1];

                    setMessage("Cert generated");
                    return 0;
                } catch (Exception e) {

                    nota.initErrorDialog(e.getMessage());
                    setError("Action failed");
                    return 1;
                }
            }
        };
        tt.run();
    }

    private boolean inserted = false;

    private void insertCert() {

        if (tmpCert.length() < 2) {
            nota.initUserError("Empty certificate number!");

        }

        try {
            IssueService.updateRecord(reEntry, reEntry.getEvent());
        } catch (Exception e) {

            nota.initErrorDialog(e.getMessage());
            setError("Action failed");

        }

    }

    @FXML
    protected void insertRecord(ActionEvent event) {

        List<String> ch = new ArrayList<>();
        ch.add(nameSurname.getText());
        ch.add(personCode.getText());
        ch.add(seamansBook.getText());
        ch.add(oldCertNr.getText());
        ch.add(oldDateIssued.getText());
        ch.add(oldIssuer.getText());
        ch.add(certNr.getText());
        ch.add(dateIssued.getText());

        for (String ss : ch) {
            if (ss.isEmpty()) {
                nota.initUserError("Please, recheck fields");
                return;
            }
        }

        setProgress("Saving record...");
        Task<Integer> tt = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                try {
                    List<ReissueEntry> test = new ArrayList<>();
                    ReissueEntry re = new ReissueEntry();
                    re.setCertNr(certNr.getText());
                    re.setEvent("certByName");
                    test = IssueService.fetchList(re, re.getEvent());

                    if (test.isEmpty()) {

                        reEntry.setEvent("_updateAddCertificate");
                        reEntry.setCourseId(courseMap.get(courseId.getValue()));
                        reEntry.setCertNr(certNr.getText());
                        IssueService.updateRecord(reEntry, reEntry.getEvent());
                        reEntry.setNameSurname(nameSurname.getText());
                        reEntry.setPersonCode(personCode.getText());
                        reEntry.setSeamansBook(seamansBook.getText());
                        reEntry.setOldCertNr(oldCertNr.getText());
                        reEntry.setOldDateIssued(oldDateIssued.getText());
                        reEntry.setOldIssuer(oldIssuer.getText());
                        reEntry.setCertNr(certNr.getText());
                        reEntry.setDateIssued(dateIssued.getText());
                        reEntry.setCourseId(courseMap.get(courseId.getValue()));
                        reEntry.setEvent("_updateAddReissueRecord");
                        IssueService.updateRecord(reEntry, reEntry.getEvent());
                        setMessage("Record inserted");
                        return 0;

                    } else {
                        System.out.println(test.get(0).getOldCertNr());
                        nota.initUserError("Error. Certificate already exists. Please, generate new one.");
                    }
                    return 0;

                } catch (Exception e) {
                    nota.initErrorDialog(e.getMessage());
                    setError("Action failed");
                    return 1;
                }
            }
        };

        tt.run();
        certNr.clear();
    }

    @FXML
    protected void clearFields(ActionEvent ev) {
        nameSurname.clear();
        personCode.clear();
        seamansBook.clear();
        oldCertNr.clear();
        oldDateIssued.clear();
        oldIssuer.clear();
        certNr.clear();
        dateIssued.clear();
    }
//update combobox

    @FXML
    protected void openProto(ActionEvent ev) {
        try {
            java.awt.Desktop.getDesktop().browse(new URI(ResourceBundle.getBundle("resources.urls").getString("ProtocolsAddress") + "?groupName=" + protoNum.getValue()));
        } catch (IOException ex) {
            Logger.getLogger(Reissue.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(Reissue.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    protected void courseSelected(ActionEvent event) {
        setProgress("Fetching lists...");
        protoNum.getItems().clear();

        Task<Integer> tt = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                try {
                    reEntry.setCourseId(courseMap.get(vCourseId.getValue()));
                    reEntry.setEvent("recordCount");
                    List<ReissueEntry> ls = new ArrayList<>();
                    ls = IssueService.fetchList(reEntry, reEntry.getEvent());
                    List<ReissueEntry> gdl = new ArrayList<>();
                    reEntry.setEvent("distinctGroups");
                    gdl = IssueService.fetchList(reEntry, reEntry.getEvent());

                    if (!gdl.isEmpty()) {
                        for (ReissueEntry re : gdl) {
                            protoNum.getItems().add(re.getGroupName());
                        }
                    }

                    Double ll = Math.ceil(ls.size() / 10);
                    Integer ff = new Integer(ll.intValue());
                    labelProtoCount.setText("Protocol count: " + " " + ff.toString());

                    setMessage("List ready");
                    return 0;
                } catch (Exception e) {
                    nota.initErrorDialog(e.getMessage());
                    setError("Action failed");
                    return 1;
                }
            }
        };

        tt.run();
        fillTable();
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    protected void initialize() {

        //String email = ResourceBundle.getBundle("resources.settings").getString("contactAddress");
        List<ReissueEntry> ls = new ArrayList<>();
        List<String> css = new ArrayList<>();
        ReissueEntry re = new ReissueEntry();
        re.setEvent("selectCourses");

        try {
            ls = IssueService.fetchList(re, re.getEvent());
        } catch (Exception ex) {
            nota.initErrorDialog(ex.getMessage());
        }

        for (ReissueEntry ree : ls) {
            courseMap.put(ree.getGroupName(), ree.getCourseId());
            css.add(ree.getGroupName());
        }

        courseId.getItems().clear();
        vCourseId.getItems().clear();

        courseId.getItems().addAll(css);
        vCourseId.getItems().addAll(css);
        initTable();
        //  protoDate.setText(new DateTime().toString(dtf));
        setMessage("Program ready");
    }

    private void setMessage(String message) {
        progressLabel.setStyle("-fx-text-fill: green;");
        progressLabel.setText(message);
        progressBar.setProgress(1D);
    }

    private void setProgress(String message) {
        progressLabel.setStyle("-fx-text-fill: blue;");
        progressLabel.setText(message);
        progressBar.setProgress(-1.0);
    }

    private void setError(String message) {
        progressLabel.setStyle("-fx-text-fill: red;");
        progressLabel.setText(message);
        progressBar.setProgress(1D);
    }
}
