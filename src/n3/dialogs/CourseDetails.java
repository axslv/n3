package n3.dialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import n3.WinLoader;

public class CourseDetails implements Initializable {

    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private ComboBox<?> department;
    @FXML
    private ComboBox<?> domain;
    @FXML
    private TextField fAddress;
    @FXML
    private TextField fAuditor;
    @FXML
    private TextArea fCertHeaderEn;
    @FXML
    private TextArea fCertHeaderLv;
    @FXML
    private TextField fCertificatePrefix;
    @FXML
    private TextField fCertificateValidity;
    @FXML
    private TextArea fCourseDescription;
    @FXML
    private TextArea fCourseDescriptionEn;
    @FXML
    private TextArea fCourseDescriptionLv;
    @FXML
    private TextField fCourseName;
    @FXML
    private TextField fGroupAbbr;
    @FXML
    private TextField fLength;
    @FXML
    private TextField fLengthH;
    @FXML
    private TextField fPrice1;
    @FXML
    private TextField fPrice2;
    @FXML
    private TextField fPrice3;
    @FXML
    private TextField fProtocolNumber;
    @FXML
    private TextField fRoom;
    @FXML
    private TextArea fStcwCodes;
    @FXML
    private TextField fVatRate;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label statusLabel;
    @FXML
    private Button saveButton;
    private WinLoader dialogs = new WinLoader();
    @FXML private Button closeButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ImageView ok = new ImageView();
        ImageView cancel = new ImageView();
        saveButton.setGraphic(ok);
        closeButton.setGraphic(cancel);
        ok.setImage(new Image(getClass().getResourceAsStream("images/ok.png")));
        cancel.setImage(new Image(getClass().getResourceAsStream("images/cancel.png")));   
    }

    @FXML
    private void saveRecord(ActionEvent event) {
        
    }
    
    @FXML
    private void exit(ActionEvent ev) {
        ((Button) ev.getSource()).getScene().getWindow().hide();
    }

    private void setMessage(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: green");
        progressBar.setProgress(1D);

    }

    private void setError(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: red");
        progressBar.setProgress(1D);
    }

    private void setProgress(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: #0000FF");
        progressBar.setProgress(-1D);
    }

    private void setWarning(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: #F25509");
        progressBar.setProgress(1D);
    }
}
//Library1337