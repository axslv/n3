package n3.dialogs;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import lv.nmc.entities.ClientEntry;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.entities.ReissueEntry;
import lv.nmc.rest.ClientService;
import lv.nmc.rest.IssueService;
import lv.nmc.rest.RegistrationService;
import lv.nmc.shared.Documents;
import lv.nmc.shared.LastClient;
import n3.GroupPage;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;
import n3.table.cells.AutoCompleteComboBoxListener;
import n3.table.cells.CertIssuedCell;
import n3.table.cells.documents.DocName;
import n3.table.cells.documents.DocNum;
import n3.table.cells.documents.DocType;
import n3.table.cells.documents.IssueDate;
import n3.table.cells.documents.Issuer;
import n3.table.cells.documents.ValidTill;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class UserDetails
        implements Initializable {

    @FXML //  fx:id="company"  
    private Label pLang; // Value injected by FXMLLoader
    @FXML //  fx:id="company"  
    private TextField company, contractNum; // Value injected by FXMLLoader
    @FXML //  fx:id="email"
    private TextField email; // Value injected by FXMLLoader
    @FXML //  fx:id="homeAddress"
    private TextField homeAddress; // Value injected by FXMLLoader
    @FXML //  fx:id="nameSurname"
    private TextField nameSurname; // Value injected by FXMLLoader
    @FXML //  fx:id="ncPhone"
    private TextField ncPhone; // Value injected by FXMLLoader
    @FXML //  fx:id="personCode"
    private TextField personCode; // Value injected by FXMLLoader
    @FXML //  fx:id="phoneNumber"
    private TextField phoneNumber; // Value injected by FXMLLoader
    @FXML //  fx:id="rank"
    private TextField rank, ccode, secPhoneNumber; // Value injected by FXMLLoader
    @FXML //  fx:id="userComments"
    private TextArea userComments; // Value injected by FXMLLoader

    @FXML //  fx:id="vipCheckBox"
    private CheckBox vipCheckBox; // Value injected by FXMLLoader
    @FXML //  fx:id="vipCheckBox"
    private CheckBox smsCheckBox; // Value injected by FXMLLoader   
    @FXML //  fx:id="vipCheckBox"
    private ComboBox domainBox; // Value injected by FXMLLoader
    @FXML //  fx:id="vipCheckBox"
    private ComboBox<String> prefLangBox; // Value injected by FXMLLoader    
    @FXML //  fx:id="vipCheckBox"
    private Button saveButton; // Value injected by FXMLLoader   
    @FXML //  fx:id="vipCheckBox"
    private Button cancelButton; // Value injected by FXMLLoader  
    @FXML //  fx:id="vipCheckBox"
    private Label statusLabel; // Value injected by FXMLLoader    
    @FXML //  fx:id="vipCheckBox"
    private ProgressBar progressBar; // Value injected by FXMLLoader  
    @FXML
    private ComboBox<String> dc;
    @FXML
    private TextField sbNum;
    private ClientEntry currentEntry = LastClient.getInstance().getLastClient();
    private String oldPersonCode = "";
    private boolean unique = true;
    private WinLoader dialogs = new WinLoader();

    @FXML
    private TableView<RegistrationEntry> docsTable, docsTypeTable;
    @FXML
    private TableColumn<RegistrationEntry, String> docName, docNum, issuedBy, issueDate, validTill, docType;
    @FXML
    private ComboBox adocumentsList;
    @FXML
    private TextField adocNum, aissuedBy, aissueDate, avalidTill, tdocumentType;
    @FXML
    private TabPane usrPane;
    @FXML
    private Tab docTab;

    @FXML
    private TableView<ReissueEntry> newClients;
    @FXML
    private TableColumn<ReissueEntry, String> rtNameSurname;
    @FXML
    private TableColumn<ReissueEntry, String> rtSeamansBook;
    @FXML
    private TableColumn<ReissueEntry, String> rtReissueDate;
    @FXML
    private TableColumn<ReissueEntry, String> rtCertNr;
    @FXML
    private TableColumn<ReissueEntry, String> rtOldCertNr;
    @FXML
    private TableColumn<ReissueEntry, String> rtOldIssuer;
    @FXML
    private TableColumn<ReissueEntry, String> rtOldDateIssued;
    @FXML
    private ComboBox<String> rProtoNum;

    @FXML // fx:id="certNr"
    private TextField rCertNr; // Value injected by FXMLLoader
    @FXML // fx:id="courseId"
    private ComboBox<String> rCourseId; // Value injected by FXMLLoader
    @FXML // fx:id="courseId"
    private ComboBox<String> rVCourseId; // Value injected by FXMLLoader
    @FXML // fx:id="dateIssued"
    private TextField rDateIssued; // Value injected by FXMLLoader
    @FXML // fx:id="nameSurname"
    private TextField rNameSurname; // Value injected by FXMLLoader
    @FXML // fx:id="oldCertNr"
    private TextField rOldCertNr; // Value injected by FXMLLoader
    @FXML // fx:id="oldDateIssued"
    private TextField rOldDateIssued; // Value injected by FXMLLoader
    @FXML // fx:id="oldIssuer"
    private TextField rOldIssuer; // Value injected by FXMLLoader
    @FXML // fx:id="personCode"
    private TextField rPersonCode; // Value injected by FXMLLoader
    @FXML // fx:id="progressLabel"
    private Label labelProtoCount; // Value injected by FXMLLoader  
    @FXML // fx:id="seamansBook"
    private TextField rSeamansBook; // Value injected by FXMLLoader

    private ReissueEntry reEntry = new ReissueEntry();
    private HashMap<String, Integer> courseMap = new HashMap<>();
    private HashMap<String, Integer> protoMap = new HashMap<>();
    WinLoader nota = new WinLoader();

    private DateTime currentDate = new DateTime();
    DateTimeFormatter dtf = DateTimeFormat.forPattern("Y-MM-d");

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {

        ImageView ok = new ImageView();
        ImageView cancel = new ImageView();

        ok.setImage(new Image(getClass().getResourceAsStream("images/ok.png")));
        cancel.setImage(new Image(getClass().getResourceAsStream("images/cancel.png")));

        saveButton.setGraphic(ok);
        cancelButton.setGraphic(cancel);

        List<ClientEntry> cls = new ArrayList<>();

        try {
            currentEntry.setEvent("clientDetails");
            cls = ClientService.fetchList(currentEntry, currentEntry.getEvent());
            //System.out.println(currentEntry.getUserId());

            if (cls.isEmpty()) {
                dialogs.initUserError("User not found");
                return;

            }

            ClientEntry ge = cls.get(0);
            currentEntry = ge;
            nameSurname.setText(ge.getNameSurname());
            rNameSurname.setText(ge.getNameSurname());
            personCode.setText(ge.getPersonCode());
            rPersonCode.setText(ge.getPersonCode());
            oldPersonCode = ge.getPersonCode();
            if (null != ge.getRank()) {
                rank.setText(ge.getRank());
            }
            if (null != ge.getCompany()) {
                company.setText(ge.getCompany());
            }

            if (null != ge.getCcode()) {
                ccode.setText(ge.getCcode());
            }

            if (null != ge.getSecPhone()) {
                secPhoneNumber.setText(ge.getSecPhone());
            }

            if (null != ge.getPhone()) {
                phoneNumber.setText(ge.getPhone());
            }
            if (null != ge.getnConnect()) {
                ncPhone.setText(ge.getnConnect());
            }

            if (null != ge.getEmail()) {
                email.setText(ge.getEmail());
            }
            if (null != ge.getAddress()) {
                homeAddress.setText(ge.getAddress());
            }
            if (null != ge.getComments()) {
                userComments.setText(ge.getComments());
            }
            if (null != ge.getMarineBookId()) {
                sbNum.setText(ge.getMarineBookId());
                rSeamansBook.setText(ge.getMarineBookId());
            }

            vipCheckBox.setSelected(ge.isVip());
            smsCheckBox.setSelected(ge.isSkip());
            pLang.setText("(" + ge.getPrefLang() + ")");
            contractNum.setText(ge.getContractNum());

        } catch (NullPointerException ex) {
            Logger.getLogger(UserDetails.class.getName()).log(Level.SEVERE, null, ex);
            statusLabel.setText("Warning! Some values not displayed!");
            statusLabel.setStyle("-fx-text-fill: #FF8000");
        } catch (Exception e) {
            statusLabel.setText("Server error");
            statusLabel.setStyle("-fx-text-fill: red");
            dialogs.initErrorDialog(e.getMessage());
        }

        initDocumentsTable();
        updateDocumentTypes();

        usrPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {

            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldTab, Tab newTab) {
                if (newTab == docTab) {
                    updateDocumentTypes();
                }
            }
        });

        // ------- reissue stuff
        List<ReissueEntry> ls = new ArrayList<>();
        List<String> css = new ArrayList<>();
        ReissueEntry re = new ReissueEntry();
        re.setEvent("selectCourses");

        try {
            ls = IssueService.fetchList(re, re.getEvent());
        } catch (Exception ex) {
            nota.initErrorDialog(ex.getMessage());
        }

        for (ReissueEntry ree : ls) {
            courseMap.put(ree.getGroupName(), ree.getCourseId());
            css.add(ree.getGroupName());
        }

        rVCourseId.getItems().clear();
        rVCourseId.getItems().clear();

        rCourseId.getItems().addAll(css);
        rVCourseId.getItems().addAll(css);
        initTable();

    }

   //init reissue table
    private void initTable() {
        newClients.setEditable(true);
        newClients.getSelectionModel().setCellSelectionEnabled(true);
        rtReissueDate.setEditable(true);

        rtNameSurname.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("nameSurname"));
        rtSeamansBook.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("seamansBook"));
        rtReissueDate.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("dateIssued"));
        rtCertNr.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("certNr"));
        rtOldCertNr.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("oldCertNr"));
        rtOldIssuer.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("oldIssuer"));
        rtOldDateIssued.setCellValueFactory(
                new PropertyValueFactory<ReissueEntry, String>("oldDateIssued"));

        Callback<TableColumn<ReissueEntry, String>, TableCell<ReissueEntry, String>> instructorFactory
                = new Callback<TableColumn<ReissueEntry, String>, TableCell<ReissueEntry, String>>() {
            @Override
            public TableCell<ReissueEntry, String> call(TableColumn<ReissueEntry, String> p) {
                return new CertIssuedCell();
            }
        };
        rtReissueDate.setCellFactory(instructorFactory);

    }

    //fill reissue table
    private void fillTable() {
        reEntry.setEvent("newUsers");
        reEntry.setCourseId(courseMap.get(rVCourseId.getValue()));
        setProgress("Fetching list...");
        Task<Integer> tt = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                try {
                    List<ReissueEntry> ls = new ArrayList<>();
                    ls = IssueService.fetchList(reEntry, reEntry.getEvent());
                    newClients.getItems().clear();
                    newClients.getItems().addAll(ls);                    
                    setMessage("List ready");
                    return 0;
                } catch (Exception e) {
                    nota.initErrorDialog(e.getMessage());
                    setError("Action failed");
                    return 1;
                }
            }
        };

        tt.run();
    }

    //generate reissue group (DMP-....)
    private String generateGroup(Integer nn) {
        nn = nn + 1;
        String prefix = "DMP-" + rVCourseId.getValue() + "-" + String.format("%03d", nn) + "/17";
        return prefix;
    }

    @FXML
    protected void createProtocol(ActionEvent ev) {

        setProgress("Fetching list...");
        Task<Integer> tt = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                reEntry.setEvent("lastGroup");
                reEntry.setCourseId(courseMap.get(rVCourseId.getValue()));

                //reEntry.setGroupName(generateName);
                List<ReissueEntry> lss = new ArrayList<>();
                lss = IssueService.fetchList(reEntry, reEntry.getEvent());

                if (lss.isEmpty()) {
                    reEntry.setGroupName(generateGroup(0));
                } else {
                    String[] gn = lss.get(0).getGroupName().split("-");
                    String number = gn[2].split("\\/")[0];
                    reEntry.setGroupName(generateGroup(Integer.parseInt(number)));
                    //  nota.initUserError(reEntry.getGroupName() + " " + number);
                    // return 1;
                }

                ObservableList<ReissueEntry> items = newClients.getItems();
                Iterator iter = items.iterator();

                while (iter.hasNext()) {
                    ReissueEntry re = (ReissueEntry) iter.next();

                    if (!courseMap.get(rVCourseId.getValue()).equals(re.getCourseId())) {
                        continue;
                    }

                    reEntry.setEvent("_updateUnOrphan");
                    reEntry.setNameSurname(re.getNameSurname());
                    reEntry.setSeamansBook(re.getSeamansBook());
                    //  reEntry.setCourseId(re.getCourseId());

                    try {
                        IssueService.updateRecord(reEntry, reEntry.getEvent());
                        //return 0;
                    } catch (Exception e) {
                        nota.initErrorDialog(e.getMessage());
                        setError("Action failed");
                        return 1;
                    }
                }
                setMessage("Protocol created");
                return 0;
            }
        };

        tt.run();
    }

    @FXML
    protected void openOrphans(ActionEvent ev) {
        try {
            java.awt.Desktop.getDesktop().browse(new URI("https://apps.novikontas.lv/nReg/helper.orb?_eventName=showOrphans"));
        } catch (IOException ex) {
            Logger.getLogger(Reissue.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(Reissue.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    protected void writeToday(ActionEvent ev) {
        DateTime dt = new DateTime();
        rDateIssued.setText(dt.toString(dtf));
    }

    @FXML
    protected void generateCert(ActionEvent ev) {
        generateTmpCert();
        rCertNr.setText(tmpCert);
    }
    private String tmpCert = "";

    private void generateTmpCert() {
        if (null == rCourseId.getValue()) {
            nota.initUserError("Please, select course");
            return;
        }

        reEntry.setEvent("selectLastCert");
        reEntry.setCourseId(courseMap.get(rCourseId.getValue()));

        setProgress("Generating cert");
        Task<Integer> tt = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                try {
                    List<ReissueEntry> ls = new ArrayList<>();
                    ls = IssueService.fetchList(reEntry, reEntry.getEvent());

                    if (ls.isEmpty()) {
                        nota.initUserError("No certificates for this course");
                        return 0;
                    }

                    ReissueEntry re = ls.get(0);
                    String[] cname = re.getCertNr().split("-");

                    String[] cnum = cname[1].split("\\/");
                    Integer nn = new Integer(cnum[0]) + 1;

                    tmpCert = cname[0] + "-" + String.format("%03d", nn) + "/" + cnum[1];

                    setMessage("Cert generated");
                    return 0;
                } catch (Exception e) {

                    nota.initErrorDialog(e.getMessage());
                    setError("Action failed");
                    return 1;
                }
            }
        };
        tt.run();
    }
    private boolean inserted = false;

    private void insertCert() {

        if (tmpCert.length() < 2) {
            nota.initUserError("Empty certificate number!");

        }

        try {
            IssueService.updateRecord(reEntry, reEntry.getEvent());
        } catch (Exception e) {

            nota.initErrorDialog(e.getMessage());
            setError("Action failed");

        }

    }

    @FXML
    protected void insertRecord(ActionEvent event) {

        List<String> ch = new ArrayList<>();
        ch.add(nameSurname.getText());
        ch.add(personCode.getText());
        ch.add(rSeamansBook.getText());
        ch.add(rOldCertNr.getText());
        ch.add(rOldDateIssued.getText());
        ch.add(rOldIssuer.getText());
        ch.add(rCertNr.getText());
        ch.add(rDateIssued.getText());

        for (String ss : ch) {
            if (ss.isEmpty()) {
                nota.initUserError("Please, recheck fields");
                return;
            }
        }

        setProgress("Saving record...");
        Task<Integer> tt = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                try {
                    List<ReissueEntry> test = new ArrayList<>();
                    ReissueEntry re = new ReissueEntry();
                    re.setCertNr(rCertNr.getText());
                    re.setEvent("certByName");
                    test = IssueService.fetchList(re, re.getEvent());

                    if (test.isEmpty()) {

                        reEntry.setEvent("_updateAddCertificate");
                        reEntry.setCourseId(courseMap.get(rCourseId.getValue()));
                        reEntry.setCertNr(rCertNr.getText());
                        IssueService.updateRecord(reEntry, reEntry.getEvent());
                        reEntry.setNameSurname(nameSurname.getText());
                        reEntry.setPersonCode(personCode.getText());
                        reEntry.setSeamansBook(rSeamansBook.getText());
                        reEntry.setOldCertNr(rOldCertNr.getText());
                        reEntry.setOldDateIssued(rOldDateIssued.getText());
                        reEntry.setOldIssuer(rOldIssuer.getText());
                        reEntry.setCertNr(rCertNr.getText());
                        reEntry.setDateIssued(rDateIssued.getText());
                        reEntry.setCourseId(courseMap.get(rCourseId.getValue()));
                        reEntry.setEvent("_updateAddReissueRecord");
                        IssueService.updateRecord(reEntry, reEntry.getEvent());
                        setMessage("Record inserted");
                        return 0;

                    } else {
                        System.out.println(test.get(0).getOldCertNr());
                        nota.initUserError("Error. Certificate already exists. Please, generate new one.");
                    }
                    return 0;

                } catch (Exception e) {
                    nota.initErrorDialog(e.getMessage());
                    setError("Action failed");
                    return 1;
                }
            }
        };

        tt.run();
        rCertNr.clear();
    }

    @FXML
    protected void clearFields(ActionEvent ev) {
        // nameSurname.clear();
        // personCode.clear();
        //seamansBook.clear();
        rOldCertNr.clear();
        rOldDateIssued.clear();
        rOldIssuer.clear();
        rCertNr.clear();
        rDateIssued.clear();
    }

    @FXML
    protected void openProto(ActionEvent ev) {
        try {
            java.awt.Desktop.getDesktop().browse(new URI(ResourceBundle.getBundle("resources.urls").getString("ProtocolsAddress") + "?groupName=" + rProtoNum.getValue()));
        } catch (IOException ex) {
            Logger.getLogger(Reissue.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(Reissue.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    protected void courseSelected(ActionEvent event) {
        setProgress("Fetching lists...");
        rProtoNum.getItems().clear();

        Task<Integer> tt = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                try {
                    reEntry.setCourseId(courseMap.get(rVCourseId.getValue()));
                    reEntry.setEvent("recordCount");
                    List<ReissueEntry> ls = new ArrayList<>();
                    ls = IssueService.fetchList(reEntry, reEntry.getEvent());
                    List<ReissueEntry> gdl = new ArrayList<>();
                    reEntry.setEvent("distinctGroups");
                    gdl = IssueService.fetchList(reEntry, reEntry.getEvent());

                    if (!gdl.isEmpty()) {
                        for (ReissueEntry re : gdl) {
                            rProtoNum.getItems().add(re.getGroupName());
                        }
                    }

                    Double ll = Math.ceil(ls.size() / 10);
                    Integer ff = new Integer(ll.intValue());
                    labelProtoCount.setText("Protocol count: " + " " + ff.toString());

                    setMessage("List ready");
                    return 0;
                } catch (Exception e) {
                    nota.initErrorDialog(e.getMessage());
                    setError("Action failed");
                    return 1;
                }
            }
        };

        tt.run();
        fillTable();
    }

    private void updateDocumentTypes() {
        document.setEvent("selectAllDocuments");

        try {
            Documents.getInstance().setDocumentsList(RegistrationService.fetchList(document, document.getEvent()));
            adocumentsList.getItems().clear();
            docsTypeTable.getItems().clear();

            for (RegistrationEntry re : Documents.getInstance().getDocumentsList()) {
                adocumentsList.getItems().add(re.getDocumentName());
                docsTypeTable.getItems().add(re);
            }

        } catch (Exception ex) {
            Logger.getLogger(UserDetails.class.getName()).log(Level.SEVERE, null, ex);
        }

        adocumentsList.setEditable(true);
        AutoCompleteComboBoxListener lst = new AutoCompleteComboBoxListener(adocumentsList);
    }

    private List<RegistrationEntry> selectedDocuments = new ArrayList<>();
    private List<RegistrationEntry> newDocuments = new ArrayList<>();
    RegistrationEntry document = new RegistrationEntry();

    private void initDocumentsTable() {
        docsTable.getSelectionModel().setCellSelectionEnabled(true);
        docsTypeTable.getSelectionModel().setCellSelectionEnabled(true);
        docsTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        docsTypeTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        docsTable.setEditable(true);
        docsTypeTable.setEditable(true);

        docName.setEditable(false);
        docNum.setEditable(true);
        issuedBy.setEditable(true);
        issueDate.setEditable(true);
        validTill.setEditable(true);

        docType.setEditable(true);

        docName.setCellValueFactory(new PropertyValueFactory<RegistrationEntry, String>("documentName"));
        docNum.setCellValueFactory(new PropertyValueFactory<RegistrationEntry, String>("documentNr"));
        issuedBy.setCellValueFactory(new PropertyValueFactory<RegistrationEntry, String>("issuedBy"));
        issueDate.setCellValueFactory(new PropertyValueFactory<RegistrationEntry, String>("issueDate"));
        validTill.setCellValueFactory(new PropertyValueFactory<RegistrationEntry, String>("validTill"));

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> docNameFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn<RegistrationEntry, String> p) {
                return new DocName();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> docNumFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn<RegistrationEntry, String> p) {
                return new DocNum();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> issuerFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn<RegistrationEntry, String> p) {
                return new Issuer();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> issueDateFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn<RegistrationEntry, String> p) {
                return new IssueDate();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> validTillFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn<RegistrationEntry, String> p) {
                return new ValidTill();
            }
        };

        Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>> docTypeFactory
                = new Callback<TableColumn<RegistrationEntry, String>, TableCell<RegistrationEntry, String>>() {
            @Override
            public TableCell<RegistrationEntry, String> call(TableColumn<RegistrationEntry, String> p) {
                return new DocType();
            }
        };

        docName.setCellFactory(docNameFactory);
        docNum.setCellFactory(docNumFactory);
        issuedBy.setCellFactory(issuerFactory);
        issueDate.setCellFactory(issueDateFactory);
        validTill.setCellFactory(validTillFactory);
        fetchDocuments();

        docType.setCellValueFactory(new PropertyValueFactory<RegistrationEntry, String>("documentName"));
        docType.setCellFactory(docTypeFactory);
        updateDocumentTypes();

    }

    private void fetchDocuments() {
        document.setEvent("selectUserDocuments");
        document.setUserId(currentEntry.getUserId());
        try {
            selectedDocuments = RegistrationService.fetchList(document, document.getEvent());
            docsTable.getItems().addAll(selectedDocuments);
        } catch (Exception ex) {
            Logger.getLogger(UserDetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void addDocument(ActionEvent ev) {
        RegistrationEntry re = new RegistrationEntry();
        re.setEvent("_insertDocumentRecord");
        re.setDocumentName(adocumentsList.getValue().toString());
        re.setDocumentNr(adocNum.getText());
        re.setIssuedBy(aissuedBy.getText());
        re.setIssueDate(aissueDate.getText());
        re.setValidTill(avalidTill.getText());
        re.setUserId(currentEntry.getUserId());

        try {
            RegistrationService.updateRecord(re, re.getEvent());
            docsTable.getItems().clear();
            fetchDocuments();
        } catch (Exception ex) {
            Logger.getLogger(UserDetails.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void deleteDocument(ActionEvent ev) {
        try {
            document = docsTable.getSelectionModel().getSelectedItem();
        } catch (NullPointerException ee) {
            dialogs.initMessage("Please, select record to delete");
            return;
        }

        document.setEvent("_updateDeleteDocument");

        try {
            RegistrationService.updateRecord(document, document.getEvent());
        } catch (Exception ee) {
            dialogs.initMessage("Error deleting record");
        }
        docsTable.getItems().clear();
        fetchDocuments();

    }

    @FXML
    private void addDocumentType(ActionEvent ev) {

        document.setEvent("_insertDocumentType");
        document.setDocumentName(tdocumentType.getText());

        try {
            RegistrationService.updateRecord(document, document.getEvent());
            updateDocumentTypes();
        } catch (Exception ee) {
            dialogs.initMessage("Error on document insert");
        }

    }

    @FXML
    private void deleteDocumentType(ActionEvent ev) {

        dialogs.initConfirmDialog("ALL DOCUMENTS CONNECTED WITH THIS RECORD WILL BE LOST. SURE?");

        if (DialogState.getInstance().getState() == 0) {

            try {
                document = docsTypeTable.getSelectionModel().getSelectedItem();
                document.setEvent("_updateDeleteDocumentType");
            } catch (NullPointerException ee) {
                dialogs.initMessage("Please, select record to delete");
                return;
            }

            try {
                RegistrationService.updateRecord(document, document.getEvent());
            } catch (Exception ee) {
                dialogs.initMessage("Error deleting record");
            }
            docsTable.getItems().clear();
            updateDocumentTypes();
            fetchDocuments();
        }
    }

    @FXML
    protected void exitDialog(ActionEvent ev) {
        ((Button) ev.getSource()).getScene().getWindow().hide();
    }

    @FXML
    protected void saveRecord(ActionEvent ev) {

        if (nameSurname.getText().isEmpty()) {
            dialogs.initUserError("Name and surname can't be empty!");
            //JOptionPane.showMessageDialog(null, "Name and surname cannot be empty!", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (personCode.getText().isEmpty()) {
            dialogs.initUserError("Person code can't be empty!");
            // JOptionPane.showMessageDialog(null, "Person code cannot be empty!", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        currentEntry.setNameSurname(nameSurname.getText());
        currentEntry.setRank(rank.getText());
        currentEntry.setCompany(company.getText());

        currentEntry.setPhone(phoneNumber.getText());

        currentEntry.setSecPhone(secPhoneNumber.getText());

        if (ccode.getText().contains("+")) {
            dialogs.initWarning("Please, no \"+\" sign in phone code ");
            return;
        }
        currentEntry.setCcode(ccode.getText());

        currentEntry.setnConnect(ncPhone.getText());
        currentEntry.setEmail(email.getText());
        currentEntry.setAddress(homeAddress.getText());
        currentEntry.setComments(userComments.getText());
        currentEntry.setVip(vipCheckBox.isSelected());
        currentEntry.setSkip(smsCheckBox.isSelected());
        currentEntry.setPersonCode(personCode.getText());
        currentEntry.setMarineBookId(sbNum.getText());
        currentEntry.setContractNum(contractNum.getText());

        Task<Integer> t = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                ClientService.updateRecord(currentEntry, "_updateUserProfile");
                setMessage("User profile updated");
                return 0;
            }
        };

        Task<Integer> tIns = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                ClientService.updateRecord(currentEntry, "_insertUserProfile");
                setMessage("User profile created");
                return 0;
            }
        };

        setProgress("Updating user profile");

        try {
            t.run();
        } catch (Exception e) {
            setError("Error updating user");
            dialogs.initErrorDialog(e.getMessage());
        }

        if (!oldPersonCode.equals(personCode.getText())) {
            isPkUnique();

            if (!unique) {
                setWarning("Person code not unique! Will not update it. Please, merge possible duplicates!");
                dialogs.initWarning("Duplicate person code found!");
                //JOptionPane.showMessageDialog(null, "", "Warning", JOptionPane.WARNING_MESSAGE);
            } else {
                updatePersonCode();
            }
        }

    }

    private void isPkUnique() {

        Task<Integer> t = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                List<ClientEntry> cli = new ArrayList<>();

                try {
                    cli = ClientService.fetchList(currentEntry, "idByPersonCode");
                } catch (Exception e) {
                    setError("Error checking person code!");
                    return 0;
                }

                if (cli.size() >= 1) {
                    unique = false;
                    return 0;
                } else {
                    unique = true;
                }

                return 0;
            }
        };

        Pattern lvFormat = Pattern.compile("[0-9]{6}-[0-9]{5}");
        Pattern intFormat = Pattern.compile(".{5}/[0-9]{4}-00000");
        Matcher pCode = lvFormat.matcher(personCode.getText());
        Matcher intCode = intFormat.matcher(personCode.getText());

        if (pCode.matches() && !"000000-00000".equals(personCode.getText()) && !oldPersonCode.equals(personCode.getText())) {
            t.run();
        }

        if (unique) {
            updatePersonCode();
        }

    }

    private void updatePersonCode() {

        currentEntry.setPersonCode(personCode.getText());

        Task<Integer> up = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                ClientService.updateRecord(currentEntry, "_updatePersonCode");
                setMessage("Profile saved and person code updated");
                return 0;
            }
        };

        setProgress("Updating person code");
        try {
            up.run();
        } catch (Exception e) {
            setError("Person code update failed");
        }

    }

    @FXML
    protected void updatePrefLang(ActionEvent ev) {

        Task<Integer> ts = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                HashMap<String, String> langs = new HashMap();
                langs.put("English", "en");
                langs.put("Russian", "ru");
                langs.put("Latvian", "lv");

                String lng = "en";

                if (null != prefLangBox.getValue()) {
                    lng = langs.get(prefLangBox.getValue());
                }

                ClientEntry ce = new ClientEntry();
                ce.setUserId(currentEntry.getUserId());
                ce.setPrefLang(lng);
                ClientService.updateRecord(ce, "_updatePrefLang");
                setMessage("Client language updated");
                pLang.setText("(" + lng + ")");
                return 0;

            }
        };

        setProgress("Saving default language");

        try {
            ts.run();
        } catch (Exception e) {
            setError("Update error");
        }

    }

    @FXML
    protected void openUserList(ActionEvent ev) {

    }

    @FXML
    private void printPassList(ActionEvent ev) {
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + new URI("https://apps.novikontas.lv/nReg/document.aq?_eventName=testList&dc="
                    + dc.getValue() + "&userId=" + currentEntry.getUserId()));
        } catch (URISyntaxException ex) {
            Logger.getLogger(GroupPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GroupPage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setMessage(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: green");
        progressBar.setProgress(1D);

    }

    private void setError(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: red");
        progressBar.setProgress(1D);
    }

    private void setProgress(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: #0000FF");
        progressBar.setProgress(-1D);
    }

    private void setWarning(String error) {
        statusLabel.setText(error);
        statusLabel.setStyle("-fx-text-fill: #F25509");
        progressBar.setProgress(1D);
    }
}
//Library1337
