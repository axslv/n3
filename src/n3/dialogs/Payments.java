package n3.dialogs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import lv.nmc.entities.ClientEntry;
import lv.nmc.entities.CourseEntry;
import lv.nmc.entities.JournalEntry;
import lv.nmc.entities.Payment;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.rest.ClientService;
import lv.nmc.rest.CourseService;
import lv.nmc.rest.JournalService;
import lv.nmc.rest.PaymentService;
import lv.nmc.rest.RegistrationService;
import n3.GroupPage;
import n3.WinLoader;
import n3.dialogs.shared.DialogState;
import n3.table.cells.AutoCompleteComboBoxListener;
import n3.table.cells.ps.BanDateCell;
import n3.table.cells.ps.AuxCell;
import n3.table.cells.ps.BillDateCell;
import n3.table.cells.ps.BillNrCell;
import n3.table.cells.ps.CertGivenCell;
import n3.table.cells.ps.NameSurnamePs;
import n3.table.cells.ps.PComment;
import n3.table.cells.ps.PCompany;
import n3.table.cells.ps.PCourseComment;
import n3.table.cells.ps.PPBank;
import n3.table.cells.ps.PPComment;
import n3.table.cells.ps.PPDate;
import n3.table.cells.ps.PPVoucher;
import n3.table.cells.ps.PriceCell;
import n3.table.cells.ps.VesselCell;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Payments {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ContextMenu groupNameContext;

    @FXML
    private AnchorPane payPane;

    @FXML
    private TableView<Payment> prePayments, paid, advTable;
    @FXML
    private TableColumn<Payment, String> pNameSurname, pCourseName, pDateEnd, pBillNr, pBillDate, pCompany, pPrice, pComments, pPersonCode;
    @FXML
    private TableColumn<Payment, String> ppBillNr, ppBillDate, ppBank, ppVoucher, ppPaymentDate, ppPricePaid, aNameSurname, aPayDate,
            aSource, aPrice, aCourse, aVoucher, ppComment, pDateStart, pBanned, aPersonCode, pVessel, pAux;
    @FXML
    private TableColumn<Payment, Integer> pId, ppRecordId, aId, pRegId;
    @FXML
    private TableColumn<Payment, Boolean> crtIssued;

    @FXML
    private TextField payVoucherNum, payPayDate, paySum;
    @FXML
    private ComboBox banklist, courseAbbr;

    @FXML
    private Label statusLabel, infoLabel, sumLabel;

    @FXML
    private ProgressIndicator progressBar;

    @FXML
    private TextField nameSurname, personCode, billNr, comment, dateEnd, payCurrency, billDate,
            advPayDate, advPaySum, advPayCurr, advNameSurname, advPersonCode, advVoucher, advComment;
    @FXML
    private ComboBox company, advCourse;
    private List<Payment> selectedPayments, advances = new ArrayList<>();
    private Payment selectedPayment = new Payment();
    private WinLoader dialogs = new WinLoader();
    private final DateTimeFormatter sqlDate = DateTimeFormat.forPattern("yyyy-MM-dd");
    private DateTime dateTime = new DateTime();
    @FXML
    private TitledPane advancesPane;
    @FXML
    private Label courseLabel, periodLabel;
    @FXML
    private ComboBox payBank, advPayBank, domain;
    @FXML
    private CheckBox debtsOnly;
    @FXML
    private TextField dateTill, dateStart, paymentDate, rankField, certField, courseField, advSearchStart, advSearchEnd, vesselField, auxField;    
    @FXML
    private TextArea regCommentField, accCommentField, bCommentField, totalPaymentsArea;

    private List<Payment> allPayments = new ArrayList<>();
    private List<Payment> debts = new ArrayList<>();
    private List<Payment> paidDebts = new ArrayList<>();

    @FXML
    void initialize() {
        fetchCompanies();
        initPrePayments();
        initPayments();
        initAdvances();
        initDomains();
        prepareBanks();
        AutoCompleteComboBoxListener lst = new AutoCompleteComboBoxListener(company);
        AutoCompleteComboBoxListener lst2 = new AutoCompleteComboBoxListener(courseAbbr);
        
    }

    @FXML
    private void advancesByInterval(ActionEvent ev) {
        selectedPayment.setDateStart(advSearchStart.getText());
        selectedPayment.setDateEnd(advSearchEnd.getText());
        selectedPayment.setEvent("advancesByInterval");
        List<Payment> advances = new ArrayList<>();

        try {
            advances = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            advTable.getItems().clear();
            advTable.getItems().addAll(advances);

        } catch (Exception ee) {
            ee.printStackTrace();
        }

    }

    @FXML
    private void exportAdvances(ActionEvent ev) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export excel");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Excel file", "*.xls"));
        List<Payment> advancesList = new ArrayList<>();

        advancesList.addAll(advTable.getItems());

        try {
            File f = fileChooser.showSaveDialog(payBank.getScene().getWindow());

            if (null == f) {
                return;
            }

            if (!f.getName().endsWith(".xls")) {
                f = new File(f.getAbsolutePath() + ".xls");
            }

            OutputStream os;
            os = (OutputStream) new FileOutputStream(f);
            String encoding = "UTF-8";
            OutputStreamWriter osw;
            osw = new OutputStreamWriter(os, encoding);
            BufferedWriter bw = new BufferedWriter(osw);

            StringBuilder export = new StringBuilder();
            export.append("Name Surname").append("\t");
            export.append("Person code").append("\t");
            export.append("Payment date").append("\t");
            export.append("Source").append("\t");
            export.append("Sum").append("\t");
            export.append("Course").append("\n");
            bw.write(export.toString());

            for (Payment xlsDebt : advancesList) {
                StringBuilder fin = new StringBuilder();
                fin.append(xlsDebt.getNameSurname()).append("\t");
                fin.append(xlsDebt.getPersonCode()).append("\t");
                fin.append(xlsDebt.getPaymentDate()).append("\t");
                fin.append(xlsDebt.getBank()).append("\t");
                fin.append(xlsDebt.getAdvanceSum().replace(",", ".")).append("\t");
                fin.append(xlsDebt.getCourseName()).append("\n");
                bw.write(fin.toString());
            }

            bw.flush();
            bw.close();

        } catch (Exception ee) {
            ee.printStackTrace();
        }

    }

    @FXML
    private void xlsExport(ActionEvent ev) {

        LinkedHashMap<Integer, List<Payment>> paidMap = new LinkedHashMap<>();
        LinkedHashMap<Integer, Payment> unpaidMap = new LinkedHashMap<>();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export excel");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Excel file", "*.xls"));

        try {

            File f = fileChooser.showSaveDialog(payBank.getScene().getWindow());

            if (null == f) {
                return;
            }

            if (!f.getName().endsWith(".xls")) {
                f = new File(f.getAbsolutePath() + ".xls");
            }

            OutputStream os;
            os = (OutputStream) new FileOutputStream(f);
            String encoding = "UTF-8";
            OutputStreamWriter osw;
            osw = new OutputStreamWriter(os, encoding);
            BufferedWriter bw = new BufferedWriter(osw);

            StringBuilder export = new StringBuilder();
            //  export.append("\ufeff");
            export.append("Name Surname").append("\t");
            export.append("Person code").append("\t");
            export.append("Course").append("\t");
            export.append("Start date").append("\t");
            export.append("End date").append("\t");
            export.append("Banned").append("\t");
            export.append("Bill Nr").append("\t");
            export.append("Bill date").append("\t");
            export.append("Company").append("\t");
            export.append("To pay").append("\t");
            export.append("Currency").append("\t");
            export.append("Source").append("\t");
            export.append("Voucher num").append("\t");
            export.append("Payment date").append("\t");
            export.append("Paid").append("\t");
            export.append("Paid CUR").append("\t");
            export.append("Comment1").append("\t");
            export.append("Comment2").append("\n");
            // byte[] utfb = export.toString().getBytes(Charset.forName("UTF-8"));

            bw.write(export.toString());

            List<String> pids = new ArrayList<>();
            for (Payment pp : prePayments.getItems()) {
                pids.add(pp.getPaymentId().toString());
                unpaidMap.put(pp.getPaymentId(), pp);
            }

            StringBuilder sb = new StringBuilder();
            //    System.out.println("Pids: " + pids.size()); //1943

            List<List<String>> chopped = chopped(pids, 50);

            //   System.out.println(chopped.size());
            List<Payment> payCache = new ArrayList<>();

            for (List<String> ls : chopped) {

                for (String str : ls) {
                    sb.append(str);
                    sb.append(",");
                }
                HashMap<String, String> addMap = new HashMap<>();

                selectedPayments = PaymentService.fetchByLongList(replaceLast(sb.toString(), ",", ""), "list", "detailsByMultipleIdsString");
                payCache.addAll(selectedPayments);
                sb = new StringBuilder();
                //  System.out.println("payCache: " + payCache.size());
            }

            // sb = new StringBuilder();
            for (Payment pss : payCache) {

                if (paidMap.containsKey(pss.getPaymentId())) {
                    paidMap.get(pss.getPaymentId()).add(pss);
                    continue;
                }

                List<Payment> pps = new ArrayList<>();
                pps.add(pss);
                paidMap.put(pss.getPaymentId(), pps);
            }

            for (Integer pid : unpaidMap.keySet()) {
                StringBuilder fin = new StringBuilder();
                Payment xlsDebt = unpaidMap.get(pid);
                fin.append(xlsDebt.getNameSurname()).append("\t");
                fin.append(xlsDebt.getPersonCode()).append("\t");
                fin.append(xlsDebt.getCourseName()).append("\t");
                fin.append(xlsDebt.getDateStart()).append("\t");
                fin.append(xlsDebt.getDateEnd()).append("\t");
                fin.append(xlsDebt.getBanDate()).append("\t");
                fin.append(xlsDebt.getBillNr()).append("\t");
                fin.append(xlsDebt.getBillDate()).append("\t");
                fin.append(xlsDebt.getCompany()).append("\t");

                try {
                    fin.append(xlsDebt.getDisplayPrice().split(" ")[0]).append("\t");
                    fin.append(xlsDebt.getDisplayPrice().split(" ")[1]).append("\t");
                } catch (Exception ee) {
                    fin.append(xlsDebt.getDisplayPrice()).append("\t");
                    fin.append(xlsDebt.getDisplayPrice()).append("\t");
                }

                List<Payment> paidList = paidMap.get(pid);

                if (null == paidList || paidList.isEmpty()) { //no payments for this record
                    fin.append("").append("\t"); //skip Source
                    fin.append("").append("\t"); //skip Voucher
                    fin.append("").append("\t"); //skip Payment date
                    fin.append("").append("\t"); //skip amount paid
                    fin.append("").append("\t"); //skip amount paid currency

                    if (null == xlsDebt.getCrewComment()) {
                        xlsDebt.setCrewComment("");
                    }

                    if (null == xlsDebt.getCourseComment()) {
                        xlsDebt.setCourseComment("");
                    }

                    fin.append(xlsDebt.getCrewComment().replace("\t", "").replace("\n", "").trim()).append("\t");
                    fin.append(xlsDebt.getCourseComment().replace("\t", "").replace("\n", "").trim()).append("\t").append("\n");

                    bw.write(fin.toString());
                    continue;
                }

                int step = 0;

                for (Payment paidd : paidList) {
                    step++;
                    if (step > 1) {
                        fin.append(xlsDebt.getNameSurname()).append("\t");
                        fin.append(xlsDebt.getPersonCode()).append("\t");
                        fin.append(xlsDebt.getCourseName()).append("\t");
                        fin.append(xlsDebt.getDateStart()).append("\t");
                        fin.append(xlsDebt.getDateEnd()).append("\t");
                        fin.append(xlsDebt.getBanned()).append("\t");
                        fin.append(xlsDebt.getBillNr()).append("\t");
                        fin.append(xlsDebt.getBillDate()).append("\t");
                        fin.append(xlsDebt.getCompany()).append("\t");

                        try {
                            fin.append(xlsDebt.getDisplayPrice().split(" ")[0]).append("\t");
                            fin.append(xlsDebt.getDisplayPrice().split(" ")[1]).append("\t");
                        } catch (Exception ee) {
                            fin.append(xlsDebt.getDisplayPrice()).append("\t");
                            fin.append(xlsDebt.getDisplayPrice()).append("\t");
                        }
                    }

                    fin.append(paidd.getBank()).append("\t");
                    fin.append(paidd.getVoucher()).append("\t");
                    fin.append(paidd.getPaymentDate()).append("\t");

                    try {
                        fin.append(paidd.getDisplayPrice().split(" ")[0]).append("\t");
                        fin.append(paidd.getDisplayPrice().split(" ")[1]).append("\t");
                    } catch (Exception ee) {
                        fin.append(paidd.getDisplayPrice()).append("\t");
                        fin.append(paidd.getDisplayPrice()).append("\t");
                    }

                    // fin.append(paidd.getDisplayPrice()).append("\t");
                    if (null == xlsDebt.getCrewComment()) {
                        xlsDebt.setCrewComment("");
                    }

                    if (null == xlsDebt.getCourseComment()) {
                        xlsDebt.setCourseComment("");
                    }

                    fin.append(xlsDebt.getCrewComment().replace("\t", "").replace("\n", "").trim()).append("\t");
                    fin.append(xlsDebt.getCourseComment().replace("\t", "").replace("\n", "").trim()).append("\t").append("\n");
                }

                bw.write(fin.toString());

            }

            bw.flush();
            bw.close();

        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    @FXML
    private void filterDebts() {

        if (debtsOnly.isSelected()) {
            debts.clear();
            paidDebts.clear();

            for (Payment pt : prePayments.getItems()) {
                if (pt.getActive() == 0) {
                    paidDebts.add(pt);
                    continue;
                }
                debts.add(pt);
            }

            prePayments.getItems().clear();
            prePayments.getItems().addAll(debts);
            return;
        }
        prePayments.getItems().clear();
        prePayments.getItems().addAll(debts);
        prePayments.getItems().addAll(paidDebts);

    }

    @FXML
    private void insertAdvance(ActionEvent ev) {
        ClientEntry re = new ClientEntry();
        Payment ppp = new Payment();
        try {
            ppp.setNameSurname(advNameSurname.getText());
            ppp.setPersonCode(advPersonCode.getText());
            ppp.setAdvanceSum(advPaySum.getText().replace(" EUR", "") + " " + advPayCurr.getText());
            try {
                ppp.setCourseName(advCourse.getValue().toString().trim());
            } catch (NullPointerException np) {
                dialogs.initUserError("Please, select course");
                return;
            }
            ppp.setVoucher(advVoucher.getText());
            if (advVoucher.getText().isEmpty()) {
                ppp.setVoucher("n/a");
            }

            ppp.setPaymentDate(advPayDate.getText());
            ppp.setBank(advPayBank.getValue().toString());
            ppp.setPaymentId(0);

            re.setNameSurname(advNameSurname.getText());
            re.setPersonCode(advPersonCode.getText());
        } catch (Exception ee) {
            dialogs.initUserError("Error occured. Please, check data you entered.");
            return;
        }

        if (null == advPayBank.getValue()) {
            dialogs.initUserError("Bank is empty");
            return;
        }

        try {
            List<ClientEntry> ls = new ArrayList<>();
            ls = ClientService.fetchList(re, "clientDetailsByPersonCodeAndName");

            if (ls.isEmpty()) {
                dialogs.initUserError("This user not found. Please, check name and/or person code.");
                return;
            }

            JournalEntry je = new JournalEntry();
            je.setUsername(System.getProperty("n3.username"));
            je.setEvent("_insertJournal");
            je.setUserId(ls.get(0).getUserId());
            je.setOptype("insert");
            je.setSysmsg("Username " + je.getUsername() + " has inserted advance record for client |"
                    + ls.get(0).getNameSurname() + "//" + ls.get(0).getPersonCode() + "| userId=" + ls.get(0).getUserId() + " and sum=" + ppp.getAdvanceSum());

            PaymentService.updateRecord(ppp, "_updateInsertAdvance");
            JournalService.insertRecord(je, je.getEvent());
            advTable.getItems().add(ppp);
            //  refreshAdvances();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    @FXML
    private void deleteAdvance(ActionEvent ev) {
        if (null == advTable.getSelectionModel().getSelectedItem()) {
            dialogs.initUserError("Please, select advance to delete.");
            return;
        }

        dialogs.initConfirmDialog("Sure to delete selected advance?");

        if (dialogs.getResult() == 1) {
            return;
        }

        selectedPayment.setAdvanceId(advTable.getSelectionModel().getSelectedItem().getAdvanceId());
        JournalEntry je = new JournalEntry();
        je.setUsername(System.getProperty("n3.username"));
        je.setOptype("delete");
        je.setSysmsg("Username " + je.getUsername() + " has deleted advance record with id=" + selectedPayment.getAdvanceId()
                + " sum=" + advTable.getSelectionModel().getSelectedItem().getAdvanceSum());
        je.setUserId(prePayments.getSelectionModel().getSelectedItem().getUserId());
        je.setEvent("_insertJournal");
        try {
            PaymentService.updateRecord(selectedPayment, "_updateDeleteAdvance");
            JournalService.insertRecord(je, je.getEvent());
        } catch (Exception ee) {
            ee.printStackTrace();
        }
        advTable.getItems().remove(advTable.getSelectionModel().getSelectedIndex());

    }

    @FXML
    private void newRegistration(ActionEvent ev) {

    }

    @FXML
    private void coursesByDomain(ActionEvent ev) {
        courseAbbr.getItems().clear();
        advCourse.getItems().clear();

        CourseEntry ce = new CourseEntry();
        ce.setDomain(domain.getValue().toString());
        List<CourseEntry> fetchList = new ArrayList();

        try {
            fetchList = CourseService.fetchList(ce, "coursesByDomain");
        } catch (Exception ex) {
            dialogs.initErrorDialog(ex.getMessage());
            Logger.getLogger(GroupPage.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (CourseEntry ice : fetchList) {
            courseAbbr.getItems().add(ice.getGroupAbbr());
            advCourse.getItems().add(ice.getGroupAbbr());
        }

    }

    private Boolean partialPayment = false;

    @FXML
    private void insertPayment(ActionEvent ev) {

        BigDecimal paidSum = new BigDecimal(0.00);
        for (Payment pp : paid.getItems()) {
            String[] sums = pp.getDisplayPrice().split(" ");
            paidSum = paidSum.add(new BigDecimal(sums[0]));
        }
        BigDecimal debtSum = new BigDecimal(prePayments.getSelectionModel().getSelectedItem().getDisplayPrice().split(" ")[0]);

        // new BigDecimal(paySum.getText())
        if (Math.signum(new BigDecimal(paySum.getText()).doubleValue()) > 0) {
            if (paidSum.compareTo(debtSum) > 0) {
                dialogs.initUserError("There is no more debt on this record");
                return;
            }

            BigDecimal future = paidSum.add(new BigDecimal(paySum.getText()));

            if (future.compareTo(debtSum) > 0) {
                dialogs.initUserError("Payment exceeds it's value!");
                return;
            }
        }

        BigDecimal wantToPay = new BigDecimal(paySum.getText());
        //System.out.println(selectedPayment.getDisplayPrice().split(" ")[0]);
        BigDecimal debt = new BigDecimal(selectedPayment.getDisplayPrice().split(" ")[0]);

        selectedPayment = prePayments.getSelectionModel().getSelectedItem();
        selectedPayment.setEvent("_updateInsertPayment");
        selectedPayment.setVoucher(payVoucherNum.getText());
        selectedPayment.setPaidSum(paySum.getText() + " " + payCurrency.getText());
        selectedPayment.setBank(payBank.getValue().toString());
        selectedPayment.setPaymentDate(payPayDate.getText());
        selectedPayment.setBillNr(billNr.getText());
        selectedPayment.setBillDate(billDate.getText());

        if (debt.compareTo(wantToPay) == 0) {
            selectedPayment.setActive(0);
            prePayments.getSelectionModel().getSelectedItem().setActive(0);
            refreshPrePayments();
        }

        if (wantToPay.compareTo(debt) == -1) {
            partialPayment = true;
        }

        BigDecimal partials = new BigDecimal(0.00);
        partials.setScale(2);

        if (partialPayment) {
            for (Payment p : paid.getItems()) {
                partials = partials.add(new BigDecimal(p.getDisplayPrice().split(" ")[0]));
            }

            BigDecimal oct = partials.add(new BigDecimal(paySum.getText()));

            if (debt.compareTo(oct) == 0) {
                selectedPayment.setActive(0);
                prePayments.getSelectionModel().getSelectedItem().setActive(0);
                refreshPrePayments();
            }

        }

        JournalEntry je = new JournalEntry();
        je.setUsername(System.getProperty("n3.username"));
        je.setRegistrationId(prePayments.getSelectionModel().getSelectedItem().getRegistrationId());
        je.setOptype("insert");
        je.setSysmsg("Username " + je.getUsername() + " has inserted payment for registrationId=" + je.getRegistrationId() + ". Price=" + selectedPayment.getPaidSum());
        je.setEvent("_insertJournal");
        je.setUserId(prePayments.getSelectionModel().getSelectedItem().getUserId());

        try {
            PaymentService.updateRecord(selectedPayment, selectedPayment.getEvent());
            PaymentService.updateRecord(selectedPayment, "_updateChangePaymentDetails");
            JournalService.insertRecord(je, je.getEvent());

        } catch (Exception ee) {
            ee.printStackTrace();
        }
        fetchPaymentDetails();

    }

    @FXML
    private void preparePayment(ActionEvent ev) {

        selectedPayment = prePayments.getSelectionModel().getSelectedItem();

        try {
            calcRequiredSum();
        } catch (NumberFormatException exc) {
            dialogs.initUserError("Please, set correct format for price in prepaiment data!");
            return;
        }

        payPayDate.setText(dateTime.toString(sqlDate));
        infoLabel.setText(infoLabel.getText().replace("%course%", selectedPayment.getCourseName()));
        billNr.setText(selectedPayment.getBillNr());
        billDate.setText(selectedPayment.getBillDate());

        try {
            String[] summ = selectedPayment.getSumToPay().split(" ");
            sumLabel.setText(summ[0]);
            payCurrency.setText(summ[1]);
            paySum.setText(summ[0]);
        } catch (IndexOutOfBoundsException ex) {
            dialogs.initUserError("Please, set correct format for price in prepaiment data!");
        }
    }

    private void calcRequiredSum() throws NumberFormatException {
        BigDecimal paidSum = new BigDecimal(0.00);
        if (paid.getItems().size() == 0) {
            selectedPayment.setSumToPay(prePayments.getSelectionModel().getSelectedItem().getDisplayPrice());
            return;
        }

        for (Payment pp : paid.getItems()) {
            String[] sums = pp.getDisplayPrice().split(" ");
            paidSum = paidSum.add(new BigDecimal(sums[0]));

        }

        BigDecimal toPay = new BigDecimal(prePayments.getSelectionModel().getSelectedItem().getDisplayPrice().split(" ")[0]).subtract(paidSum);
        selectedPayment.setSumToPay(toPay.toString() + " " + prePayments.getSelectionModel().getSelectedItem().getDisplayPrice().split(" ")[1]);
        //System.out.println(prePayments.getSelectionModel().getSelectedItem().getDisplayPrice().split(" ")[0]);
    }

    @FXML
    private void updateSumLabel(KeyEvent ev) {
        sumLabel.setText(paySum.getText());
        selectedPayment.setSumToPay(paySum.getText());
    }

    private void fetchLastRecords() {

    }

    @FXML
    private ListView<String> companiesList;

    private void fetchCompanies() {
        
        companiesList.setEditable(true);

		companiesList.setCellFactory(TextFieldListCell.forListView());		

		companiesList.setOnEditCommit(new EventHandler<ListView.EditEvent<String>>() {
			@Override
			public void handle(ListView.EditEvent<String> t) {
                            String oldValue = companiesList.getItems().get(t.getIndex());
				companiesList.getItems().set(t.getIndex(), t.getNewValue());
                                selectedPayment.setEvent("_updateCompany");
                                selectedPayment.setCompany(t.getNewValue());
                                selectedPayment.setAuxInfo(oldValue);
                                
                                if (null == t.getNewValue()) {
                                    dialogs.initMessage("Empty company name!");
                                    return;
                                }
                                
                                if (t.getNewValue().trim().isEmpty()) {
                                    dialogs.initMessage("Empty company name!");
                                    return;
                                }
                                
                                try {
                                    PaymentService.updateRecord(selectedPayment, selectedPayment.getEvent());
                                } catch (Exception ee) {
                                    ee.printStackTrace();
                                }
                                
			}
						
		});

       
        company.getItems().clear();
        companiesList.getItems().clear();
        RegistrationEntry re = new RegistrationEntry();
        List<RegistrationEntry> rel = new ArrayList<>();
        re.setEvent("fetchCompanies");

        try {
            rel = RegistrationService.fetchList(re, re.getEvent());
            for (RegistrationEntry inner : rel) {
                company.getItems().add(inner.getCompany());
                companiesList.getItems().add(inner.getCompany());
            }

        } catch (Exception ex) {

        }
    }

    @FXML
    private TextField newCompany;

    @FXML
    private void addCompany(ActionEvent ev) {

        if (null == newCompany.getText() || newCompany.getText().trim().isEmpty()) {
            dialogs.initUserError("Empty company name!");
            return;
        }

        selectedPayment.setEvent("_updateInsertCompany");
        selectedPayment.setCompany(newCompany.getText());

        try {
            PaymentService.updateRecord(selectedPayment, selectedPayment.getEvent());
            fetchCompanies();
            newCompany.clear();
        } catch (Exception ee) {
            dialogs.initUserError(ee.getMessage());
        }

    }

    @FXML
    private void deletePaidItem(ActionEvent ev) {
        selectedPayment = paid.getSelectionModel().getSelectedItem();
        selectedPayment.setEvent("_updateRemovePaidRecord");
        Integer paymentId = selectedPayment.getPaymentId();

        dialogs.initConfirmDialog("SURE TO REMOVE PAYMENT ENTRY?");

        if (DialogState.getInstance().getState() == 1) {
            return;
        }

        JournalEntry je = new JournalEntry();
        je.setUsername(System.getProperty("n3.username"));
        je.setOptype("delete");
        je.setRegistrationId(prePayments.getSelectionModel().getSelectedItem().getRegistrationId());
        je.setUserId(prePayments.getSelectionModel().getSelectedItem().getUserId());
        je.setSysmsg("Username " + je.getUsername() + " has deleted payment record for registrationId=" + je.getRegistrationId() + ". Paid price was: " + paid.getSelectionModel().getSelectedItem().getDisplayPrice());
        je.setEvent("_insertJournal");

        try {
            PaymentService.updateRecord(selectedPayment, selectedPayment.getEvent());
            JournalService.insertRecord(je, je.getEvent());
            paid.getItems().remove(paid.getSelectionModel().getSelectedIndex());
        } catch (Exception ex) {
            Logger.getLogger(Payments.class.getName()).log(Level.SEVERE, null, ex);
        }

        BigDecimal paidSum = new BigDecimal(0.00);
        BigDecimal debtSum = new BigDecimal(prePayments.getSelectionModel().getSelectedItem().getDisplayPrice().split(" ")[0]);

        for (Payment pp : paid.getItems()) {
            String[] sums = pp.getDisplayPrice().split(" ");
            paidSum = paidSum.add(new BigDecimal(sums[0]));
        }

        if (debtSum.equals(paidSum)) {
            return;
        }

        selectedPayment.setEvent("_updateActivatePayment");
        selectedPayment.setPaymentId(paymentId);
        prePayments.getSelectionModel().getSelectedItem().setActive(1);
        refreshPrePayments();

        try {
            PaymentService.updateRecord(selectedPayment, selectedPayment.getEvent());
        } catch (Exception ee) {

        }

    }

    private void initPrePayments() {
        prePayments.getSelectionModel().setCellSelectionEnabled(true);
        prePayments.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        prePayments.setEditable(true);
        pBillNr.setEditable(true);
        pBillDate.setEditable(true);
        pCompany.setEditable(true);
        pComments.setEditable(true);
        crtIssued.setEditable(true);

        pId.setCellValueFactory(new PropertyValueFactory<Payment, Integer>("paymentId"));
        pNameSurname.setCellValueFactory(new PropertyValueFactory<Payment, String>("nameSurname"));
        pPersonCode.setCellValueFactory(new PropertyValueFactory<Payment, String>("personCode"));
        pCourseName.setCellValueFactory(new PropertyValueFactory<Payment, String>("courseName"));
        pDateEnd.setCellValueFactory(new PropertyValueFactory<Payment, String>("dateEnd"));
        pBillNr.setCellValueFactory(new PropertyValueFactory<Payment, String>("billNr"));
        pBillDate.setCellValueFactory(new PropertyValueFactory<Payment, String>("billDate"));
        pCompany.setCellValueFactory(new PropertyValueFactory<Payment, String>("company"));
        pPrice.setCellValueFactory(new PropertyValueFactory<Payment, String>("displayPrice"));
        pComments.setCellValueFactory(new PropertyValueFactory<Payment, String>("crewComment"));
        pDateStart.setCellValueFactory(new PropertyValueFactory<Payment, String>("dateStart"));
        crtIssued.setCellValueFactory(new PropertyValueFactory<Payment, Boolean>("crtIssued"));
        pBanned.setCellValueFactory(new PropertyValueFactory<Payment, String>("banDate"));
        pRegId.setCellValueFactory(new PropertyValueFactory<Payment, Integer>("registrationId"));
        pVessel.setCellValueFactory(new PropertyValueFactory<Payment, String>("vessel"));
        pAux.setCellValueFactory(new PropertyValueFactory<Payment, String>("auxInfo"));
        

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> banFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new BanDateCell();
            }
        };
        
        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> priceFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new PriceCell();
            }
        };

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> billNrFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new BillNrCell();
            }
        };

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> nsFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new NameSurnamePs();
            }
        };

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> companyFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new PCompany();
            }
        };

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> commentFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new PComment();
            }
        };

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> courseCommentFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new PCourseComment();
            }
        };

        Callback<TableColumn<Payment, Boolean>, TableCell<Payment, Boolean>> certPrintFactory
                = new Callback<TableColumn<Payment, Boolean>, TableCell<Payment, Boolean>>() {
            @Override
            public TableCell<Payment, Boolean> call(TableColumn<Payment, Boolean> p) {
                return new CertGivenCell();
            }
        };

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> billDateFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new BillDateCell();
            }
        };
        
        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> vesselFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new VesselCell();
            }
        };
        
        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> auxFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new AuxCell();
            }
        };

        pCompany.setCellFactory(companyFactory);
        pComments.setCellFactory(commentFactory);
        pNameSurname.setCellFactory(nsFactory);
        pPrice.setCellFactory(priceFactory);
        crtIssued.setCellFactory(certPrintFactory);
        pBillNr.setCellFactory(billNrFactory);
        pBillDate.setCellFactory(billDateFactory);
        pVessel.setCellFactory(vesselFactory);
        pAux.setCellFactory(auxFactory);
        pBanned.setCellFactory(banFactory);

        prePayments.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observale, Object oldValue, Object newValue) {
                payVoucherNum.clear();
                Payment ge = (Payment) newValue;

                if (null == ge) {
                    return;
                }

                selectedPayment = ge;
                fetchPaymentDetails();
                courseLabel.setText(ge.getCourseName());
                preparePayment(new ActionEvent());
                advNameSurname.setText(selectedPayment.getNameSurname());
                advPersonCode.setText(selectedPayment.getPersonCode());
                vesselField.setText(selectedPayment.getVessel());
                auxField.setText(selectedPayment.getAuxInfo());
                advPayDate.clear();
                advPaySum.clear();
                advVoucher.clear();

            }
        });
    }

    private void initPayments() {
        paid.getSelectionModel().setCellSelectionEnabled(true);
        paid.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        paid.setEditable(true);
        ppBank.setEditable(true);
        ppVoucher.setEditable(true);
        ppComment.setEditable(true);
        ppPaymentDate.setEditable(true);

        ppRecordId.setCellValueFactory(new PropertyValueFactory<Payment, Integer>("paidId"));
        ppBank.setCellValueFactory(new PropertyValueFactory<Payment, String>("bank"));
        ppVoucher.setCellValueFactory(new PropertyValueFactory<Payment, String>("voucher"));
        ppPaymentDate.setCellValueFactory(new PropertyValueFactory<Payment, String>("paymentDate"));
        ppPricePaid.setCellValueFactory(new PropertyValueFactory<Payment, String>("displayPrice"));
        ppComment.setCellValueFactory(new PropertyValueFactory<Payment, String>("ppComment"));
        ppBillNr.setCellValueFactory(new PropertyValueFactory<Payment, String>("billNr"));
        ppBillDate.setCellValueFactory(new PropertyValueFactory<Payment, String>("billDate"));

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> bankFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new PPBank();
            }
        };

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> billDateFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new BillDateCell();
            }
        };

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> pdFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new PPDate();
            }
        };

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> vFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new PPVoucher();
            }
        };

        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> commentFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {
            @Override
            public TableCell<Payment, String> call(TableColumn<Payment, String> p) {
                return new PPComment();
            }
        };

        ppVoucher.setCellFactory(vFactory);
        ppBank.setCellFactory(bankFactory);
        ppComment.setCellFactory(commentFactory);
        ppPaymentDate.setCellFactory(pdFactory);
        ppBillDate.setCellFactory(billDateFactory);

    }

    private void initAdvances() {
        advTable.getSelectionModel().setCellSelectionEnabled(true);
        advTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        // advTable.setEditable(true);
        aId.setCellValueFactory(new PropertyValueFactory<Payment, Integer>("advanceId"));
        aNameSurname.setCellValueFactory(new PropertyValueFactory<Payment, String>("nameSurname"));
        aPersonCode.setCellValueFactory(new PropertyValueFactory<Payment, String>("personCode"));
        aPayDate.setCellValueFactory(new PropertyValueFactory<Payment, String>("paymentDate"));
        aSource.setCellValueFactory(new PropertyValueFactory<Payment, String>("bank"));
        aPrice.setCellValueFactory(new PropertyValueFactory<Payment, String>("advanceSum"));
        aCourse.setCellValueFactory(new PropertyValueFactory<Payment, String>("courseName"));
        aVoucher.setCellValueFactory(new PropertyValueFactory<Payment, String>("voucher"));

        advTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observale, Object oldValue, Object newValue) {
                if (null == newValue) {
                    return;
                }
                Payment newp = (Payment) newValue;
                advComment.setText(newp.getNameSurname() + " " + newp.getPaymentDate() + " " + newp.getBank() + " "
                        + newp.getAdvanceSum() + " " + newp.getCourseName() + " " + newp.getVoucher()
                );

            }
        });

    }

    private void fetchPaymentDetails() {
        rankField.clear();
        certField.clear();
        bCommentField.clear();
        courseField.clear();

        paid.getItems().clear();
        advTable.getItems().clear();
        selectedPayment.setEvent("paymentDetails");
        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());

            if (!selectedPayments.isEmpty()) {
                rankField.setText(selectedPayments.get(0).getRank());
                certField.setText(selectedPayments.get(0).getCertNr());
                bCommentField.setText(selectedPayments.get(0).getPpComment());
                courseField.setText(selectedPayments.get(0).getCourseName());
            }

            for (Payment ps : selectedPayments) {
                if (null != ps.getDisplayPrice()) {
                    paid.getItems().add(ps);
                }
            }

            selectedPayment.setEvent("advancesByUserId");
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            advTable.getItems().addAll(selectedPayments);
            advancesPane.setText("ADVANCES (" + selectedPayments.size() + ") " + selectedPayment.getNameSurname().toUpperCase());
            if (selectedPayments.size() > 0) {
                advancesPane.setStyle("-fx-text-fill: red; ");
            } else {
                advancesPane.setStyle("-fx-text-fill: black");
            }

            regCommentField.setText(prePayments.getSelectionModel().getSelectedItem().getCrewComment());
            accCommentField.setText(prePayments.getSelectionModel().getSelectedItem().getCourseComment());

        } catch (Exception ee) {
            setError("Error");
            ee.printStackTrace();
        }
    }

    @FXML
    private void addBill(ActionEvent ev) {
        List<Integer> pids = new ArrayList<>();

        dialogs.initConfirmDialog("You are about to update selected records with bill num " + billNr.getText()
                + " and bill date " + billDate.getText() + ". Are you sure?");

        if (1 == DialogState.getInstance().getState()) {
            return;
        }

        List<Payment> selectedItems = prePayments.getSelectionModel().getSelectedItems();
        List<Payment> ls = new ArrayList<>();

        for (Payment pe : selectedItems) {
            pids.add(pe.getPaymentId());
            pe.setBillNr(billNr.getText());
            pe.setBillDate(billDate.getText());

            if (null == billNr.getText() || billNr.getText().isEmpty()) {
                pe.setBillNr("n/a");
            }

            if (null == billDate.getText() || billDate.getText().isEmpty()) {
                pe.setBillDate("1970-01-01");
            }

            pe.setEvent("_updateChangeBill");
            try {
                PaymentService.updateRecord(pe, pe.getEvent());
                pe.setEvent("_updateChangePaidBill");
                PaymentService.updateRecord(pe, pe.getEvent());

                JournalEntry je = new JournalEntry();
                je.setEvent("_insertJournal");
                je.setUsername(System.getProperty("n3.username"));
                je.setOptype("update");
                je.setUserId(pe.getUserId());
                je.setRegistrationId(pe.getRegistrationId());
                je.setSysmsg("Username " + je.getUsername() + " has updated bill number for payment record with paymentId=" + pe.getPaymentId() + ". billNr=" + pe.getBillNr() + 
                        " AND billDate=" + pe.getBillDate());
                JournalService.insertRecord(je, je.getEvent());

            } catch (Exception ee) {
                ee.printStackTrace();
                dialogs.initErrorDialog(ee.getMessage());
            }

        }

        prePayments.getColumns().get(0).setVisible(false);
        prePayments.getColumns().get(0).setVisible(true);

    }

    private void prepareSearch() {
        debts.clear();
        paidDebts.clear();
        debtsOnly.setSelected(false);

    }

    private void fillPrePayments() {
        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
            setMessage("Records ready. Total rows: " + selectedPayments.size());
        } catch (Exception ee) {
            setError("Error");
        }
    }

    @FXML
    private void byPaymentDate(ActionEvent ev) {
        if (!isDateGood(paymentDate.getText())) {
            return;
        }

        if (!dateTill.getText().isEmpty()) {
            byPaymentDateInterval();
            return;
        }

        prepareSearch();
        selectedPayment.setPaymentDate(paymentDate.getText());
        selectedPayment.setEvent("byPaymentDate");
        prePayments.getItems().clear();

        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }

    }

    @FXML
    private void byCourse(ActionEvent ev) {

        if (null == courseAbbr.getValue()) {
            return;
        }

        if (courseAbbr.getValue().toString().isEmpty()) {
            return;
        }

        if (!dateTill.getText().isEmpty()) {
            byCourseInterval();
            return;
        }

        prepareSearch();
        selectedPayment.setCourseName(courseAbbr.getValue().toString());
        selectedPayment.setEvent("byCourse");

        prePayments.getItems().clear();

        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }

        //fetchPrepayData();
    }

    @FXML
    private void byNameSurname(ActionEvent ev) {
        prepareSearch();
        if (nameSurname.getText().length() < 3) {
            dialogs.initUserError("Please, enter at least 3 characters");
            return;
        }
        prePayments.getSelectionModel().clearSelection();

        selectedPayment.setNameSurname(nameSurname.getText());
        selectedPayment.setEvent("byNameSurname");

        prePayments.getItems().clear();

        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }

        //fetchPrepayData();
    }

    @FXML
    private void byPersonCode(ActionEvent ev) {
        prepareSearch();
        if (personCode.getText().length() < 5) {
            dialogs.initUserError("Please, enter at least 5 characters");
            return;
        }

        selectedPayment.setPersonCode(personCode.getText());
        selectedPayment.setEvent("byPersonCode");
        //fetchPrepayData();

        prePayments.getItems().clear();

        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");

        }

    }

    @FXML
    private TextField fbillNr;

    @FXML
    private void byBillNr(ActionEvent ev) {
        prepareSearch();
        if (fbillNr.getText().length() < 3) {
            dialogs.initUserError("Please, enter at least 3 characters");
            return;
        }

        if (!dateTill.getText().isEmpty()) {
            byBillNrInterval();
            return;
        }

        selectedPayment.setBillNr(fbillNr.getText());
        selectedPayment.setEvent("byBillNr");
        // fetchPrepayData();

        prePayments.getItems().clear();

        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");

        }

    }

    @FXML
    private void copyVal(ActionEvent ev) {

        ObservableList<TablePosition> sc = prePayments.getSelectionModel().getSelectedCells();
        int row = sc.get(0).getRow();
        Payment item = prePayments.getItems().get(row);
        TableColumn col = sc.get(0).getTableColumn();
        String data = (String) col.getCellObservableValue(item).getValue();

        ClipboardContent cc = new ClipboardContent();
        cc.putString(data);
        Clipboard.getSystemClipboard().setContent(cc);

    }

    @FXML
    private void clrde(ActionEvent ev) {
        dateEnd.setText("");
    }

    @FXML
    private void clrds(ActionEvent ev) {
        dateStart.setText("");
    }

    @FXML
    private void byDateInterval(ActionEvent ev) {
        //prepareSearch();
    }

    private void byPaymentDateInterval() {

        prepareSearch();
        selectedPayment.setDateStart(paymentDate.getText());
        selectedPayment.setDateEnd(dateTill.getText());
        selectedPayment.setEvent("byPaymentDateInterval");

        if (null != payBank.getValue() && !"n/a".equals(payBank.getValue().toString())) {
            byPaymentDateBankInterval();
            return;
        }

        prePayments.getItems().clear();
        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }
        calcSumsTotal();
    }

    private void byPaymentDateBankInterval() {

        if (!isDateGood(paymentDate.getText()) || !isDateGood(dateTill.getText())) {
            dialogs.initWarning("Please check date formats. Should by YYYY-MM-DD");
            return;
        }
        prepareSearch();
        selectedPayment.setDateStart(paymentDate.getText());
        selectedPayment.setDateEnd(dateTill.getText());
        selectedPayment.setBank(payBank.getValue().toString());
        selectedPayment.setEvent("byPaymentDateBankInterval");

        prePayments.getItems().clear();
        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }

    }

    private void byCompanyInterval() {
        selectedPayment.setDateStart(dateStart.getText());
        selectedPayment.setDateEnd(dateTill.getText());
        selectedPayment.setCompany(company.getValue().toString());
        selectedPayment.setEvent("byCompanyInterval");
        prePayments.getItems().clear();
        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }
    }

    private void byBillNrInterval() {
        selectedPayment.setDateStart(dateStart.getText());
        selectedPayment.setDateEnd(dateTill.getText());
        selectedPayment.setBillNr(billNr.getText());
        selectedPayment.setEvent("byBillNrInterval");
        prePayments.getItems().clear();
        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }
    }

    private void byCourseInterval() {
        selectedPayment.setDateStart(dateStart.getText());
        selectedPayment.setDateEnd(dateTill.getText());
        selectedPayment.setCourseName(courseAbbr.getValue().toString());
        selectedPayment.setEvent("byCourseInterval");
        prePayments.getItems().clear();
        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }
    }

    @FXML
    private void byCommentInterval() {
        prepareSearch();
        selectedPayment.setCourseComment(comment.getText());
        selectedPayment.setCrewComment(comment.getText());
        selectedPayment.setPpComment(comment.getText());
        selectedPayment.setEvent("byCommentInterval");
        selectedPayment.setDateStart(dateStart.getText());
        selectedPayment.setDateEnd(dateTill.getText());
        // fetchPrepayData();

        prePayments.getItems().clear();

        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");

        }
    }

    private void byPaymentDate() {
        prepareSearch();
        selectedPayment.setDateStart(paymentDate.getText());
        selectedPayment.setDateEnd(dateTill.getText());
        selectedPayment.setEvent("byPaymentDate");

        if (null != dateTill.getText() && !"n/a".equals(dateTill.getText())) {
            byPaymentDateInterval();
            return;
        }

        if (null != payBank.getValue() && !"n/a".equals(payBank.getValue())) {
            byPaymentDateInterval();
            return;
        }
        prePayments.getItems().clear();
        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }
    }

    private void byDateEndInterval() {

        if (!isDateGood(dateEnd.getText()) || !isDateGood(dateTill.getText())) {
            dialogs.initWarning("Incorrect date format! Should be YYYY-MM-DD");
            return;
        }

        prepareSearch();
        selectedPayment.setDateStart(dateEnd.getText());
        selectedPayment.setDateEnd(dateTill.getText());
        selectedPayment.setEvent("byDateEndInterval");
        prePayments.getItems().clear();
        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }
    }

    private void byDateStartInterval() {
        prepareSearch();
        if (!isDateGood(dateStart.getText()) || !isDateGood(dateTill.getText())) {
            dialogs.initWarning("Incorrect date format! Should be YYYY-MM-DD");
            return;
        }

        selectedPayment.setDateStart(dateStart.getText());
        selectedPayment.setDateEnd(dateTill.getText());
        selectedPayment.setEvent("byDateStartInterval");
        prePayments.getItems().clear();

        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }
    }

    @FXML
    private void byDateStart(ActionEvent ev) {

        if (!isDateGood(dateStart.getText())) {
            dialogs.initWarning("Incorrect date format! Should be YYYY-MM-DD");
            return;
        }

        if (!dateTill.getText().trim().isEmpty() && null != dateTill.getText()) {
            byDateStartInterval();
            return;
        }

        prepareSearch();
        selectedPayment.setDateStart(dateStart.getText());
        selectedPayment.setEvent("byDateStart");
        prePayments.getItems().clear();

        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
        } catch (Exception ee) {
            ee.printStackTrace();
        }
        prePayments.getItems().addAll(selectedPayments);

    }

    @FXML
    private void byDateEnd(ActionEvent ev) {
        if (!isDateGood(dateEnd.getText())) {
            dialogs.initWarning("Incorrect date format! Should be YYYY-MM-DD");
            return;
        }

        if (!dateTill.getText().isEmpty()) {
            byDateEndInterval();
            return;
        }

        prepareSearch();
        selectedPayment.setDateEnd(dateEnd.getText());
        selectedPayment.setEvent("byDateEnd");
        prePayments.getItems().clear();

        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");
        }

    }

    @FXML
    private void byComment(ActionEvent ev) {

        if (null != dateTill.getText()) {
            byCommentInterval();
            return;
        }

        prepareSearch();
        selectedPayment.setCourseComment(comment.getText());
        selectedPayment.setEvent("byComment");
        prePayments.getItems().clear();

        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            prePayments.getItems().addAll(selectedPayments);
        } catch (Exception ee) {
            setError("Error");

        }

    }

    @FXML
    private void byCompany(ActionEvent ev) {

        Task<Integer> t = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {

                Thread thr = new Thread() {
                    public void run() {

                        if (null == company.getSelectionModel().getSelectedItem()) {
                            return;
                        }

                        if (!dateTill.getText().trim().isEmpty() && null != dateTill.getText()) {
                            byCompanyInterval();
                            return;
                        }

                        prepareSearch();
                        selectedPayment.setEvent("byCompany");
                        selectedPayment.setCompany(company.getValue().toString());
                        try {
                            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }

                    }
                };

                thr.start();

                while (true) {

                    if (!thr.isAlive()) {
                        prePayments.getItems().clear();
                        prePayments.getItems().addAll(selectedPayments);
                        break;
                    }

                }

                return 0;
            }
        };

        Platform.runLater(t);

    }

    private void prepareBanks() {
        Payment pm = new Payment();
        pm.setEvent("selectBanks");
        advPayBank.getItems().clear();
        List<Payment> pms = new ArrayList<>();

        try {
            pms = PaymentService.fetchList(pm, pm.getEvent());
            payBank.getItems().clear();
        } catch (Exception ee) {
            ee.printStackTrace();
        }

        for (Payment p : pms) {
            payBank.getItems().add(p.getBank());
            advPayBank.getItems().add(p.getBank());
        }
    }

    private void initDomains() {
        courseAbbr.getItems().clear();
        domain.getItems().clear();
        domain.getItems().add("tc");
        domain.getItems().add("ppp_eng");
        domain.getItems().add("ppp_nav");
        domain.getItems().add("col_eng");
        domain.getItems().add("col_nav");
        domain.getItems().add("gwo");
        domain.getItems().add("pvn");
    }

    private void setProgress(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: green");
        progressBar.setProgress(-1D);
    }

    private void setError(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: red");
        progressBar.setProgress(1D);
    }

    private void setMessage(String message) {
        statusLabel.setText(message);
        statusLabel.setStyle("-fx-text-fill: green");
        progressBar.setProgress(1D);
    }

    private void refreshPrePayments() {
        prePayments.getColumns().get(0).setVisible(false);
        prePayments.getColumns().get(0).setVisible(true);
    }

    private void refreshAdvances() {
        advTable.getColumns().get(0).setVisible(false);
        advTable.getColumns().get(0).setVisible(true);
    }

    private boolean isDateGood(String date) {

        DateTimeFormatter dtf = DateTimeFormat.forPattern("y-M-d");

        try {
            DateTime dt = dtf.parseDateTime(date);
        } catch (Exception ee) {
            return false;
        }

        return true;
    }

    private void calcSumsTotal() {
        selectedPayment.setDateStart(paymentDate.getText());
        selectedPayment.setDateEnd(dateTill.getText());
        selectedPayment.setEvent("calcFukkenShit");

        List<Payment> advancesSums = new ArrayList<>();

        try {
            selectedPayments = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());
            selectedPayment.setEvent("calcFukkenShitAdv");
            advancesSums = PaymentService.fetchList(selectedPayment, selectedPayment.getEvent());

            // selectedPayments.addAll(advancesSums);
            periodLabel.setText((new StringBuilder().append(paymentDate.getText()).append(" till ").append(dateTill.getText()).toString()));

            StringBuilder sb = new StringBuilder();
            sb.append("Payments total:\n").append("--------------------\n");

            for (Payment pe : selectedPayments) {
                BigDecimal bd = new BigDecimal(pe.getDisplayPrice());
                bd.setScale(2);

                String pattern = "#0.00";
                DecimalFormat myFormatter = new DecimalFormat(pattern);
                String output = myFormatter.format(bd);

                sb.append("Payments from ").append(pe.getBank()).append(" ").append(output).append(" EUR").append("\n");
            }

            sb.append("--------------------\n").append("ADVANCES total:\n").append("--------------------\n");

            for (Payment ppe : advancesSums) {
                BigDecimal bd = new BigDecimal(ppe.getDisplayPrice());
                bd.setScale(2);

                String pattern = "#0.00";
                DecimalFormat myFormatter = new DecimalFormat(pattern);
                String output = myFormatter.format(bd);

                sb.append("Advances from ").append(ppe.getBank()).append(" ").append(output).append(" EUR").append("\n");
            }

            totalPaymentsArea.setText(sb.toString());

            (sb.append(sb).append(sb).append(sb).append(sb)).toString();

            //StringBuilder
        } catch (Exception ee) {
            dialogs.initErrorDialog(ee.getMessage());
        }

    }

    private <T> List<List<T>> chopped(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }

    private String replaceLast(String string, String substring, String replacement) {
        int index = string.lastIndexOf(substring);
        if (index == -1) {
            return string;
        }
        return string.substring(0, index) + replacement
                + string.substring(index + substring.length());
    }

}
