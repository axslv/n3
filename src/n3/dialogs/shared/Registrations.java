/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.dialogs.shared;

import lv.nmc.entities.ClientEntry;

/**
 *
 * @author User
 */
public class Registrations {
    private ClientEntry sharedClient = new ClientEntry();

    /**
     * @return the sharedClient
     */
    public ClientEntry getSharedClient() {
        return sharedClient;
    }

    /**
     * @param aSharedClient the sharedClient to set
     */
    public void setSharedClient(ClientEntry aSharedClient) {
        sharedClient = aSharedClient;
    }
    
    private Registrations() {
    }
    
    public static Registrations getInstance() {
        return RegistrationsHolder.INSTANCE;
    }
    
    private static class RegistrationsHolder {

        private static final Registrations INSTANCE = new Registrations();
    }
}
