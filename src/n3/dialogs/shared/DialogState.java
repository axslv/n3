/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package n3.dialogs.shared;

/**
 *
 * @author User
 */
public class DialogState {
    private String lastError, lastMessage;
    private Integer state = 1;
    
    private DialogState() {
    }
    
    public static DialogState getInstance() {
        return DialogStateHolder.INSTANCE;
    }

    /**
     * @return the lastError
     */
    public String getLastError() {
        return lastError;
    }

    /**
     * @param lastError the lastError to set
     */
    public void setLastError(String lastError) {
        this.lastError = lastError;
    }

    /**
     * @return the lastMessage
     */
    public String getLastMessage() {
        return lastMessage;
    }

    /**
     * @param lastMessage the lastMessage to set
     */
    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    /**
     * @return the state
     */
    public Integer getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(Integer state) {
        this.state = state;
    }
    
    private static class DialogStateHolder {

        private static final DialogState INSTANCE = new DialogState();
    }
}
