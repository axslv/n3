/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.List;
import java.util.ResourceBundle;
import lv.nmc.entities.StaffEntry;



/**
 *
 * @author jm
 */
public class LoginService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("LoginService");

    public static List<StaffEntry> fetchList(StaffEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, StaffEntry.class, url);
        ff.sendQuery();
        return (List<StaffEntry>) ff.getResponseObject();
    }

    public static List<StaffEntry> fetchByList(StaffEntry se, List<String> vals, String field, String eventName) throws Exception {        
        Class cs = StaffEntry.class;
        ServerQuery ff = new ServerQuery(se, cs, url);
        ff.sendAdditionalQuery(vals, field, eventName);
        return (List<StaffEntry>) ff.getResponseObject();
    }

    public static StaffEntry fetchItem(StaffEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, StaffEntry.class, url);
        ff.sendQuery();
        return ((List<StaffEntry>) ff.getResponseObject()).get(0);
    }

    public static void updateRecord(StaffEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, StaffEntry.class, url);
        ff.sendQuery();
    }

    public static void deleteRecord(StaffEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, StaffEntry.class, url);
        ff.sendQuery();
    }
}
