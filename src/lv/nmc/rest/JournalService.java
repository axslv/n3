/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.ResourceBundle;
import lv.nmc.entities.JournalEntry;

/**
 *
 * @author jm
 */
public class JournalService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("JournalService");
    
    public static void insertRecord(JournalEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, JournalEntry.class, url);
        ff.sendQuery();
    }
}
