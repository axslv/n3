/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.List;
import java.util.ResourceBundle;
import lv.nmc.entities.GroupEntry;

/**
 *
 * @author jm
 */
public class GroupService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("GroupService");

    public static List<GroupEntry> fetchList(GroupEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, GroupEntry.class, url);
        ff.sendQuery();
        return (List<GroupEntry>) ff.getResponseObject();
    }

    public static List<GroupEntry> fetchByList(GroupEntry se, List<String> vals, String field, String eventName) throws Exception {        
        Class cs = GroupEntry.class;
        ServerQuery ff = new ServerQuery(se, cs, url);
        ff.sendAdditionalQuery(vals, field, eventName);
        return (List<GroupEntry>) ff.getResponseObject();
    }

    public static GroupEntry fetchItem(GroupEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, GroupEntry.class, url);
        ff.sendQuery();
        return ((List<GroupEntry>) ff.getResponseObject()).get(0);
    }

    public static void updateRecord(GroupEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, GroupEntry.class, url);
        ff.sendQuery();
    }

    public static void deleteRecord(GroupEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, GroupEntry.class, url);
        ff.sendQuery();
    }
    
    public void testMethod() {
        
    }
}
