/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.util.HashMap;
import java.util.ResourceBundle;
import javax.ws.rs.core.MultivaluedMap;
import lv.nmc.xml.ObjToMap;

public class RestRequest {
    private String hostname = ResourceBundle.getBundle("resources.settings").getString("serviceHost");
    private Integer portNum = Integer.parseInt(ResourceBundle.getBundle("resources.settings").getString("apiPort"));
    private String filePath, username = System.getProperty("n3.username"), password = System.getProperty("n3.password");
    private HashMap<String, String> queryParams = new HashMap();
    private String serverReply;
    private HttpClientPool hPool = HttpClientPool.getInstance();
    private String protocol = "http://";
    //MD5 version+++key
    private final String APIKEY = "19be52df2e817170dc1d01ce029ddbd8";

    

    public RestRequest(String filePath) {
        this.filePath = filePath;
        if (ResourceBundle.getBundle("resources.settings").getString("ssl").equals("true")) {
            this.portNum = Integer.parseInt(ResourceBundle.getBundle("resources.settings").getString("apiPortSSL"));
            this.protocol = "https://";
        }
        
    }

    public RestRequest() {
        
        if (ResourceBundle.getBundle("resources.settings").getString("ssl").equals("true")) {
            this.portNum = Integer.parseInt(ResourceBundle.getBundle("resources.settings").getString("apiPortSSL"));
            this.protocol = "https://";
        }
        
        }

 

    private void makeRequest(String username, String password) throws Exception {
        Client c = hPool.getC();
        c.addFilter(new HTTPBasicAuthFilter(username, password));
        WebResource rs = c.resource(protocol + getHostname() + ":" + portNum + getFilePath());
        MultivaluedMap map = new MultivaluedMapImpl();
    //    System.out.println(protocol + hostname + ":" + portNum + getFilePath());
        for (String keys : queryParams.keySet()) {
            map.add(keys, queryParams.get(keys));
        }
        map.add("version", APIKEY);
        this.serverReply = rs.queryParams(map).post(String.class);

    }
    
     private void makePublicRequest() throws Exception {
        Client c = hPool.getC();
       // c.addFilter(new HTTPBasicAuthFilter(username, password));
        WebResource rs = c.resource(protocol + getHostname() + ":" + portNum + getFilePath());
        MultivaluedMap map = new MultivaluedMapImpl();
     //   System.out.println(protocol + hostname + ":" + portNum + getFilePath());
        for (String keys : queryParams.keySet()) {
            map.add(keys, queryParams.get(keys));
        }
        map.add("version", APIKEY);
        this.serverReply = rs.queryParams(map).post(String.class);

    }

    /**
     * @return the hostname
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * @param hostname the hostname to set
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return the queryParams
     */
    public HashMap<String, String> getQueryParams() {
        return queryParams;
    }

    /**
     * @param queryParams the queryParams to set
     */
    public void performRequest(Object f, boolean auth) throws Exception {
        
        
        this.queryParams = ObjToMap.fromObject(f);
        if (auth) {
            makeRequest(username, password);
        } else {
            makePublicRequest();
        }
        
    }

    public void performRequestMap(HashMap<String, String> map) throws Exception {
        this.queryParams = map;
        makeRequest(username, password);
    }

    /**
     * @return the serverReply
     */
    public String getServerReply() {
        return serverReply;
    }

    /**
     * @param serverReply the serverReply to set
     */
    public void setServerReply(String serverReply) {
        this.serverReply = serverReply;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
