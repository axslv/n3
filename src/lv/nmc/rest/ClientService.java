/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.List;
import java.util.ResourceBundle;
import lv.nmc.entities.ClientEntry;

/**
 *
 * @author jm
 */
public class ClientService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("ClientService");

    public static List<ClientEntry> fetchList(ClientEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, ClientEntry.class, url);
        ff.sendQuery();
        return (List<ClientEntry>) ff.getResponseObject();
    }

    public static List<ClientEntry> fetchByList(ClientEntry se, List<String> vals, String field, String eventName) throws Exception {        
        Class cs = ClientEntry.class;
        ServerQuery ff = new ServerQuery(se, cs, url);
        ff.sendAdditionalQuery(vals, field, eventName);
        return (List<ClientEntry>) ff.getResponseObject();
    }

    public static ClientEntry fetchItem(ClientEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, ClientEntry.class, url);
        ff.sendQuery();
        return ((List<ClientEntry>) ff.getResponseObject()).get(0);
    }

    public static void updateRecord(ClientEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, ClientEntry.class, url);
        ff.sendQuery();
    }

    public static void deleteRecord(ClientEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, ClientEntry.class, url);
        ff.sendQuery();
    }
}
