/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import com.sun.jersey.api.client.Client;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jm
 */
public class HttpClientPool {
    private Client c = Client.create();
    private List<Client> poolMap = new ArrayList(); 
    private List<Client> busyPoolMap = new ArrayList(); 
    
    private HttpClientPool() {
        c.setConnectTimeout(3000);
        
  /*      poolMap.add(Client.create());
        poolMap.add(Client.create());
        poolMap.add(Client.create());
        poolMap.add(Client.create());
        poolMap.add(Client.create());*/
    }
    
    public synchronized Client createSession() {
        if (poolMap.isEmpty()) {
            busyPoolMap.add(Client.create());
        } else {                       
            busyPoolMap.add(poolMap.get(0));
            poolMap.remove(0); 
        }
        
        return busyPoolMap.get(0);
    }
    
    public synchronized void releaseSession() {
        poolMap.add(busyPoolMap.get(0));
        busyPoolMap.remove(0);
    }
    
    public static HttpClientPool getInstance() {
        return HttpClientPoolHolder.INSTANCE;
    }

    /**
     * @return the c
     */
    public Client getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(Client c) {
        this.c = c;
    }
    
    private static class HttpClientPoolHolder {

        private static final HttpClientPool INSTANCE = new HttpClientPool();
    }
}
