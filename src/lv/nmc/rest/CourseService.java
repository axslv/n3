/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.List;
import java.util.ResourceBundle;
import lv.nmc.entities.CourseEntry;

/**
 *
 * @author jm
 */
public class CourseService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("CourseService");

    public static List<CourseEntry> fetchList(CourseEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, CourseEntry.class, url);
        ff.sendQuery();
        return (List<CourseEntry>) ff.getResponseObject();
    }

    public static List<CourseEntry> fetchByList(CourseEntry se, List<String> vals, String field, String eventName) throws Exception {        
        Class cs = CourseEntry.class;
        ServerQuery ff = new ServerQuery(se, cs, url);
        ff.sendAdditionalQuery(vals, field, eventName);
        return (List<CourseEntry>) ff.getResponseObject();
    }

    public static CourseEntry fetchItem(CourseEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, CourseEntry.class, url);
        ff.sendQuery();
        return ((List<CourseEntry>) ff.getResponseObject()).get(0);
    }

    public static void updateRecord(CourseEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, CourseEntry.class, url);
        ff.sendQuery();
    }

    public static void deleteRecord(CourseEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, CourseEntry.class, url);
        ff.sendQuery();
    }
}
