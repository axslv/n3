/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import lv.nmc.entities.Payment;

/**
 *
 * @author jm
 */
public class PaymentService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("PaymentService");

    public static List<Payment> fetchList(Payment se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, Payment.class, url);
        ff.sendQuery();
        return (List<Payment>) ff.getResponseObject();
    }
    
        public static List<Payment> fetchByLongList(String addVals, String alias, String event) throws Exception {
        Payment pt = new Payment();
        ServerQuery ff = new ServerQuery(pt, Payment.class, url);
        ff.sendLongList(addVals, "list", "detailsByMultipleIdsString");
        return (List<Payment>) ff.getResponseObject();
    }

    public static List<Payment> fetchByList(Payment se, List<String> vals, String field, String eventName) throws Exception {
        Class cs = Payment.class;
        ServerQuery ff = new ServerQuery(se, cs, url);

        List<String> bckp = new ArrayList<>();

        if (vals.size() > 1000) {
            bckp = vals.subList(0, 999);
        }

        Integer mainSize = vals.size(); //876

   

        ff.sendAdditionalQuery(vals, field, eventName);
        return (List<Payment>) ff.getResponseObject();
    }
    
    

    public static Payment fetchItem(Payment se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, Payment.class, url);
        ff.sendQuery();
        return ((List<Payment>) ff.getResponseObject()).get(0);
    }

    public static void updateRecord(Payment se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, Payment.class, url);
        ff.sendQuery();
    }

    public static void deleteRecord(Payment se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, Payment.class, url);
        ff.sendQuery();
    }
}
