/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.List;
import java.util.ResourceBundle;
import lv.nmc.entities.DocumentEntry;



/**
 *
 * @author jm
 */
public class DocumentService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("DocumentService");

    public static List<DocumentEntry> fetchList(DocumentEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, DocumentEntry.class, url);
        ff.sendQuery();
        return (List<DocumentEntry>) ff.getResponseObject();
    }

    public static List<DocumentEntry> fetchByList(DocumentEntry se, List<String> vals, String field, String eventName) throws Exception {        
        Class cs = DocumentEntry.class;
        ServerQuery ff = new ServerQuery(se, cs, url);
        ff.sendAdditionalQuery(vals, field, eventName);
        return (List<DocumentEntry>) ff.getResponseObject();
    }

    public static DocumentEntry fetchItem(DocumentEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, DocumentEntry.class, url);
        ff.sendQuery();
        return ((List<DocumentEntry>) ff.getResponseObject()).get(0);
    }

    public static void updateRecord(DocumentEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, DocumentEntry.class, url);
        ff.sendQuery();
    }

    public static void deleteRecord(DocumentEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, DocumentEntry.class, url);
        ff.sendQuery();
    }
}
