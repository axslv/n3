/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import lv.nmc.entities.Sms;

/**
 *
 * @author jm
 */
public class SmsService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("SmsService");

    public static List<Sms> fetchList(Sms se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, Sms.class, url);
        ff.sendQuery();
        return (List<Sms>) ff.getResponseObject();
    }

    public static List<Sms> fetchByList(Sms se, List<String> vals, String field, String eventName) throws Exception {
        Class cs = Sms.class;
        ServerQuery ff = new ServerQuery(se, cs, url);

        List<String> bckp = new ArrayList<>();

        if (vals.size() > 1000) {
            bckp = vals.subList(0, 999);
        }

        Integer mainSize = vals.size(); //876

   

        ff.sendAdditionalQuery(vals, field, eventName);
        return (List<Sms>) ff.getResponseObject();
    }
    
    public static List<Sms> fetchByLongList(String addVals, String alias, String event) throws Exception {
        Sms pt = new Sms();
        ServerQuery ff = new ServerQuery(pt, Sms.class, url);
        ff.sendLongList(addVals, "list", event);
        return (List<Sms>) ff.getResponseObject();
    }
    
        public static List<Sms> fetchByMultipleLists(String[] addVals, String[] alias, String event) throws Exception {
        Sms pt = new Sms();
        ServerQuery ff = new ServerQuery(pt, Sms.class, url);
        ff.sendMultipleLists(addVals, alias, event);
        return (List<Sms>) ff.getResponseObject();
    }

    public static Sms fetchItem(Sms se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, Sms.class, url);
        ff.sendQuery();
        return ((List<Sms>) ff.getResponseObject()).get(0);
    }

    public static void updateRecord(Sms se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, Sms.class, url);
        ff.sendQuery();
    }

    public static void deleteRecord(Sms se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, Sms.class, url);
        ff.sendQuery();
    }
}
