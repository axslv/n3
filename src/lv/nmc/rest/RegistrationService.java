/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.List;
import java.util.ResourceBundle;
import lv.nmc.entities.RegistrationEntry;

/**
 *
 * @author jm
 */
public class RegistrationService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("RegistrationService");

    public static List<RegistrationEntry> fetchList(RegistrationEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, RegistrationEntry.class, url);
        ff.sendQuery();
        return (List<RegistrationEntry>) ff.getResponseObject();
    }

    public static RegistrationEntry fetchItem(RegistrationEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, RegistrationEntry.class, url);
        ff.sendQuery();
        return ((List<RegistrationEntry>) ff.getResponseObject()).get(0);
    }

    public static void updateRecord(RegistrationEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, RegistrationEntry.class, url);
        ff.sendQuery();
    }

    public static void deleteRecord(RegistrationEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, RegistrationEntry.class, url);
        ff.sendQuery();
    }
}
