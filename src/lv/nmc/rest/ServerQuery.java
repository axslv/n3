/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.util.HashMap;
import java.util.List;
import lv.nmc.entities.ClientEntry;
import lv.nmc.entities.CourseEntry;
import lv.nmc.entities.DocumentEntry;
import lv.nmc.entities.Email;
import lv.nmc.entities.GroupEntry;
import lv.nmc.entities.JournalEntry;
import lv.nmc.entities.Payment;
import lv.nmc.entities.RegistrationEntry;
import lv.nmc.entities.ReissueEntry;
import lv.nmc.entities.Sms;
import lv.nmc.entities.StaffEntry;
import lv.nmc.entities.col.CollegeEntry;
import lv.nmc.xml.ObjToMap;

/**
 *
 * @author jm
 */
public class ServerQuery {

    private String url;
    private String responseString = "";
    private Object responseObject = "";
    private Object queryObject;
    private Class serializeTo;

    //<editor-fold desc="Constructors">
    public ServerQuery(CollegeEntry queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery(Sms queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery(JournalEntry queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery(DocumentEntry queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery(Payment queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery(Email queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery(ReissueEntry queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery(ClientEntry queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery(CourseEntry queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery(GroupEntry queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery(StaffEntry queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery(RegistrationEntry queryObject, Class cls, String url) {
        this.queryObject = queryObject;
        serializeTo = cls;
        this.url = url;
    }

    public ServerQuery() {
    }

    //</editor-fold>
    public void sendQuery() throws Exception {
        Object se = queryObject;
        RestRequest rc = new RestRequest(url);
        rc.performRequest(se, true);
        responseString = rc.getServerReply();
        readXml();

    }

    public void sendPublicQuery() throws Exception {
        Object se = queryObject;
        RestRequest rc = new RestRequest(url);
        rc.performRequest(se, false);
        responseString = rc.getServerReply();
        readXml();
    }

    // returns REQUEST map
    public HashMap<String, String> sendCustomQuery(List<String> addVals, String fieldName, String eventName) throws Exception {
        Object se = queryObject;
        RestRequest rc = new RestRequest(url);
        HashMap<String, String> qmap = ObjToMap.fromObject(se);
        qmap.put("_eventName", eventName);

        int i = 0;

        for (String str : addVals) {
            StringBuilder sb = new StringBuilder();
            sb.append(fieldName);
            sb.append("[");
            sb.append(i);
            sb.append("]");
            qmap.put(sb.toString(), str);
            i++;
        }

        rc.performRequestMap(qmap);
        responseString = rc.getServerReply();
        readXml();

        return qmap;
    }

    public void sendLongList(String addVals, String fieldName, String eventName) throws Exception {
        Object se = queryObject;
        RestRequest rc = new RestRequest(url);
        HashMap<String, String> qmap = ObjToMap.fromObject(se);
        qmap.put("_eventName", eventName);

        qmap.put(fieldName, addVals);

        rc.performRequestMap(qmap);
        responseString = rc.getServerReply();
        // System.out.println(responseString);
        readXml();
        // System.out.println(qmap.get("_eventName"));
        // System.out.println(qmap.get(fieldName));
    }

    public void sendMultipleLists(String[] addVals, String[] fieldName, String eventName) throws Exception {
        Object se = queryObject;
        RestRequest rc = new RestRequest(url);
        HashMap<String, String> qmap = ObjToMap.fromObject(se);
        qmap.put("_eventName", eventName);

        qmap.put(fieldName[0], addVals[0]);
        System.out.println(fieldName[0] + " " + addVals[0]);
        qmap.put(fieldName[1], addVals[1]);
        System.out.println(fieldName[1] + " " + addVals[1]);
        rc.performRequestMap(qmap);
        responseString = rc.getServerReply();
        // System.out.println(responseString);
        readXml();
        // System.out.println(qmap.get("_eventName"));
        // System.out.println(qmap.get(fieldName));
    }

    public void sendAdditionalQuery(List<String> addVals, String fieldName, String eventName) throws Exception {
        Object se = queryObject;
        RestRequest rc = new RestRequest(url);
        HashMap<String, String> qmap = ObjToMap.fromObject(se);
        qmap.put("_eventName", eventName);

        int i = 0;

        for (String str : addVals) {
            StringBuilder sb = new StringBuilder();
            sb.append(fieldName);
            sb.append("[");
            sb.append(i);
            sb.append("]");
            qmap.put(sb.toString(), str.trim());
            i++;
        }

        rc.performRequestMap(qmap);
        responseString = rc.getServerReply();
        // System.out.println(responseString);
        readXml();
    }

    public void readXml() throws Exception {
        XStream xs = new XStream(new StaxDriver());
        xs.processAnnotations(serializeTo);
        responseObject = xs.fromXML(responseString);
    }

    /**
     * @return the responseObject
     */
    public Object getResponseObject() {
        return responseObject;
    }

    public String getResponseString() {
        return responseString;
    }
}
