/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.List;
import java.util.ResourceBundle;

import lv.nmc.entities.col.CollegeEntry;

/**
 *
 * @author jm
 */
public class CollegeService {
    private final static String url = ResourceBundle.getBundle("resources.urls").getString("CollegeService");

    public static List<CollegeEntry> fetchList(CollegeEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, CollegeEntry.class, url);
        ff.sendQuery();
        return (List<CollegeEntry>) ff.getResponseObject();
    }

    public static List<CollegeEntry> fetchByList(CollegeEntry se, List<String> vals, String field, String eventName) throws Exception {        
        Class cs = CollegeEntry.class;
        ServerQuery ff = new ServerQuery(se, cs, url);
        ff.sendAdditionalQuery(vals, field, eventName);
        return (List<CollegeEntry>) ff.getResponseObject();
    }

    public static CollegeEntry fetchItem(CollegeEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, CollegeEntry.class, url);
        ff.sendQuery();
        return ((List<CollegeEntry>) ff.getResponseObject()).get(0);
    }

    public static void updateRecord(CollegeEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, CollegeEntry.class, url);
        ff.sendQuery();
    }

    public static void deleteRecord(CollegeEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, CollegeEntry.class, url);
        ff.sendQuery();
    }
}
