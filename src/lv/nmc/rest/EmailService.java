/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.ResourceBundle;
import lv.nmc.entities.Email;

/**
 *
 * @author jm
 */
public class EmailService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("EmailService");

    public static Object fetchList(Email se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, Email.class, url);
        ff.sendQuery();
        return ff.getResponseObject();
    }

   
}
