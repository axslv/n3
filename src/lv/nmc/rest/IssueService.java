
package lv.nmc.rest;

import java.util.List;
import java.util.ResourceBundle;
import lv.nmc.entities.ReissueEntry;


/**
 *
 * @author jm
 */
public class IssueService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("IssueService");

    public static List<ReissueEntry> fetchList(ReissueEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, ReissueEntry.class, url);
        ff.sendQuery();
        return (List<ReissueEntry>) ff.getResponseObject();
    }

    public static List<ReissueEntry> fetchByList(ReissueEntry se, List<String> vals, String field, String eventName) throws Exception {        
        Class cs = ReissueEntry.class;
        ServerQuery ff = new ServerQuery(se, cs, url);
        ff.sendAdditionalQuery(vals, field, eventName);
        return (List<ReissueEntry>) ff.getResponseObject();
    }

    public static ReissueEntry fetchItem(ReissueEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, ReissueEntry.class, url);
        ff.sendQuery();
        return ((List<ReissueEntry>) ff.getResponseObject()).get(0);
    }

    public static void updateRecord(ReissueEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, ReissueEntry.class, url);
        ff.sendQuery();
    }

    public static void deleteRecord(ReissueEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, ReissueEntry.class, url);
        ff.sendQuery();
    }
}
