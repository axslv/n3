/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.rest;

import java.util.List;
import java.util.ResourceBundle;
import lv.nmc.entities.StaffEntry;

/**
 *
 * @author jm
 */
public class StaffService {

    private final static String url = ResourceBundle.getBundle("resources.urls").getString("StaffService");

    public static boolean loginUser(String username) throws Exception {
        StaffEntry se = new StaffEntry();
        se.setUsername(username.trim());
        se.setEvent("loginSpecialUser");
        ServerQuery ff = new ServerQuery(se, StaffEntry.class, url);
        ff.sendQuery();
        return true;
    }

    public static List<StaffEntry> fetchList(StaffEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, StaffEntry.class, url);
        ff.sendQuery();
        return (List<StaffEntry>) ff.getResponseObject();
    }
    
       public static StaffEntry fetchItem(StaffEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, StaffEntry.class, url);
        ff.sendQuery();        
        return ((List<StaffEntry>) ff.getResponseObject()).get(0);
    } 
    
    public static void updateRecord(StaffEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, StaffEntry.class, url);
        ff.sendQuery();
    }
    
    public static void deleteRecord(StaffEntry se, String event) throws Exception {
        se.setEvent(event);
        ServerQuery ff = new ServerQuery(se, StaffEntry.class, url);
        ff.sendQuery();
    }


}
