/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.xml;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jm
 */
public class ObjToMap {

    public static HashMap<String, String> fromObject(Object obj) {
       
        HashMap<String, String> query = new HashMap();

        List<Field> ls = Arrays.asList(obj.getClass().getDeclaredFields());

        for (Field ff : ls) {
            ff.setAccessible(true);

            try {

                if (ff.getName().contains("List")) {
                    
                    int i = 0;
                    List<Object> objList = (List<Object>) ff.get(obj);
                    
                    for (Object ob : objList) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(ff.getName());
                        sb.append("[");
                        sb.append(i);
                        sb.append("]");
                        query.put(sb.toString(), ob.toString());                        
                        i++;
                    }
                    
                    continue;
                }


                if (null != ff.get(obj)) {
                    query.put(ff.getName(), ff.get(obj).toString());
                }


            } catch (IllegalArgumentException ex) {
                Logger.getLogger(ObjToMap.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ObjToMap.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return query;

    }
}
