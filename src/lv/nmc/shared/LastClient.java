/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.shared;

import lv.nmc.entities.ClientEntry;

/**
 *
 * @author Novikontas
 */
public class LastClient {
    private ClientEntry lastClient = new ClientEntry();

    
    private LastClient() {
    }
    
    public static LastClient getInstance() {
        return LastClientHolder.INSTANCE;
    }

    /**
     * @return the lastClient
     */
    public ClientEntry getLastClient() {
        return lastClient;
    }

    /**
     * @param lastClient the lastClient to set
     */
    public void setLastClient(ClientEntry lastClient) {
        this.lastClient = lastClient;
    }
    
    private static class LastClientHolder {

        private static final LastClient INSTANCE = new LastClient();
    }
}
