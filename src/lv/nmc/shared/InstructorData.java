/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.shared;

import java.util.HashMap;
import lv.nmc.entities.StaffEntry;

/**
 *
 * @author User
 */
public class InstructorData {
    private HashMap<String, StaffEntry> lastInstructors = new HashMap();
    
    private InstructorData() {
    }
    
    public static InstructorData getInstance() {
        return CourseDataHolder.INSTANCE;
    }

    /**
     * @return the lastInstructors
     */
    public HashMap<String, StaffEntry> getLastInstructors() {
        return lastInstructors;
    }

    
    private static class CourseDataHolder {

        private static final InstructorData INSTANCE = new InstructorData();
    }
}
