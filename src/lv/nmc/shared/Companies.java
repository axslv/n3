/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lv.nmc.entities.RegistrationEntry;

/**
 *
 * @author User
 */
public class Companies {
    private HashMap<String, RegistrationEntry> lastCompanies = new HashMap();
    private List<RegistrationEntry> companiesList = new ArrayList<>();
    
    private Companies() {
    }
    
    public static Companies getInstance() {
        return CourseDataHolder.INSTANCE;
    }

    /**
     * @return the lastCompanies
     */
    public HashMap<String, RegistrationEntry> getLastCompanies() {
        return lastCompanies;
    }

    /**
     * @return the companiesList
     */
    public List<RegistrationEntry> getCompaniesList() {
        return companiesList;
    }

    /**
     * @param companiesList the companiesList to set
     */
    public void setCompaniesList(List<RegistrationEntry> companiesList) {
        this.companiesList = companiesList;
    }


    
    private static class CourseDataHolder {

        private static final Companies INSTANCE = new Companies();
    }
}
