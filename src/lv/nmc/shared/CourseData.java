/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.shared;

import java.util.HashMap;
import lv.nmc.entities.CourseEntry;

/**
 *
 * @author User
 */
public class CourseData {
    private HashMap<String, CourseEntry> lastCourses = new HashMap();
    
    private CourseData() {
    }
    
    public static CourseData getInstance() {
        return CourseDataHolder.INSTANCE;
    }

    /**
     * @return the lastCourses
     */
    public HashMap<String, CourseEntry> getLastCourses() {
        return lastCourses;
    }
    
    private static class CourseDataHolder {

        private static final CourseData INSTANCE = new CourseData();
    }
}
