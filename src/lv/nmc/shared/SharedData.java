package lv.nmc.shared;

import java.util.HashMap;
import lv.nmc.entities.Payment;
import lv.nmc.entities.StaffEntry;

public class SharedData {

    private String domain = "tc";
    private HashMap<Integer, StaffEntry> selectedInsId = new HashMap<>();
    private HashMap<String, StaffEntry> selectedIns = new HashMap<>();
    private HashMap<String, StaffEntry> selectedInsName = new HashMap<>();
    private Boolean giveCerts = false;
    private Payment selectedPayment = new Payment();
    private String reason = "";
    private Boolean reasonProvided = false;

    private SharedData() {
    }

    public static SharedData getInstance() {
        return ConstantDataHolder.INSTANCE;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return the selectedInsId
     */
    public HashMap<Integer, StaffEntry> getSelectedInsId() {
        return selectedInsId;
    }

    /**
     * @param selectedInsId the selectedInsId to set
     */
    public void setSelectedInsId(HashMap<Integer, StaffEntry> selectedInsId) {
        this.selectedInsId = selectedInsId;
    }

    /**
     * @return the selectedIns
     */
    public HashMap<String, StaffEntry> getSelectedIns() {
        return selectedIns;
    }

    /**
     * @param selectedIns the selectedIns to set
     */
    public void setSelectedIns(HashMap<String, StaffEntry> selectedIns) {
        this.selectedIns = selectedIns;
    }

    /**
     * @return the selectedInsName
     */
    public HashMap<String, StaffEntry> getSelectedInsName() {
        return selectedInsName;
    }

    /**
     * @param selectedInsName the selectedInsName to set
     */
    public void setSelectedInsName(HashMap<String, StaffEntry> selectedInsName) {
        this.selectedInsName = selectedInsName;
    }

    /**
     * @return the giveCerts
     */
    public Boolean getGiveCerts() {
        return giveCerts;
    }

    /**
     * @param giveCerts the giveCerts to set
     */
    public void setGiveCerts(Boolean giveCerts) {
        this.giveCerts = giveCerts;
    }

    private static class ConstantDataHolder {

        private static final SharedData INSTANCE = new SharedData();
    }

    /**
     * @return the selectedPayment
     */
    public Payment getSelectedPayment() {
        return selectedPayment;
    }

    /**
     * @param selectedPayment the selectedPayment to set
     */
    public void setSelectedPayment(Payment selectedPayment) {
        this.selectedPayment = selectedPayment;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
        this.reasonProvided = true;
    }

    /**
     * @return the reasonProvided
     */
    public Boolean getReasonProvided() {
        return reasonProvided;
    }

    /**
     * @param reasonProvided the reasonProvided to set
     */
    public void setReasonProvided(Boolean reasonProvided) {
        this.reasonProvided = reasonProvided;
    }
}
