/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.shared;

import java.util.HashMap;
import lv.nmc.entities.RegistrationEntry;

/**
 *
 * @author User
 */
public class GroupUsers {
    private HashMap<String, RegistrationEntry> lastCompanies = new HashMap();
    
    private GroupUsers() {
    }
    
    public static GroupUsers getInstance() {
        return CourseDataHolder.INSTANCE;
    }

    /**
     * @return the lastCompanies
     */
    public HashMap<String, RegistrationEntry> getLastCompanies() {
        return lastCompanies;
    }


    
    private static class CourseDataHolder {

        private static final GroupUsers INSTANCE = new GroupUsers();
    }
}
