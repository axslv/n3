/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.shared;

import lv.nmc.entities.Email;
import lv.nmc.rest.EmailService;

/**
 *
 * @author JM
 */
public class MessageSender {
    
    public static void sendMail(String messageText, String subject, String sendTo, String sendFrom) {
        Email em = new Email();
        em.setEmailText(messageText);
        em.setEmailSubject(subject);
        em.setSendTo(sendTo);
        em.setSendFrom(sendFrom);
        em.setEvent("sendMail");
        
        try {
            EmailService.fetchList(em, em.getEvent());
        } catch (Exception ee) {
            ee.printStackTrace();
        }
        
    }
    
}
