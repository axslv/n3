/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.shared;

/**
 *
 * @author jm
 */
public class LoginData {
    private String username="", password="";
    
    private LoginData() {
    }
    
    public static LoginData getInstance() {
        return LoginDataHolder.INSTANCE;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    private static class LoginDataHolder {

        private static final LoginData INSTANCE = new LoginData();
    }
}
