/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lv.nmc.entities.RegistrationEntry;

/**
 *
 * @author User
 */
public class Documents {
    private HashMap<String, RegistrationEntry> lastDocuments = new HashMap();
    private List<RegistrationEntry> documentsList = new ArrayList<>();
    
    private Documents() {
    }
    
    public static Documents getInstance() {
        return CourseDataHolder.INSTANCE;
    }

    private static class CourseDataHolder {

        private static final Documents INSTANCE = new Documents();
    }

    /**
     * @return the lastDocuments
     */
    public HashMap<String, RegistrationEntry> getLastDocuments() {
        return lastDocuments;
    }

    /**
     * @param lastDocuments the lastDocuments to set
     */
    public void setLastDocuments(HashMap<String, RegistrationEntry> lastDocuments) {
        this.lastDocuments = lastDocuments;
    }

    /**
     * @return the documentsList
     */
    public List<RegistrationEntry> getDocumentsList() {
        return documentsList;
    }

    /**
     * @param documentsList the documentsList to set
     */
    public void setDocumentsList(List<RegistrationEntry> documentsList) {
        this.documentsList = documentsList;
    }
}
