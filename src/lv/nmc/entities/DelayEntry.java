package lv.nmc.entities;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

/**
 *
 * @author jm
 */
@XStreamAlias("DelayEntry")
public class DelayEntry implements Serializable {   
    private String event = "noEvent";
    private String nameSurname, personCode, banReason;
    private String banDate;
    private Integer courseId, groupId, registrationId;

    public DelayEntry() {
        
    }


    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }


    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }


    public String getPersonCode() {
        return personCode;
    }


    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }


    public String getBanReason() {
        return banReason;
    }


    public void setBanReason(String banReason) {
        this.banReason = banReason;
    }


    public String getBanDate() {
        return banDate;
    }

    public void setBanDate(String banDate) {
        this.banDate = banDate;
    }


    public Integer getCourseId() {
        return courseId;
    }


    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

 
    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(Integer registrationId) {
        this.registrationId = registrationId;
    }

}
