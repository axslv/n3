package lv.nmc.entities;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@XStreamAlias("TcPrereg")
public class TcPrereg implements Serializable {

    private Integer registrationId, groupId, companyId, courseId, userId, toRegisterL;
    private String courseTitle, price, dateStart, crewComment, crewCompany;
    private String event = "noEvent";
    private boolean selected = true;

    public TcPrereg() {
    }

    /**
     * @return the registrationId
     */
    public Integer getRegistrationId() {
        return registrationId;
    }

    /**
     * @param registrationId the registrationId to set
     */
    public void setRegistrationId(Integer registrationId) {
        this.registrationId = registrationId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

  
    /**
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }


    /**
     * @return the companyId
     */
    public Integer getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId the companyId to set
     */
    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    /**
     * @return the courseTitle
     */
    public String getCourseTitle() {
        return courseTitle;
    }

    /**
     * @param courseTitle the courseTitle to set
     */
    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return the dateStart
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * @return the crewComment
     */
    public String getCrewComment() {
        return crewComment;
    }

    /**
     * @param crewComment the crewComment to set
     */
    public void setCrewComment(String crewComment) {
        this.crewComment = crewComment;
    }

    /**
     * @return the crewCompany
     */
    public String getCrewCompany() {
        return crewCompany;
    }

    /**
     * @param crewCompany the crewCompany to set
     */
    public void setCrewCompany(String crewCompany) {
        this.crewCompany = crewCompany;
    }

    /**
     * @return the toRegisterL
     */
    public Integer getToRegisterL() {
        return toRegisterL;
    }

    /**
     * @param toRegisterL the toRegisterL to set
     */
    public void setToRegisterL(Integer toRegisterL) {
        this.toRegisterL = toRegisterL;
        if (toRegisterL.equals(0)) {
            this.selected = false;
        } else {
            this.selected = true;
        }
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
        if (selected) {
            this.toRegisterL = 1;
        } else {
            this.toRegisterL = 0;
        }
    }

 
}
