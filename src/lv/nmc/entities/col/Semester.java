
package lv.nmc.entities.n3.col;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Semester")
public class Semester {
    private String dateStart, dateEnd;
    private Integer semesterNum;
    
    public Semester(String dateStart, String dateEnd) {
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }
    
    public Semester(Integer i) {
        this.semesterNum = i;
    }


    public String getDateStart() {
        return dateStart;
    }


    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Integer getSemesterNum() {
        return semesterNum;
    }

    public void setSemesterNum(Integer semesterNum) {
        this.semesterNum = semesterNum;
    }
    
}
