/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.entities;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author JM
 */
@XStreamAlias("NotaEntry")
public class NotaEntry {
    private Integer notaId;
    private String emailSubject, emailText, sendTo, sendFrom="robots@novikontas.lv", event;
    private String notaType="email", sysName;
    private String notaTarget, notaCriteria;

    /**
     * @return the notaId
     */
    public Integer getNotaId() {
        return notaId;
    }

    /**
     * @param notaId the notaId to set
     */
    public void setNotaId(Integer notaId) {
        this.notaId = notaId;
    }

    /**
     * @return the emailSubject
     */
    public String getEmailSubject() {
        return emailSubject;
    }

    /**
     * @param emailSubject the emailSubject to set
     */
    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    /**
     * @return the emailText
     */
    public String getEmailText() {
        return emailText;
    }

    /**
     * @param emailText the emailText to set
     */
    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }

    /**
     * @return the sendTo
     */
    public String getSendTo() {
        return sendTo;
    }

    /**
     * @param sendTo the sendTo to set
     */
    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    /**
     * @return the sendFrom
     */
    public String getSendFrom() {
        return sendFrom;
    }

    /**
     * @param sendFrom the sendFrom to set
     */
    public void setSendFrom(String sendFrom) {
        this.sendFrom = sendFrom;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the notaType
     */
    public String getNotaType() {
        return notaType;
    }

    /**
     * @param notaType the notaType to set
     */
    public void setNotaType(String notaType) {
        this.notaType = notaType;
    }

    /**
     * @return the sysName
     */
    public String getSysName() {
        return sysName;
    }

    /**
     * @param sysName the sysName to set
     */
    public void setSysName(String sysName) {
        this.sysName = sysName;
    }

    /**
     * @return the notaTarget
     */
    public String getNotaTarget() {
        return notaTarget;
    }

    /**
     * @param notaTarget the notaTarget to set
     */
    public void setNotaTarget(String notaTarget) {
        this.notaTarget = notaTarget;
    }

    /**
     * @return the notaCriteria
     */
    public String getNotaCriteria() {
        return notaCriteria;
    }

    /**
     * @param notaCriteria the notaCriteria to set
     */
    public void setNotaCriteria(String notaCriteria) {
        this.notaCriteria = notaCriteria;
    }
    
}
