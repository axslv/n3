/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lv.nmc.entities;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author Administrator
 */
@XStreamAlias("Email")
public class Email {
    private String emailSubject, emailText, sendTo="robots@novikontas.lv", sendFrom="robots@novikontas.lv", event="sendMail";

    /**
     * @return the emailSubject
     */
    public String getEmailSubject() {
        return emailSubject;
    }

    /**
     * @param emailSubject the emailSubject to set
     */
    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    /**
     * @return the emailText
     */
    public String getEmailText() {
        return emailText;
    }

    /**
     * @param emailText the emailText to set
     */
    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }

    /**
     * @return the sendTo
     */
    public String getSendTo() {
        return sendTo;
    }

    /**
     * @param sendTo the sendTo to set
     */
    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    /**
     * @return the sendFrom
     */
    public String getSendFrom() {
        return sendFrom;
    }

    /**
     * @param sendFrom the sendFrom to set
     */
    public void setSendFrom(String sendFrom) {
        this.sendFrom = sendFrom;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }
    
}
